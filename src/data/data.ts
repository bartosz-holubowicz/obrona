import { ICategory } from "./ICategory";

export const questionsData: ICategory[] = [
  {
    "id": 0,
    "name": "Algorytmy i struktury danych",
    "icon": "calculator",
    "chance": 95,
    "questions": [
      {
        "id": 0,
        "question": "Algorytm. Własności algorytmu. Prezentacja algorytmu. +",
        "answer": `
          <p><strong>Algorytm</strong> — jednoznacznie sformułowany sposób postępowania, który w skończonej liczbie kroków umożliwia rozwiązanie zadania określonej klasy. Zakodowany w wybranym języku programowania zamienia się w program komputerowy.</p>
          <p><strong>Każdy algorytm:</strong></p>
          <ul>
            <li>Posiada dane wejściowe pochodzące z dobrze zdefiniowanego zbioru</li>
            <li>Produkuje pewien wynik</li>
            <li>Każdy krok algorytmu musi być jednoznacznie określony</li>
            <li>Jest skończony</li>
            <li>Powinien być efektywny tzn. wykonywać swoją pracę w jak najkrótszym czasie i wykorzystując jak najmniejszą ilość pamięci</li>
            <li>Powinien dawać poprawne wyniki</li>
          </ul>
        `
      },
      {
        "id": 1,
        "question": "Złożoność algorytmów. +",
        "answer": `
          <p><strong>Złożoność algorytmu</strong> rozumiemy jako <strong>ilość niezbędnych zasobów do wykonania algorytmu</strong>.</p>
          <p>Złożoność dzielimy na <strong>czasową</strong> oraz <strong>pamięciową</strong>:</p>
          <ul>
          <li>w przypadku złożoności czasowej, z reguły wyróżnimy pewną <strong>operację dominującą</strong>, a <strong>czas</strong> będziemy traktować jako <strong>liczbę wykonanych operacji dominujących</strong>. Złożoność algorytmu może być rozumiana w sensie <strong>złożoności najgorszego przypadku</strong> lub <strong>złożoności średniej</strong>. Złożoność najgorszego przypadku nazywamy <strong>złożonością pesymistyczną</strong> - jest to <strong>maksymalna złożoność dla danych tego samego rozmiaru n</strong>. g(n) = n liniowa, g(n) = n<sup>2</sup> kwadratowa, gdy g(n) jest wielomianem to złożoność wielomianowa.</li>
          <li>złożoność pamięciowa jest <strong>miarą ilości wykorzystanej pamięci</strong>. Jako tę ilość najczęściej przyjmuje się <strong>użytą pamięć maszyny abstrakcyjnej</strong>. Możliwe jest również obliczanie rozmiaru potrzebnej pamięci fizycznej wyrażonej w bitach lub bajtach.</li>
        `
      },
      {
        "id": 2,
        "question": "Problemy sortowania. Przykłady algorytmów sortowania i ich złożoność. +",
        "answer": `
          <p><strong>Sortowanie</strong> – polega na uporządkowaniu zbioru danych względem pewnych cech charakterystycznych każdego elementu tego zbioru.</p> 
          <p>Złożoność algorytmów sortowania jest określana jako liczba wykonywanych porównań i zamian elementów, wyrażona w zależności od liczby porządkowanych elementów. Złożoność najlepszych algorytmów sortowania jest proporcjonalna do n*logn, gdzie n jest liczbą porządkowanych elementów.</p>
          <p><strong>Przykładowe algorytmy sortowania</strong></p>
          <ul>
            <li>sortowanie kubełkowe (ang. bucket sort) - O(n^2)</li>
            <li>sortowanie bąbelkowe (ang. bubblesort) - O(n^2)</li>
            <li>sortowanie przez wstawianie (ang. insertion sort) - O(n^2)</li>
            <li>sortowanie przez wybieranie (ang. selection sort) - O(n^2)</li>
            <li>sortowanie przez scalanie (ang. merge sort) - O(n*logn)</li>
            <li>sortowanie szybkie (ang. quicksort) - O(n*logn)</li>
            <li>sortowanie przez kopcowanie (ang. heapsort) - O(n*logn)</li>
            <li>sortowanie przez zliczanie (ang. counting sort) - O(n + k)</li>
          </ul>
        `
      },
      {
        "id": 3,
        "question": "Drzewa przeszukiwań binarnych. Sposób wykonywania na nich podstawowych operacji (dodawanie, wyszukiwanie, usuwanie). +",
        "answer": `
          <p><strong>Drzewo przeszukiwań binarnych (BST)</strong> to takie drzewo binarne, w którym każdy węzeł spełnia dane reguły:</p>
          <ol>
            <li>dla dowolnego węzła, <strong>lewy syn</strong> (jeżeli istnieje) posiada <strong>wartość mniejszą</strong></li>
            <li>dla dowolnego węzła, <strong>prawy syn</strong> (jeżeli istnieje) posiada <strong>wartość większą</strong></li>
            <li>jeśli węzeł posiada lewe poddrzewo (drzewo, którego korzeniem jest lewy syn), to wszystkie węzły w tym poddrzewie mają wartość nie większą od wartości danego węzła</li>
            <li>jeśli węzeł posiada prawe poddrzewo, to wszystkie węzły w tym poddrzewie są nie mniejsze od wartości danego węzła</li>
          </ol>
          <p class="text-xl"><strong>Wyszukiwanie</strong>:</p>
          <p><strong>Wszerz</strong>: po kolei, po każdym poziomie od lewej do prawej
          <p><strong>W głąb</strong>:
          <ul>
            <li><em>Pre-order</em>: korzeń, lewe poddrzewo, prawe poddrzewo</li>
            <li><em>In-order</em>: lewe poddrzewo, korzeń, prawe poddrzewo</li>
            <li><em>Post-order</em>: lewe poddrzewo, prawe poddrzewo, korzeń</li>
          </ul>
          <p class="text-xl"><strong>Dodawanie</strong>:</p>
          <p><strong>Jeśli drzewo jest puste</strong>, to nowy węzeł staje się jego <strong>korzeniem</strong>.</p>
          <p>W przeciwnym razie porównujemy klucz wstawianego węzła z kluczami kolejnych węzłów, idąc w dół drzewa. Jeśli <strong>klucz</strong> nowego węzła <stsrong>jest mniejszy od klucza aktualnie odwiedzonego węzła w drzewie</strong>, to przechodzimy do <strong>lewego syna</strong>.</p>
          <p>Inaczej przechodzimy do <strong>prawego syna</strong>. Całą procedurę kontynuujemy do momentu, <strong>aż dany syn nie istnieje</strong>. Wtedy dołączamy nowy węzeł na jego miejsce i kończymy.
          <p class="text-xl"><strong>Usuwanie</strong>:</p>
          <p>Jeśli węzeł <strong>nie posiada dzieci</strong>, to po prostu <strong>odłączamy go od struktury drzewa</strong>.</p>
          <p>Jeśli węzeł posiada tylko lewego lub tylko prawego potomka - usuwany węzeł zastępujemy potomkiem</p>
          <p>Jeśli węzeł posiada obu potomków - lewego i prawego, usuwamy go na jeden ze sposobów:</p>
          <ol>
            <li>połączenie poprzednika elementu z wierzchołkiem o <strong>najmniejszej wartości</strong> z <strong>prawej poddrzewa</strong></li>
            <li>połączenie poprzednika elementu z wierzchołkiem o <strong>największej wartości</strong> z <strong>lewego poddrzewa</strong></li>
          </ol>
        `
      },
      {
        "id": 4,
        "question": "Algorytmy przeszukiwania grafu. +",
        "answer": `
          <p><strong>Przeszukiwanie w głąb (DFS)</strong></p>
          <ol>
            <li>Wybieramy wierzchołek, od którego rozpoczniemy szukanie. Odwiedzamy go, a następnie wrzucamy na stos wszystkie następniki tego wierzchołka, w kolejności od tego z największym indeksem.</li>
            <li>Ściągamy wierzchołek ze stosu i odwiedzamy go, a wszystkie nieodwiedzone następniki wrzucamy na stos.</li>
            <li>Poprzedni krok powtarzamy aż stos się nie opróżni.</li>
          </ol>
          <p><strong>Przeszukiwanie wszerz (BFS)</strong></p>
          <ol>
            <li>Wybieramy wierzchołek, od którego rozpoczniemy szukanie. Odwiedzamy go, a następnie wrzucamy do kolejki wszystkie następniki tego wierzchołka, w kolejności od tego z najmniejszym indeksem.</li>
            <li>Wyciągamy wierzchołek z kolejki i odwiedzamy go, a wszystkie nieodwiedzone następniki wrzucamy do kolejki.</li>
            <li>Poprzedni krok powtarzamy aż kolejka się nie opróżni.</li>
          </ol>
        `
      },
      {
        "id": 5,
        "question": "Abstrakcyjne struktury danych: listy, kolejki, stosy. Ich implementacja komputerowa. -",
        "answer": `
          <p class="text-xl"><strong>Listy</strong></p>
          <p>To rodzaj kontenera – dynamiczna struktura danych – składająca się z podstruktur wskazujących na następniki i/lub poprzedniki. Typowa lista jest łączona jednostronnie - komórki zawierają tylko odnośnik do kolejnej komórki. Innym przypadkiem jest lista dwustronna, gdzie komórki zawierają także odnośnik do poprzednika.</p>
          <p><strong>Implementacja wskaźnikowa</strong></p>
          <p>W tej implementacji każdy obiekt na liście musi zawierać dodatkowy element: wskaźnik do innego obiektu tego typu. Wynika to z faktu, że to wskaźniki są podstawą nawigacji w tym typie listy, a dostęp do jej elementów jest możliwy wyłącznie przez wskaźnik.</p>
          <p class="text-xl"><strong>Stos</strong></p>
          <p>Liniowa struktura danych, w której dane dokładane są na wierzch stosu i z wierzchołka stosu są pobierane (bufor typu LIFO, Last In, First Out; ostatni na wejściu, pierwszy na wyjściu).</p>
          <p>Ideę stosu danych można zilustrować jako stos ułożonych jedna na drugiej książek – nowy egzemplarz kładzie się na wierzch stosu i z wierzchu stosu zdejmuje się kolejne egzemplarze. Elementy stosu poniżej wierzchołka stosu można wyłącznie obejrzeć, aby je ściągnąć, trzeba najpierw po kolei ściągnąć to, co jest nad nimi.</p>
          <p><strong>Implementacja</strong></p>
          <p>Strukturami danych służącymi do reprezentacji stosu mogą być tablice (gdy znamy maksymalny rozmiar stosu), tablice dynamiczne lub listy. Złożoność obliczeniowa operacji na stosie zależy od konkretnej implementacji, ale w większości przypadków jest to czas stały O(1).</p>
          <p class="text-xl"><strong>Kolejki</strong></p>
          <p>Liniowa struktura danych, w której nowe dane dopisywane są na końcu kolejki, a z początku kolejki pobierane są dane do dalszego przetwarzania (bufor typu FIFO, First In, First Out; pierwszy na wejściu, pierwszy na wyjściu).</p>
          <p><strong>Implementacja</strong></p>
          <p>Podobnie jak w przypadku stosu, strukturę danych służącą do reprezentacji kolejki mogą być tablice, tablice dynamiczne lub listy.</p>
        `
      },
      {
        "id": 6,
        "question": "Zasada działania i sposoby implementacji tablic haszowanych. -",
        "answer": `
          <p>Tablice mieszające opierają się na <strong>zwykłych tablicach indeksowanych liczbami</strong> - dostęp do
          danych jest <em>bardzo szybki</em>, nie zależy od rozmiaru tablicy ani położenia elementu. W tablicy mieszającej
          stosuje się <strong>funkcję mieszającą</strong> (<em>funkcję skrótu</em>, <em>funkcję haszującą</em>), która dla danego klucza wyznacza indeks
          w tablicy.</p>
          <p class="text-xl"><strong>Funkcja skrótu</strong>:</p>
          <p><strong>Dzielenie: h(x) = x mod m</strong> zbiór kluczy składa się z liczb naturalnych, a m jest rozmiarem tablicy
          haszującej. Zakładając, że tablica haszująca ma 10 elementów, 149 powinno się znaleźć na pozycji
          dziewiątej, bo 149 mod 10 = 9.</p>
          <p><strong>Wycinanie</strong>: do obliczenia adresu wykorzystuje się tylko część klucza. Może to być np. fragment
          środkowy, albo ostatnie cztery pozycje, albo konkretnie wskazane pozycje. Na ogół pomijamy te fragmenty
          klucza, które nie rozróżniają dostatecznie dobrze elementów.</p>
          <p>Na przykład, gdybyśmy chcieli zapamiętać numery telefonów stacjonarnych, to nie ma sensu brać pod
          uwagę prefiksu 89, gdyż jest on identyczny dla wszystkich przypadków.</p>
          <p class="text-xl"><strong>Problem kolizji</strong>:</p>
          <p>Wartość funkcji mieszającej obliczonej dla klucza elementu wstawianego do tablicy pokrywa się z wartością
          funkcji obliczoną dla klucza jakiegoś elementu już znajdującego się w tej tablicy.</p>
          <p><strong>Metoda łańcuchowa</strong> - Polega na przechowywaniu elementów nie bezpośrednio w tablicy, lecz na
          liście związanej z danym indeksem. Wstawiane elementy dołącza się do jednego z końców listy.</p>
          <p><strong>Adresowanie otwarte</strong> - W podejściu tym nowy element wstawia się w innym miejscu niż
          wynikałoby to z wartości funkcji mieszającej. Nowa lokalizacja określana jest przez dodanie do wartości
          funkcji mieszającej wartości tzw. <strong>funkcji przyrostu</strong> <em>p(i)</em>, gdzie i oznacza numer próby (to znaczy ile razy
          wstawienie się nie powiodło ze względu na kolizję).</p>
        `
      },
      {
        "id": 7,
        "question": "Idea algorytmu zachłannego. Przykład. +",
        "answer": `
          <p>Algorytmy zachłanne są algorytmami, które w <strong>każdym kroku wybierają lokalnie najkorzystniejszą decyzję</strong>. <strong>Nie dają gwarancji znalezienia optymalnego globalnego rozwiązania</strong> oraz <strong>nie ma w nich losowości</strong> (należą do podzbioru algorytmów heurystycznych i są deterministyczne).</p>
          <p class="text-xl"><strong>Przykład - problem wydawania reszty</strong></p>
          <p>Chcemy wydać resztę tak, aby użyć jak najmniejszej ilości monet. Dla monet o nominałach 1, 3 oraz 4. Chcemy wydać 6.</p>
          <p>Algorytm w pierwszym kroku wybierze monetę o nominale 4 (<strong>najkorzystniejsze rozwiązanie lokalne</strong>), pozostanie kwota 2, więc <strong>algorytm wybierze 1 i jeszcze raz 1</strong>. Użyto 3 monet, podczas gdy <strong>rozwiązanie optymalne to 2 monety o nominale 3</strong>.</p>
          <p>Nie istnieje ogólna metoda dowodzenia, czy dla danego problemu rozwiązanie zachłanne (zawsze) odnajduje poprawny (i optymalny) wynik. Istnieją jednak stosujące się do samego problemu kryteria, pozwalające sądzić, iż dla owego problemu istnieje rozwiązanie zachłanne.</p>
          <p>Własność zachłannego wyboru:</p>
          <ul>
            <li>za pomocą lokalnie optymalnych (zachłannych) wyborów można uzyskać optymalne rozwiązanie całego zadania.</li>
          </ul>
          <p>Własność optymalnej podstruktury:</p>
          <ul>
            <li>optymalne rozwiązanie całego problemu jest możliwe tylko przy optymalnym rozwiązaniu podproblemów.</li>
          </ul>
        `
      },
      {
        "id": 8,
        "question": "Idea Dijkstry algorytmu znajdowania najkrótszej ścieżki. +",
        "answer": `
          <p><strong>Algorytm Dijkstry</strong>, służy do znajdowania najkrótszej ścieżki z pojedynczego źródła w grafie o nieujemnych
          wagach krawędzi. Jest często używany w trasowaniu. Mając dany graf z wyróżnionym wierzchołkiem
          (<em>źródłem</em>) algorytm znajduje odległości od źródła do wszystkich pozostałych wierzchołków. Łatwo
          zmodyfikować go tak, aby szukał wyłącznie ścieżki do jednego ustalonego wierzchołka, po prostu
          przerywając działanie w momencie dojścia do wierzchołka docelowego.</p>
        `
      },
      {
        "id": 9,
        "question": "Pozycyjne systemy liczbowe (binarny, dziesiętny, szesnastkowy). Prezentacja liczb w komputerze.",
        "answer": `
        <p>Pozycyjne systemy liczbowe posiadają symbole (cyfry) tylko dla kilku najmniejszych liczb naturalnych: 0, 1, 2, ..., g-1, gdzie <strong>g</strong> to tzw. <strong>podstawa systemu</strong>. Cyfry te są umieszczane na ściśle określonych pozycjach i są mnożone przez odpowiednią potęgę g.</p>
        <p><strong>DEC</strong> - System dziesiętny podstawą pozycji są kolejne potęgi liczby 10. Do zapisu liczb potrzebne jest więc w nim 10 cyfr: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.</p>
        <p><em>Pozycyjny, dziesiętny system liczbowy jest obecnie na świecie podstawowym systemem stosowanym niemal we wszystkich krajach.</em></p>
        <p><strong>BIN</strong> - System binarny (dwójkowy) podstawą jest liczba 2. Do zapisu liczb potrzebne są więc tylko dwie cyfry: 0 i 1. Powszechnie używany w elektronice cyfrowej, przyjął się też w informatyce. Liczba zapisana w dziesiętnym systemie liczbowym jako 10, w systemie dwójkowym przybiera postać 1010.</p>
        <p><strong>HEX</strong> - System heksadecymalny (szesnastkowy) Szesnastkowy system – podstawą jest liczba 16. W najpowszechniejszym standardzie poza cyframi dziesiętnymi od 0 do 9 używa się pierwszych sześciu liter alfabetu łacińskiego: A, B, C, D, E, F (dużych lub małych). Cyfry 0-9 mają te same wartości co w systemie dziesiętnym, natomiast litery odpowiadają następującym wartościom: A = 10, B = 11, C = 12, D = 13, E = 14 oraz F = 15. Liczba zapisana w dziesiętnym systemie liczbowym jako 1000, w systemie szesnastkowym przybiera postać 3E8. System szesnastkowy sprawdza się szczególnie przy zapisie dużych liczb takich jak adresy pamięci, zakresy parametrów itp.</p>
        `
      }
    ]
  },
  {
    "id": 1,
    "name": "Architektura i organizacja komputerów",
    "icon": "box",
    "chance": 5,
    "questions": [
      {
        "id": 0,
        "question": "Budowa komputera.",
        "answer": `
          <p>Komponenty, z których jest zbudowany komputer to:</p>
          <ul>
            <li><strong>płyta główna</strong> (łączy podzespoły i zapewnia komunikację między nimi),</li>
            <li><strong>procesor</strong> (koordynuje pracę pozostałych podzespołów komputera, wykonuje instrukcje programów),</li>
            <li><strong>pamięć RAM</strong> (pamięć tymczasowa, przechowuje dane dotyczące aktualnych programów i ich wykonywania),</li>
            <li><strong>dysk HDD/SSD</strong> (urządzenie pamięci masowej, pozwala na zapisywanie dużej ilości danych i przechowywanie ich przez długi czas),</li>
            <li><strong>zasilacz</strong> (dostarcza energię do poszczególnych podzespołów),</li>
            <li><strong>karta graficzna</strong>  (odpowiada za renderowanie grafiki i jej konwersję na sygnał zrozumiały dla wyświetlacza),</li>
            <li><strong>obudowa</strong> (umożliwia zamontowanie w niej podzespołów komputerowych),</li>
            <li><strong>opcjonalny napęd optyczny</strong> (umożliwia odczyt i/lub zapis danych na nośnikach optycznych)</li>
          </ul>
        `
      },
      {
        "id": 1,
        "question": "Struktura procesora",
        "answer": `
          <p><strong>W funkcjonalnej strukturze procesora można wyróżnić takie elementy, jak</strong>:</p>
          <ul>
            <li>bardzo szybką pamięć podręczną (cache)</li>
            <li>zespół rejestrów do przechowywania danych i wyników,</li>
            <li>jednostkę arytmetyczną do wykonywania operacji obliczeniowych na danych,</li>
            <li>układ sterujący przebiegiem wykonywania programu,</li>
            <li>inne układy, w które producent wyposaża procesor w celu usprawnienia jego pracy.</li>
          </ul>
          <p>Jednym z parametrów procesora jest rozmiar elementów budujących jego strukturę. Im są one mniejsze,
          tym niższe jest zużycie energii, napięcie pracy oraz wyższa możliwa do osiągnięcia częstotliwość pracy.</p>
        `
      },
      {
        "id": 2,
        "question": "Cykl rozkazowy procesora",
        "answer": `
          <p>Cykl rozkazowy procesora to pewien schemat powtarzających się czynności, realizujących wykonywanie
          instrukcji programu. Na cykl rozkazowy składają się dwie fazy:</p>
          <ul>
            <li>faza pobrania,</li>
            <li>faza wykonania.</li>
          </ul>
          <p><strong>W fazie pobrania</strong> procesor pobiera kod rozkazu z komórki pamięci. Adres pod którym znajduje się rozkaz
          przechowywany jest w liczniku rozkazów. Wartość tego licznika wysyłana jest na magistralę adresową a
          licznik rozkazów zostaje zwiększony tak, aby wskazywał na następny rozkaz przeznaczony do wykonania.</p>
          <p>Po fazie pobrania następuje <strong>faza wykonania</strong>, w której procesor dekoduje rozkaz i określa jego typ. Układ
          sterowania generuje odpowiednie sygnały sterujące, realizujące dany rozkaz. Jeżeli rozkaz wymaga pobrania
          argumentów, to przed jego wykonaniem są one pobierane z pamięci i umieszczane w odpowiednich
          rejestrach.</p>
        `
      },
      {
        "id": 3,
        "question": "Metody odwzorowania pamięci głównej w pamięci podręcznej.",
        "answer": `
          <p><strong>Odwzorowanie bezpośrednie</strong> - najprostsza metoda, polega na odwzorowywaniu każdego bloku pamięci
          głównej na tylko jeden możliwy wiersz pamięci podręcznej.</p>
          <p><strong>Odwzorowywanie skojarzeniowe</strong> umożliwia ładowanie każdego bloku pamięci głównej do dowolnego
          wiersza pamięci podręcznej. W tym przypadku sterujące układy logiczne pamięci podręcznej interpretują
          adres pamięci po prostu jako znacznik i pole słowa. Pole znacznika jednoznacznie określa blok pamięci
          głównej.</p>
          <p><strong>Odwzorowywanie sekcyjno-skojarzeniowe</strong> - pamięć podręczna jest dzielona na v sekcji, z których każda
          składa się z k wierszy. W przypadku odwzorowywania sekcyjno-skojarzeniowego blok Bj może być
          odwzorowywany na dowolny wiersz sekcji i. W tym przypadku sterujące układy logiczne pamięci podręcznej
          interpretują adres pamięci jako trzy pola: <strong>znacznik, sekcja i słowo</strong>.</p>
        `
      },
      {
        "id": 4,
        "question": "Przetwarzanie potokowe.",
        "answer": `
          <p><strong>Przetwarzanie potokowe</strong> — <em>(Używanie w celu zwiększenia wydajności)</em> sposób pracy procesora polegający na wykonywaniu kolejnych instrukcji, przy czym
          rozpoczęcie następnej nie wymaga zakończenia poprzedniej. Dzięki temu, że przetwarzanie odbywa się w
          rozdzielnych blokach system może przetwarzać jednocześnie tyle danych ile zdefiniowano bloków.</strong></p>
          <p><strong>Wadą</strong> rozwiązania jest dłuższy czas oczekiwania na zakończenie pierwszej operacji po chwili przestoju,
          gdyż dla potoku o długości n etapów minimalny czas oczekiwania wynosi n cykli zegarowych. Jedynym sposobem ograniczenia tego problemu
          jest zwiększanie częstotliwości taktowania układu oraz likwidowanie przerw w przetwarzaniu.</p>
        `
      }
    ]
  },
  {
    "id": 2,
    "name": "Sieci komputerowe",
    "icon": "diagram-3",
    "chance": 5,
    "questions": [
      {
        "id": 0,
        "question": "Porównanie różnych architektur sieci komputerowych",
        "answer": `
          <p><strong>Architektura sieci</strong> – określa sposoby realizacji przekazu informacji pomiędzy urządzeniami końcowymi. Przeważnie
          organizowana warstwowo tworząc warstwową architekturę sieci.</p>
          <p><strong>Ważniejsze architektury warstwowe</strong>:</p>
          <ul>
            <li>ISO-OSI (ISO - Open Systems Interconection),</li>
            <li>TCP/IP (Transmission Control Protocol/Internet Protocol)</li>
            <li>SNA (Systems Network Architecture) firmy IBM,</li>
            <li>DNA (Digital Network Architecture) firmy DEC</li>
          </ul>
          <p><strong>Model OSI</strong> - dwa systemy mogą wymieniać informacje między sobą pod warunkiem, że w obu przypadkach
          zaimplementowano te same protokoły komunikacyjne.</p>
          <p><strong>Warstwy modelu ISO/OSI</strong>:</p>
          <ol>
            <li><strong>APLIKACJI</strong> - zajmuje się specyfikacją interfejsu, który wykorzystują aplikacje do przesyłania danych do sieci;</li>
            <li><strong>PREZENTACJI</strong> - zapewnia tłumaczenie danych, definiowanie ich formatu oraz odpowiednią składnię;</li>
            <li><strong>SESJI</strong> - zadaniem jest zarządzanie przebiegiem komunikacji, podczas połączenia miedzy dwoma komputerami;</li>
            <li><strong>TRANSPORTOWA</strong> - segmentuje dane oraz składa je w tzw. strumień, zapewnia całościowe połączenie między stacjami;</li>
            <li><strong>SIECIOWA</strong> - podstawowe zadania to przesyłanie danych pomiędzy węzłami sieci wraz z wyznaczaniem trasy przesyłu, łączenie bloków informacji w ramki na czas ich przesyłania a następnie stosowny ich podział;</li>
            <li><strong>ŁĄCZA</strong> DANYCH - odpowiada za obsługę ramek, czyli stałej długości pakietów danych, do jej zadań należy wykrywanie wszelkich błędów, które wystąpiły w warstwie fizycznej, oraz ich usuwanie;</li>
            <li><strong>FIZYCZNA</strong> - odpowiedzialna za przesyłanie sygnałów w elektryczny, elektromagnetyczny, mechaniczny, optyczny czy też inny sposób, wykorzystując do tego fizyczne medium komunikacyjne (przewody miedziane, światłowody)</li>
          </ol>
          <p><strong>Model TCP/IP</strong> - podział całego zagadnienia komunikacji sieciowej na szereg współpracujących ze sobą
          warstw. Każda z nich może być tworzona przez programistów niezależnie, jeżeli narzucimy
          pewne protokoły według których wymieniają się one informacjami. Założenia modelu TCP/IP są pod
          względem organizacji warstw zbliżone do modelu OSI. Jednak ilość warstw jest mniejsza i bardziej
          odzwierciedla prawdziwą strukturę Internetu.</p>
          <p><strong>Model TCP/IP składa się z czterech warstw</strong>:</p>
          <ol>
            <li><strong>APLIKACJI</strong> - najwyższy poziom, w którym pracują użyteczne dla człowieka aplikacje takie jak, np. serwer WWW czy przeglądarka internetowa. Obejmuje ona zestaw gotowych protokołów, które aplikacje wykorzystują do przesyłania różnego typu informacji w sieci.</li>
            <li><strong>TRANSPORTOWA</strong> - zapewnia pewność przesyłania danych oraz kieruje właściwe informacje do odpowiednich aplikacji.</li>
            <li><strong>INTERNETU</strong> - przetwarzane są datagramy posiadające adresy IP. Ustalana jest odpowiednia droga do docelowego komputera w sieci.</li>
            <li><strong>DOSTĘPU DO SIECI</strong> - zajmuje się przekazywaniem danych przez fizyczne połączenia między urządzeniami sieciowymi. Najczęściej są to karty sieciowe lub modemy. Dodatkowo warstwa ta jest czasami wyposażona w protokoły do dynamicznego określania adresów IP.</li>
          </ol>
        `
      },
      {
        "id": 1,
        "question": "Funkcje warstwy łącza danych modelu OSI.",
        "answer": `
          <p>Warstwa łącza danych jest odpowiedzialna za końcową zgodność przesyłanych danych, a także upakowywanie instrukcji, danych itp. w tzw. ramki. Ramka jest strukturą rodzimą,
          czyli właściwą dla warstwy łącza danych, która zawiera ilość informacji wystarczającą do pomyślnego
          przesłania danych przez sieć lokalną do ich miejsca docelowego.</p>
          <p>Pomyślna transmisja danych zachodzi wtedy, gdy dane osiągają miejsce docelowe w postaci niezmienionej
          w stosunku do postaci, w której zostały wysłane. Ramka musi więc również zawierać mechanizm
          umożliwiający weryfikowanie integralności jej zawartości podczas transmisji.</p>
          <p>Wysoka jakość transmisji wymaga spełnienia następujących dwóch warunków:</p>
          <ul>
          <li>Węzeł początkowy musi odebrać od węzła końcowego potwierdzenie otrzymania każdej ramki w
          postaci niezmienionej.</li>
          <li>Węzeł docelowy przed wysłaniem potwierdzenia otrzymania ramki musi zweryfikować integralność
          jej zawartości.</li>
          </ul>
          <p>Warstwa łącza danych jest również odpowiedzialna za ponowne składanie otrzymanych z warstwy fizycznej
          strumieni binarnych i umieszczanie ich w ramkach.</p>
        `
      },
      {
        "id": 2,
        "question": "Funkcje warstwy sieci modelu OSI.",
        "answer": `
          <p>Warstwa sieci modelu OSI odpowiada za określenie trasy pomiędzy nadawcą a odbiorcą, a także kierowanie przepływem pakietów w sieci.</p>
          <p>Zapewnia ona, że pakiety przesyłane między komputerami nie
          łączącymi się bezpośrednio, będą przekazywane z komputera na komputer, aż osiągną adresata. Proces
          znajdowania drogi w sieci nazywa się trasowaniem lub rutowaniem. Nie wymaga się aby pakiety pomiędzy
          ustalonymi punktami poruszały się za każdym razem po tej samej drodze. Warstwa ta nie posiada żadnych
          mechanizmów wykrywania oraz korygowania błędów. Dopuszcza się gubienie pakietów przez tę warstwę -
          jest jedynym wyjściem gdy router - węzeł dokonujący trasowania lub pewien segment sieci są przeciążone i
          nie mogą przyjmować nowych pakietów.</p>
        `
      },
      {
        "id": 3,
        "question": "Dynamiczne przydzielanie adresów.",
        "answer": `
        <p>Protokół DHCP opisuje trzy techniki przydzielania adresów IP:</p>
        <ul>
          <li><strong>przydzielanie ręczne</strong> oparte na tablicy adresów MAC oraz odpowiednich dla nich adresów IP. Jest
          ona tworzona przez administratora serwera DHCP. W takiej sytuacji prawo do pracy w sieci mają
          tylko komputery zarejestrowane wcześniej przez obsługę systemu.</li>
          <li><strong>przydzielanie automatyczne</strong>, gdzie wolne adresy IP z zakresu ustalonego przez administratora są
          przydzielane kolejnym zgłaszającym się po nie klientom.</li>
          <li><strong>przydzielanie dynamiczne</strong>, pozwalające na ponowne użycie adresów IP. Administrator sieci nadaje
          zakres adresów IP do rozdzielenia. Wszyscy klienci po starcie systemu automatycznie pobierają
          swoje adresy na pewien czas (lease). Protokół DHCP minimalizuje również możliwe źródła błędów.</li>
        </ul>
        `
      },
      {
        "id": 4,
        "question": "Protokoły połączeniowe i bezpołączeniowe.",
        "answer": `
          <p><strong>Protokół połączeniowy</strong> to protokół komunikacyjny, w
          którym przed rozpoczęciem wymiany komunikatów strony
          muszą nawiązać połączenie, w którego ramach może
          zostać wynegocjowany sam protokół. Po zakończeniu
          łączności połączenie musi ulec likwidacji. W wyniku
          budowy połączenia zostaje utworzona pojedyncza ścieżka
          między stacją nadawczą a stacją odbiorczą. W fazie
          przesyłania dane są transmitowane przez utworzoną
          ścieżkę w sposób sekwencyjny. Dane docierają do stacji
          odbiorczej w kolejności ich wysyłania przez stację
          nadawczą.</p>
          <p><strong>Protokół bezpołączeniowy</strong> - typ protokołu sieciowego,
          umożliwia przesyłanie bez ustanawiania połączenia z odbiorcą. Nie buduje się jedynej ścieżki między stacją
          nadawczą i odbiorczą, pakiety do stacji odbiorczej mogą docierać w innej kolejności niż są wysyłane przez
          stację nadawczą, ze względu na to, że mogą być przesyłane różnymi trasami. W usłudze bezpołączeniowej
          dane przepływają przez trwałe połączenia między węzłami sieci, a każdy pakiet jest obsługiwany
          indywidualnie i niezależnie od innych pakietów danego komunikatu. Jest to możliwe pod warunkiem, że
          każdy pakiet jest kompletnie zaadresowany, to znaczy, że każdy z nich ma swój adres stacji nadawczej i
          stacji odbiorczej.</p>
        `
      },
      {
        "id": 5,
        "question": "Tryby FTP.",
        "answer": `
          <p><strong>FTP działa w dwóch trybach: aktywnym i pasywnym</strong>, w zależności od tego, w jakim jest trybie, używa
          innych portów do komunikacji.</p>
          <ul>
            <li>jeżeli połączenie FTP działa w trybie aktywnym to używa portu 21 dla poleceń – zestawiane przez
            klienta i portu 20 do przesyłu danych – zestawiane przez serwer;</li>
            <li>jeżeli połączenie FTP pracuje w trybie pasywnym to wykorzystuje port 21 dla poleceń i port o
            numerze powyżej 1024 do transmisji danych – obydwa połączenia zestawiane są przez klienta.</li>
          </ul>
          <p><strong>Aktywny</strong> tryb FTP jest wygodny dla administratora serwera FTP, jednak kłopotliwy dla klienta. Serwer FTP
          stara się nawiązać połączenie z nieuprzywilejowanym portem klienta, co najprawdopodobniej zostanie
          udaremnione przez firewall po stronie klienta.</p>
          <p>Tryb <strong>pasywny</strong> jest wygodny dla klienta, lecz kłopotliwy dla administratora serwera FTP. Oba połączenia
          nawiązuje klient, ale jedno z nich dotyczy nieuprzywilejowanego portu, i najprawdopodobniej będzie
          zablokowane przez firewall po stronie serwera.</p>
        `
      }
    ]
  },
  {
    "id": 3,
    "name": "Bazy danych",
    "icon": "server",
    "chance": 100,
    "questions": [
      {
        "id": 0,
        "question": "Podstawowe pojęcia baz danych. Baza Danych. Funkcje bazy danych. Właściwości bazy danych. Modele baz danych.",
        "answer": `
          <p><strong>Baza danych</strong> – zbiór informacji opisujący wybrany fragment rzeczywistości.</p>
          <p><strong>Właściwości baz danych</strong>:</p>
          <ul>
            <li>współdzielenie danych – wielu użytkowników tej samej bazy,</li>
            <li>integracja danych – baza nie powinna mieć powtarzających się, bądź zbędnych danych,</li>
            <li>niezależność danych – dane niezależnie od aplikacji wykorzystujących te dane.</li>
            <li>integralność danych – dokładne odzwierciedlenie rzeczywistości,</li>
            <li>trwałość danych – dane przechowywane przez pewien czas,</li>
            <li>bezpieczeństwo danych – dostęp do bazy lub jej części przez upoważnionych użytkowników,</li>
          </ul>
          <p><strong>Funkcje bazy danych</strong>:</p>
          <ul>
            <li>aktualizujące – dokonują zmian na danych,</li>
            <li>zapytań – wydobywają dane z bazy danych.</li>
          </ul>
          <p><strong>Model baz danych</strong> to zbiór zasad (specyfikacji), opisujących strukturę danych w bazie danych.
          Określane są również dozwolone operacje.</p>
          <ul>
            <li><strong>Model hierarchiczny</strong>: W modelu hierarchicznym dane są przechowywane na zasadzie rekordów
            nadrzędnych-podrzędnych, tzn. rekordy przypominają strukturę drzewa. Każdy rekord (z wyjątkiem
            głównego) jest związany z dokładnie jednym rekordem nadrzędnym.</li>
            <li><strong>Model relacyjny</strong>: Dane w takim modelu przechowywane są w tabelach, z których każda ma stałą
            liczbę kolumn i dowolną liczbę wierszy. Każda tabela (relacja) ma zdefiniowany klucz danych (key) -
            wyróżniony atrybut lub kilka takich atrybutów, którego wartość jednoznacznie identyfikuje dany wiersz.</li>
            <li><strong>Model obiektowy</strong>: Dane w tym modelu przechowywane są jako instancje/obiekty zdefiniowanych
            klas. Każda klasa może mieć zdefiniowany unikalny zbiór atrybutów, a również super klasę (przodka) po
            którym dziedziczy atrybuty. Obiekty jednej klasy posiadają ten sam zbiór atrybutów a różnić się mogą tylko
            ich wartościami.</li>
            <li><strong>Model sieciowy</strong>: Model danych operujący pojęciami typów rekordów i typów kolekcji (opisów
            związków “jeden do wielu” między dwoma typami rekordów). Związki asocjacyjne pomiędzy danymi są
            reprezentowane poprzez powiązania wskaźnikowe. Struktura danych tworzy więc graf, czyli sieć.</li>
          </ul>
        `
      },
      {
        "id": 1,
        "question": "System zarządzania bazami danych. Funkcje systemu. Przykłady SZDB.",
        "answer": `
          <p><strong>System zarządzania bazą danych, SZBD</strong> – oprogramowanie
          bądź system informatyczny służący do zarządzania bazą danych.</p>
          <p><strong>Funkcje SZBD</strong>:</p>
          <ul>
            <li><strong>optymalizacja zapytań</strong> – takie przekształcenie zapytań kierowanych do bazy, aby oczekiwanie na
            odpowiedź było możliwie najkrótsze</li>
            <li><strong>zapewnienie integralności danych</strong> – uniemożliwienie przejścia bazy do stanu, który nie istnieje w
            modelowanej rzeczywistości</li>
            <li><strong>zarządzanie współbieżnym dostępem wielu użytkowników</strong>, tak aby ich działania nie kolidowały
            ze sobą</li>
            <li><strong>odporność na awarie</strong> – możliwość odtworzenia poprawnego stanu bazy po wystąpieniu awarii</li>
            <li><strong>ochrona danych</strong> – uniemożliwienie dostępu do danych bez autoryzacji</li>
          </ul>
          <p><strong>Przykłady SZBD</strong>:</p>
          <ul>
            <li>MySQL: silniki MyISAM, InnoDB</li>
            <li>Access: silnik Microsoft Jet</li>
            <li>NoSQL: silnik MongoDB</li>
          </ul>
        `
      },
      {
        "id": 2,
        "question": "Model relacyjny baz danych. Relacje, klucze główne i obce, integralność referencyjna.",
        "answer": `
          <p><strong>Model relacyjny</strong> – model organizacji danych bazujący na matematycznej teorii mnogości, w szczególności
          na pojęciu relacji. Reprezentacją relacji jest dwuwymiarowa tabela złożona z kolumn (atrybutów) i wierszy
          (krotek). W modelu relacyjnym przyjmuje się następujące założenia o tabeli:</p>
          <ul>
            <li>Liczba kolumn (atrybutów) jest z góry ustalona</li>
            <li>Z każdą kolumną (atrybutem) jest związana jej nazwa oraz dziedzina, określająca zbiór wartości, jakie
            mogą występować w danej kolumnie</li>
            <li>Na przecięciu wiersza i kolumny znajduje się pojedyncza (atomowa) wartość należąca do dziedziny
            kolumny</li>
            <li>Wiersz (krotka) reprezentuje jeden rekord informacji</li>
            <li>W modelu relacyjnym kolejność wierszy (krotek) może się zmieniać</li>
          </ul>
          <p><strong>Relacje</strong> - związki między tabelami:
          <ul>
            <li><strong>Jeden – do – wielu</strong> (1 - ∞) wierszowi (krotce) w tabeli A może odpowiadać wiele zgodnych wierszy
            (krotek) w tabeli B, ale wierszowi w tabeli B może odpowiadać tylko jeden zgodny wiersz w tabeli A</li>
            <li><strong>Wiele – do – wielu</strong> (∞ - ∞) wierszowi (krotce) w tabeli A może odpowiadać wiele zgodnych wierszy
            (krotek) w tabeli B i na odwrót. Relacje taką tworzy się definiując trzecią tabelę, zwaną tabelą
            skrzyżowań, której klucz podstawowy zawiera zarówno klucz obcy z tabeli A, jak i z tabeli B.</li>
            <li><strong>Jeden – do – jednego</strong> (1 - 1) wierszowi (krotce) w tabeli A może odpowiadać nie więcej niż jeden
            zgodny wiersz (krotka) w tabeli B i na odwrót. Relacja jeden-do-jednego jest tworzona, jeśli obie
            powiązane kolumny są kluczami podstawowymi lub mają ograniczenia UNIQUE. </li>
          </ul>
          <p><strong>Klucz główny</strong> - dla każdej tabeli w modelu relacyjnym musi być określony, jednoznaczny identyfikator,
          nazywany kluczem głównym (podstawowym). Klucz główny składa się z jednej lub ze zbioru kolumn
          (atrybutów), w których wartości w jednoznaczny sposób identyfikują cały wiersz (krotkę). Oznacza to, że
          wartości znajdujące się w kolumnie będącej kluczem głównym nie mogą się powtarzać i muszą być
          unikatowe.</p>
          <p><strong>Klucz obcy</strong> - jest to zbiór złożony z jednej kolumny lub więcej kolumn, w których wartości występują, jako
          wartości ustalonego klucza głównego lub jednoznacznego w tej samej lub innej tabeli i są interpretowane,
          jako wskaźniki do wierszy w tej drugiej tabeli.</p>
          <p><strong>Zasada integralności referencyjnej</strong> w relacyjnej bazie danych wymaga, aby wartości klucza obcego tabeli
          podrzędnej były puste (null) lub odpowiadały wartościom klucza podstawowego tabeli nadrzędnej.</p>
        `
      },
      {
        "id": 3,
        "question": "Modelowanie baz danych. Diagram związków encji.",
        "answer": `
          <p><strong>Model bazy danych</strong> to zbiór zasad (specyfikacji), opisujących strukturę danych w bazie danych. Definiuje
          się strukturę danych poprzez specyfikację reprezentacji dozwolonych w modelu obiektów oraz ich związków.</p>
          <p><strong>Hierarchiczny model danych</strong> W modelu hierarchicznym dane są przechowywane na zasadzie rekordów
          nadrzędnych-podrzędnych, tzn. rekordy przypominają strukturę drzewa. Każdy rekord (z wyjątkiem
          głównego) jest związany z dokładnie 1 rekordem nadrzędnym. Przykładem takiego modelu może być
          struktura katalogów na dysku twardym komputera.</p>
          <p><strong>Relacyjny model danych (RDBMS)</strong> Dane w takim modelu przechowywane są w tabelach, z których każda
          ma stałą liczbę kolumn i dowolną liczbę wierszy. Każda tabela (relacja) ma zdefiniowany klucz danych (key) -
          wyróżniony atrybut lub kilka takich atrybutów, którego wartość jednoznacznie identyfikuje dany wiersz.</p>
          <p>Specyfikując schemat klasy obiektów w języku ODL, opisujemy trzy rodzaje właściwości obiektów:</p>
          <ul>
            <li>Atrybuty - przyjmują wartości typów pierwotnych takich jak całkowity lub tekstowy, albo typów złożonych</li>
            <li>powstających z pierwotnych;</li>
            <li>Związki - referencje do obiektów pewnej klasy, albo kolekcje (zbiory) takich referencji;</li>
            <li>Metody - funkcje operujące na obiektach danej klasy.</li>
          </ul>
          <p><strong>Diagram Związków Encji</strong></p>
          <p>Modele danych można opracowywać na różnych poziomach szczegółowości wykorzystując technikę
          diagram związków encji.</p>
          <ul>
            <li><strong>Encja</strong> - Rzecz mająca znaczenie, rzeczywista lub wymyślona, o której informacje należy znać lub przechowywać.</li>
            <li><strong>Atrybut</strong> - Element informacji służący do klasyfikowania, identyfikowania, kwalifikowania, określania ilości lub wyrażania stanu encji.</li>
            <li><strong>Związek</strong> - Znaczący sposób, w jaki mogą być ze sobą powiązane dwie rzeczy tego samego typu lub różnych typów.</li>
          </ul>
          <p><strong>Uwaga</strong>: encje opisuje się za pomocą rzeczowników lub wyrażeń rzeczownikowych w liczbie pojedynczej.</p>
        `
      },
      {
        "id": 4,
        "question": "Język baz danych SQL. Podjęzyki DDL, DML, DCL.",
        "answer": `
          <ol>
            <li><strong>SQL interakcyjny</strong> wykorzystywany jest przez użytkowników w celu bezpośredniego pobierania lub
            wprowadzania informacji do bazy. Przykładem może być zapytanie prowadzące do uzyskania zestawienia
            aktywności kont w miesiącu. Wynik jest wówczas przekazywany na ekran, z ewentualną opcją
            przekierowania go do pliku lub drukarki.</li>
            <li><strong>Statyczny kod SQL (Static SQL)</strong> nie ulega zmianom i pisany jest wraz z całą aplikacją, podczas której
            pracy jest wykorzystywany. Nie ulega zmianom w sensie zachowania niezmiennej treści instrukcji, które
            jednak zawierać mogą odwołania do zmiennych lub parametrów przekazujących wartości z lub do aplikacji.
            Statyczny SQL występuje w dwóch odmianach.</li>
            <ol class="pl-8">
              <li><strong>Embedded SQL (Osadzony SQL)</strong> oznacza włączenie kodu SQL do kodu źródłowego innego
              języka. Większość aplikacji pisana jest w takich językach jak C++ czy Java, jedynie odwołania do bazy
              danych realizowane są w SQL. W tej odmianie statycznego SQL-a do przenoszenia wartości
              wykorzystywane są zmienne.</li>
              <li><strong>Język modułów</strong>. W tym podejściu moduły SQL łączone są z modułami kodu w innym języku.
              Moduły kodu SQL przenoszą wartości do i z parametrów, podobnie jak to się dzieje przy
              wywoływaniu podprogramów w większości języków proceduralnych. Jest to pierwotne podejście,
              zaproponowane w standardzie SQL. Embedded SQL został do oficjalnej specyfikacji włączony nieco
              później.</li>
            </ol>
            <li><strong>Dynamiczny kod SQL (Dynamic SQL)</strong> generowany jest w trakcie pracy aplikacji. Wykorzystuje się go w
            miejsce podejścia statycznego, jeżeli w chwili pisania aplikacji nie jest możliwe określenie treści potrzebnych
            zapytań – powstaje ona w oparciu o decyzje użytkownika. Tę formę SQL generują przede wszystkim takie
            narzędzia jak graficzne języki zapytań. Utworzenie odpowiedniego zapytania jest tu odpowiedzią na
            działania użytkownika.</li>
          </ol>
          <p><strong>DML</strong> (Data Manipulation Language) służy do wykonywania operacji na danych – do ich umieszczania w
          bazie, kasowania, przeglądania, zmiany. Najważniejsze polecenia z tego zbioru to:</p>
          <p><strong>* SELECT * INSERT * UPDATE * DELETE</strong></p>
          <p><strong>DDL</strong> (Data Definition Language) można operować na strukturach, w których dane są przechowywane – czyli
          np. dodawać, zmieniać i kasować tabele lub bazy. Najważniejsze polecenia tej grupy to:</p>
          <p><strong>* CREATE * DROP * ALTER</strong></p>
          <p><strong>DCL</strong> (Data Control Language) ma zastosowanie do nadawania uprawnień do obiektów bazodanowych.
          Najważniejsze polecenia w tej grupie to:</p>
          <p><strong>* GRANT * REVOKE</strong></p>
        `
      },
      {
        "id": 5,
        "question": "Instrukcja SELECT.",
        "answer": `
          <p>Polecenie <strong>SELECT</strong> nazywa się także „zapytaniem”. Polecenie to pobiera krotki z relacyjnej bazy danych i
          zwraca w postaci zbioru odczytanych krotek (zbiór taki może być też pusty), opcjonalnie może je
          przetworzyć przed zwróceniem. Mówiąc prościej polecenie SELECT służy do odczytywania danych z bazy
          danych i dokonywaniu na nich prostych obliczeń i przekształceń.</p>
          <p>SELECT [DISTINCT] {atrybut1, atrybut2, ....}</p>
          <p>FROM {nazwa relacji}</p>
          <p>[WHERE {waruek}] [ORDER BY {{wyrażenie5 [ASC|DESC], alias1 [ASC|DESC]}] [LIMIT {limit}];</p>
        `
      },
      {
        "id": 6,
        "question": "Instrukcje DDL i DCL.",
        "answer": `
          <p><strong>DDL</strong> (Data Definition Language - Język definiowania danych) definiuje struktury danych, na których operują instrukcje DML.</p>
          <ul>
            <li><strong>CREATE</strong> (np. CREATE TABLE, CREATE DATABASE, ...) – utworzenie struktury (bazy, tabeli, indeksu itp.),</li>
            <li><strong>DROP</strong> (np. DROP TABLE, DROP DATABASE, ...) – usunięcie struktury,</li>
            <li><strong>ALTER</strong> (np. ALTER TABLE ADD COLUMN ...) – zmiana struktury (dodanie kolumny do tabeli, zmiana typu danych w kolumnie tabeli).</li>
          </ul>
          <p><strong>DCL</strong> (Data Control Language - Język kontrolowania danych) ma zastosowanie do nadawania uprawnień do obiektów bazodanowych.</p>
          <ul>
            <li><strong>GRANT</strong> – (GRANT {ALL | lista_uprawnień} TO użytkownik})-przyznaje użytkownikowi uprawnienia</li>
            <li><strong>REVOKE</strong> – (REVOKE {ALL | lista_uprawnień} TO użytkownik})-zabiera uprawnienia, które zostały wcześniej przyznane</li>
            <li><strong>DENY</strong> - (DENY {ALL | lista_uprawnień} TO użytkownik})-Bezpośrednio zabiera uprawnienia</li>
          </ul>
        `
      },
      {
        "id": 7,
        "question": "Indeksy w bazach danych, podział indeksów, B+ drzewo.",
        "answer": `
          <p>Indeks jest mechanizmem poprawienia szybkości dostępu do danych bez zmiany struktury ich
          przechowywania. Indeks zazwyczaj jest założony na jednym polu rekordu danych.
          Rekord indeksu ma strukturę: <wartość pola, adres w pliku danych> i jest zapisywany do pliku indeksu w
          kolejności zgodnej z wartością pola indeksowanego.</p>
          <p><strong>Indeksy dzielimy na</strong>:</p>
          <ul>
            <li><strong>indeks główny</strong> – założony na atrybucie porządkującym unikalnym,</li>
            <li><strong>grupujący</strong> – założony na atrybucie porządkującym nieunikalnym,</li>
            <li><strong>pomocniczy</strong> – założony na atrybucie nieporządkującym.</li>
            <li><strong>Indeks gęsty</strong> - posiada rekord indeksu dla każdego rekordu indeksowanego pliku danych</li>
            <li><strong>Indeks rzadki</strong> - posiada rekordy tylko dla wybranych rekordów indeksowanego pliku danych (wskazuje na blok rekordów, a nie poszczególne rekordy)</li>
          </ul>
          <p>W miarę wzrostu rozmiarów indeksu wydłuża się czas wyszukiwania po tym indeksie, dlatego wiele
          systemów zarządzania bazą danych stosuje pewną formę indeksu wielopoziomowego:</p>
          <ul>
            <li>statyczne wielopoziomowe indeksy – modyfikowanie zawartości pliku z danymi powoduje, że struktura indeksu staje się nieefektywna</li>
            <li>dynamiczne wielopoziomowe indeksy – B+ Drzewo</li>
          </ul>
          <p><strong>B+ drzewo</strong> jest zrównoważoną strukturą drzewiastą, w której wierzchołki wewnętrzne służą do
          wspomagania wyszukiwania, natomiast wierzchołki liści zawierają rekordy indeksu ze wskaźnikami do
          rekordów w plikach danych. Zrównoważenie struktury oznacza, że odległość (liczba poziomów) od korzenia
          do dowolnego liścia jest zawsze taka sama. W celu zapewnienia odpowiedniej efektywności realizacji
          zapytań przedziałowych wierzchołki liści stanowią listę dwukierunkową.</p>
          <p><strong>CREATE INDEX [nazwa indeksu] ON [nazwa tabeli] ([nazwa kolumny])</strong></p>
        `
      },
      {
        "id": 8,
        "question": "Normalizacja, cel normalizacji, postać normalna Boyce'a-Codda.",
        "answer": `
          <p><strong>Normalizacja</strong> – proces mający na celu eliminację powtarzających się danych w relacyjnej bazie danych. Dążymy w niej do zwiększenia elastyczności bazy danych poprzez wyeliminowanie nadmiarowości (jakieś dane są niepotrzebnie powtarzane w wielu miejscach bazy danych) i niespójnych zależności (np. sytuacja gdy adres studenta znajdowałby się w tabeli "Zaliczenie"). Pola powinny dodatkowo przechowywać dane atomowe (czyli wartości danego atrybutu nie mogą być listami, macierzami i innymi „bytami” posiadającymi jakąś szerszą strukturę). Wszystkie kolumny tabeli powinny być zależne tylko od jej klucza głównego (najczęściej stosowana jest kolumna z ID danego obiektu), a wykonywanie operacji na danych nie powinno skutkować anomaliami (np. sytuacja, że modyfikacja jednego rekordu wymusza na nas wprowadzenie zmian w innych rekordach, anomalie redundancji, usuwania, dodawania, modyfikowania).</p>
          <p>Istnieją trzy główne reguły normalizacji (pierwsza, druga oraz trzecia forma normalna) oraz dodatkowo, wyróżnia się także czwartą oraz piątą formę normalną, które są z reguły bardzo rzadko wykorzystywane w praktycznych projektach.</p>
          <p>Relacja jest w postaci normalnej Boyce’a – Codda wtedy i tylko wtedy, gdy dla każdej zależności nietrywialnej A1,…,An → B zbiór {A1,…,An} jest nadkluczem tej relacji.</p>
        `
      },
      {
        "id": 9,
        "question": "Transakcje, własności transakcji.",
        "answer": `
          <p>Transakcja jest sekwencją logicznie powiązanych operacji na bazie danych, która przeprowadza bazę danych
          z jednego stanu spójnego w inny stan spójny. Typy operacji na bazie danych obejmują: odczyt i zapis
          danych oraz zakończenie połączone z akceptacja (zatwierdzeniem) lub wycofaniem transakcji.</p>
          <p><strong>Własności transakcji</strong>:</p>
          <ul>
            <li><strong>atomowość</strong> - zbiór operacji wchodzących w skład transakcji jest niepodzielny, to znaczy albo zostaną wykonane wszystkie operacje transakcji albo żadna.</li>
            <li><strong>spójność</strong> - transakcja przeprowadza bazę danych z jednego stanu spójnego do innego stanu spójnego. W trakcie wykonywania transakcji baza danych może być przejściowo niespójna. Transakcja nie może naruszać ograniczeń integralnosciowych.</li>
            <li><strong>izolacja</strong> - transakcje są od siebie logicznie odseparowane. Transakcje oddziałują na siebie poprzez dane. Mimo współbieżnego wykonywania, transakcje widzą stan bazy danych tak, jak gdyby były wykonywane w sposób sekwencyjny.</li>
            <li><strong>trwałość</strong> - wyniki zatwierdzonych transakcji nie mogą zostać utracone w wyniku wystąpienia awarii systemu. Zatwierdzone dane w bazie danych, w przypadku awarii, musza być odtwarzalne.</li>
          </ul>
        `
      }
    ]
  },
  {
    "id": 4,
    "name": "Podstawy elektroniki i miernictwo elektroniczne",
    "icon": "lightning",
    "chance": 3,
    "questions": [
      {
        "id": 0,
        "question": "Diody półprzewodnikowe. Tranzystory.",
        "answer": `
          <p><strong>Diodą półprzewodnikową</strong> nazywamy element wykonany z półprzewodnika (np. krzem, german),
          zawierającego jedno złącze – najczęściej p-n z dwiema końcówkami wyprowadzeń.</p>
          <p>Dioda <strong>przepuszcza prąd w jednym kierunku</strong> (od anody do katody), natomiast
          w kierunku przeciwnym - w minimalnym stopniu
          Diody stosowane są w układach analogowych i cyfrowych.
          Diody półprzewodnikowe stosuje się w układach prostowania prądu zmiennego,
          w układach modulacji i detekcji, przełączania, generacji i wzmacniania sygnałów
          elektrycznych.</p>
          <p>Każda dioda ma pewną <strong>częstotliwość graniczną</strong>, po przekroczeniu której nie
          zachowuje się jak dioda, lecz jak kondensator</p>
          <p>Tranzystor posiada trzy końcówki przyłączone do warstw półprzewodnika,
          nazywane: <strong>emiter</strong>, <strong>baza</strong>, <strong>kolektor</strong></p>
          <p>Są wykorzystywane do budowy wzmacniaczy różnego rodzaju. Z tranzystorów buduje się także bramki
          logiczne realizujące podstawowe funkcje boolowskie, Tranzystory są także podstawowym budulcem wielu
          rodzajów pamięci półprzewodnikowych (RAM, ROM itp.)</p>
        `
      },
      {
        "id": 1,
        "question": "Układy scalone.",
        "answer": `
          <p>Układ scalony jest zminiaturyzowanym układem elektronicznym, który może zawierać w sobie miliony
          elementów elektronicznych. Płytki krzemowe bo na nich najczęściej budowane są układy scalone stanowią
          podłoże półprzewodnikowe dla elementów elektronicznych jak diody, kondensatory, tranzystory lub
          rezystory.</p>
          <p>Ze względu na sposób wykonania układy scalone dzieli się na główne grupy:</p>
          <ul>
            <li>monolityczne, w których wszystkie elementy, zarówno elementy czynne jak i bierne, wykonane są w
            monokrystalicznej strukturze półprzewodnika. Większość stosowanych obecnie układów scalonych
            jest wykonana właśnie w tej technologii.</li>
            <li>hybrydowe – na płytki wykonane z izolatora nanoszone są warstwy przewodnika oraz materiału
            rezystywnego, które następnie są wytrawiane, tworząc układ połączeń elektrycznych oraz rezystory.
            Do tak utworzonych połączeń dołącza się indywidualne, miniaturowe elementy elektroniczne (w tym
            układy monolityczne).</li>
          </ul>
          <p>W najnowszych technologiach, w których między innymi produkowane są procesory Intel i AMD, minimalna
          długość bramki wynosi 22nm.</p>
        `
      },
      {
        "id": 2,
        "question": "Układy impulsowe.",
        "answer": `
          <p>Układ sterowania lub regulacji, w którym sygnały wejściowe (sterujące), a często również sygnały wyjściowe
          (wielkości sterowane) mogą zmieniać swoje wartości tylko w pewnych chwilach czasu — uzależnionych od
          różnych czynników (np. gdy sygnał wejściowy przekracza pewną wartość).</p>
          <p>Układy dyskretne opisywane są za pomocą równań różnicowych. Transmitancja operatorowa układów
          dyskretnych opiera się o przekształcenie Z. Transmitancją impulsową układu dyskretnego nazywa się
          stosunek transformaty Z odpowiedzi układu do transformaty Z sygnału wejściowego.</p>
        `
      },
      {
        "id": 3,
        "question": "Układy cyfrowe.",
        "answer": `
          <p><strong>Układy cyfrowe</strong> to rodzaj układów elektronicznych, w których sygnały napięciowe przyjmują tylko
          określoną liczbę poziomów, którym przypisywane są wartości liczbowe. Najczęściej (choć nie zawsze) liczba
          poziomów napięć jest równa dwa, a poziomom przypisywane są cyfry 0 i 1, wówczas układy cyfrowe
          realizują operacje zgodnie z algebrą Boole'a i z tego powodu nazywane są też <strong>układami logicznymi</strong>.
          Obecnie układy cyfrowe budowane są w oparciu o bramki logiczne realizujące elementarne operacje znane
          z algebry Boola: iloczyn logiczny (AND, NAND), sumę logiczną (OR, NOR), negację NOT, różnicę
          symetryczną (XOR) itp.</p>
          <p>Zalety układów cyfrowych:</p>
          <ul>
            <li>Możliwość <strong>bezstratnego</strong> kodowania i przesyłania informacji</li>
            <li>Zapis i przechowywanie informacji cyfrowej jest prostsze.</li>
            <li>Mniejsza wrażliwość na zakłócenia elektryczne.</li>
            <li>Możliwość tworzenia układów programowalnych, których działanie określa program komputerowy</li>
          </ul>
          <p>Wady układów cyfrowych:</p>
          <ul>
            <li>Są skomplikowane zarówno na poziomie elektrycznym, jak i logicznym i obecnie ich projektowanie wspomagają komputery</li>
            <li>Chociaż są bardziej odporne na zakłócenia, to wykrywanie przekłamań stanów logicznych wymaga dodatkowych zabezpieczeń (patrz: kod korekcyjny) i też nie zawsze jest możliwe wykrycie błędu.</li>
          </ul>
          <p>Ze względu na sposób przetwarzania informacji rozróżnia się:</p>
          <ul>
            <li><strong>układy kombinacyjne</strong> – układy „bez pamięci”, w których sygnały wyjściowe są zawsze takie same dla określonych sygnałów wejściowych;</li>
            <li><strong>układy sekwencyjne</strong> – układy „z pamięcią”, w których stan wyjść zależy nie tylko od aktualnego stanu wejść, ale również od stanów poprzednich.</li>
          </ul>
        `
      },
      {
        "id": 4,
        "question": "Pamięci półprzewodnikowe, magnetyczne, optyczne.",
        "answer": `
          <p>Pamięci półprzewodnikowe, takie jak RAM, EEPROM, charakteryzuje:<p>
          <ul>
            <li>Bardzo szybkie – czas dostępu rzędu ns (nanosekund)</li>
            <li>Bardzo duży transfer danych – rzędu GB/s (Gigabajtów na sekundę)</li>
            <li>Duża cena za jednostkę pojemności</li>
            <li>Możliwość bezpośredniego sprzężenia z mikroprocesorami</li>
            <li>Utrata informacji po odłączeniu zasilania (RAM)</li>
            <li>Tylko do odczytu (ROM)</li>
            <li>Trwały odczyt i zapis (EEPROM, Flash)</li>
          </ul>
          <p>Pamięci magnetyczne, takie jak dysk twardy, taśma magnetyczna, i pamięci optyczne, takie jak dysk CDROM, taśma filmowa, charakteryzuje:</p>
          <ul>
            <li>Wolne – duży czas dostępu rzędu ms (milisekund)</li>
            <li>Średni transfer danych – rzędu dziesiątek MB/s (megabajtów na sekundę)</li>
            <li>Niewielka cena za jednostkę pojemności</li>
            <li>Wymagają specjalnych układów (interfejsowych – pośredniczących)</li>
            <li>Podtrzymują dane bez zasilania</li>
            <li>Możliwość realizacji jako pamięci wymiennych – nośnik jest oddzielony od czytnika</li>
          </ul>
        `
      },
      {
        "id": 5,
        "question": "Przetworniki analogowo-cyfrowe i cyfrowo-analogowe",
        "answer": `
          <p><strong>Przetwornik C/A przetwarzający sygnał cyfrowy</strong> na sygnał analogowy w postaci prądu elektrycznego lub
          napięcia o wartości proporcjonalnej do tej liczby (równoważny sygnał analogowy).
          Taki przetwornik ma n wejść i jedno wyjście.</p>
          <p><strong>Przetwornik A/C</strong> to układ służący do zamiany sygnału analogowego (ciągłego) na reprezentację cyfrową
          (sygnał cyfrowy). Dzięki temu możliwe jest przetwarzanie ich w urządzeniach elektronicznych opartych o
          architekturę zero-jedynkową oraz gromadzenie na dostosowanych do tej architektury nośnikach danych.
          Proces ten polega na uproszczeniu sygnału analogowego do postaci skwantowanej (dyskretnej), czyli
          zastąpieniu wartości zmieniających się płynnie do wartości zmieniających się skokowo w odpowiedniej skali
          (dokładności) odwzorowania.</p>
        `
      },
      {
        "id": 6,
        "question": "Metody pomiarów wielkości elektrycznych.",
        "answer": `
          <p>Do wykonania pomiarów elektrycznych niezbędne są odpowiednie przyrządy pomiarowe:</p>
          <ol>
            <li><strong>woltomierz</strong> - miernik napięcia, podłączany równolegle</li>
            <li><strong>amperomierz</strong> - miernik natężenia, podłączamy szeregowo, obwód zamknięty</li>
            <li><strong>omomierz</strong> - miernik oporu, podłączamy równolegle, obwód otwarty</li>
            <li><strong>watomierz</strong> - miernik mocy, podłączamy szeregowo gdy prądowy obwód, równolegle gdy napięciowy obwód
          </ol>
        `
      },
      {
        "id": 7,
        "question": "Generatory kwarcowe - czas i częstotliwość w komputerze",
        "answer": `
          <p><strong>Generatory kwarcowe</strong> - generatory drgań elektrycznych małej mocy, ale wielkiej częstości,
          charakteryzujące się niezwykle dużą stabilnością drgań. Wykorzystano w nich własności piezoelektryczne
          kryształu kwarcu. Umożliwiają wytwarzanie sygnału o częstotliwościach od 10 kHz do 200 MHz. Taki typ
          generatora jest stosowany w układach taktujących, układach impulsowych, we wzorcach częstotliwości i
          czasu, a także w zegarach kwarcowych.</p>
          <p><strong>Czas i częstotliwość w komputerze</strong>: Częstotliwość przerwań zegarowych zależna jest od sposobu
          zaprogramowania licznika. Tak, więc im wyższa częstotliwość przerwań zegarowych tym większa dokładność
          pomiaru czasu w systemie. Jednak częstotliwość taka, nie może być też zbyt wysoka, gdyż obsługa
          przerwania zegarowego absorbuje określony ułamek mocy procesora. Częstotliwość przerwań zegarowych
          jest kompromisem pomiędzy dokładnością pomiaru czasu, a narzutem na obsługę przerwań zegarowych.
          W większości stosowanych obecnie systemów częstotliwość przerwań leży pomiędzy 10Hz, a 1000Hz.</p>
        `
      }
    ]
  },
  {
    "id": 5,
    "name": "Matematyka dyskretna",
    "icon": "emoji-angry",
    "chance": 99,
    "questions": [
      {
        "id": 0,
        "question": "Grafy. Grafy eulerowskie i hamiltonowskie.",
        "answer": `
          <p><strong>Graf</strong> to zbiór wierzchołków, które mogą być połączone krawędziami, w taki sposób, że każda krawędź
          kończy się i zaczyna w którymś z wierzchołków.</p>
          <p><strong>Graf hamiltonowski</strong> to graf zawierający ścieżkę (drogę) przechodzącą przez <strong>każdy wierzchołek dokładnie
          jeden raz</strong> zwaną ścieżką Hamiltona. W szczególności grafem hamiltonowskim jest graf zawierający cykl
          Hamiltona, tj. zamkniętą ścieżkę Hamiltona. W niektórych źródłach graf zawierający tylko ścieżkę
          Hamiltona nazywany jest grafem półhamiltonowskim.</p>
          <p><strong>Graf eulerowski, graf Eulera</strong> – graf eulerowski odznacza się tym, że da się w nim skonstruować cykl Eulera,
          czyli drogę, która przechodzi przez każdą jego <strong>krawędź dokładnie raz i wraca do punktu wyjściowego</strong>.
          Liczba krawędzi stykających się z danym wierzchołkiem nazywana jest jego stopniem. Jeżeli wszystkie
          wierzchołki grafu nieskierowanego mają stopień parzysty, to znaczy, że da się skonstruować zamkniętą
          ścieżkę Eulera nazywaną cyklem Eulera. Jeżeli najwyżej dwa wierzchołki mają nieparzysty stopień, to
          możliwe jest zbudowanie tylko takiej ścieżki Eulera, która nie jest zamknięta. Graf zawierający cykl Eulera
          jest nazywany grafem eulerowskim, a graf posiadający jedynie ścieżkę Eulera nazywany jest
          półeulerowskim.</p>
          <p><strong>Graf skierowany</strong> - ruch może odbywać się tylko w kierunkach wyznaczonych przez krawędzie. Poruszanie
          się "pod prąd" jest zabronione. Każdy wierzchołek posiada pewną liczbę krawędzi wejściowych nazywaną
          stopniem wchodzącym. Analogicznie ilość krawędzi wychodzących to stopień wychodzący. Skierowany
          graf eulerowski definiowany jest jako graf silnie spójny, w którym dla każdego wierzchołka grafu liczba
          krawędzi wchodzących jest równa ilości krawędzi wychodzących.</p>
        `
      },
      {
        "id": 1,
        "question": "Kolorowanie grafów, definicja liczby chromatycznej grafu i indeksu chromatycznego.",
        "answer": `
          <p><strong>Kolorowanie grafu</strong> polega na przyporządkowaniu koloru każdemu węzłowi w grafie przy wykorzystaniu jak
          najmniejszej liczby kolorów (liczby chromatycznej). Jednakże przy założeniu, że dwa różne ale przyległe do
          siebie węzły nie będą miały tego samego koloru. Takie kolorowanie stosuje się często na mapach do
          oznaczenia terenów lub do określenia jak składować chemikalia bo jak wiadomo niektóre substancje po
          zetknięciu się ze sobą mogą spowodować eksplozje.</p>
          <p><strong>Liczba chromatyczna</strong> - jest to liczba kolorów (liczb) niezbędna do optymalnego klasycznego
          (wierzchołkowego) pokolorowania grafu, czyli najmniejsza możliwa liczba k taka, że możliwe jest legalne
          pokolorowanie wierzchołków grafu k kolorami. Oznacza się ją symbolem χ(G) (chi).</p>
          <p><strong>Indeks chromatyczny grafu</strong> określa minimalną liczbę kolorów wystarczającą do prawidłowego
          pokolorowania krawędzi grafu. Indeks chromatyczny grafu jest równy liczbie chromatycznej jego grafu
          krawędziowego.</p>
        `
      },
      {
        "id": 2,
        "question": "Ciąg Fibonacciego. Definicja rekurencyjna i wzór ogólny.",
        "answer": `
          <p>Definicja rekurencyjna ciągu Fibonacciego:</p>
          <img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/5662c603b1cc30912599180f12fe0d29701c1448" style="filter: invert(1);">
          <p>Wzór ogólny ciągu Fibonacciego:</p>
          <img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/0d64dd7cfcfe1c49387101a0babfa823ffc24048" style="filter: invert(1);">
        `
      },
      {
        "id": 3,
        "question": "Grafy planarne.",
        "answer": `
          <p><strong>Graf planarny</strong> – graf, który można narysować na płaszczyźnie tak, by krzywe obrazujące krawędzie grafu
          nie przecinały się ze sobą. Odwzorowanie grafu planarnego na płaszczyznę o tej własności nazywane jest
          jego rysunkiem płaskim. Graf planarny o zbiorze wierzchołków i krawędzi zdefiniowanym poprzez rysunek
          płaski nazywany jest grafem płaskim.</p>
          <p><strong>Kryterium Kuratowskiego</strong> - dwa minimalne grafy, które nie są planarne, to K<sub>5</sub> i K<sub>3,3</sub>. Twierdzenie Kuratowskiego mówi, że graf skończony jest planarny wtedy i tylko wtedy, gdy nie zawiera podgrafu homeomorficznego z grafem K<sub>5</sub> ani z grafem K<sub>3,3</sub>.</p>
          <p><strong>Wzór Eulera</strong> - dowolny rysunek płaski grafu planarnego wyznacza spójne obszary płaszczyzny zwane
          ścianami. Dokładnie jeden z tych obszarów, zwany ścianą zewnętrzną, jest nieograniczony.
          Zgodnie z wzorem Eulera, jeżeli G jest grafem spójnym i planarnym, to |V| + |S| − |E| = 2, gdzie
          V - zbiór wierzchołków, E - zbiór krawędzi, S - zbiór ścian dowolnego rysunku płaskiego grafu G.</p>
        `
      },
      {
        "id": 4,
        "question": "Drzewa spinające grafu. Minimalne drzewa spinające.",
        "answer": `
        <p><strong>Drzewem rozpinającym</strong> grafu G nazywamy drzewo, które zawiera wszystkie wierzchołki grafu G, zaś zbiór
        krawędzi drzewa jest podzbiorem zbioru krawędzi grafu. Konstrukcja drzewa rozpinającego polega na
        usuwaniu z grafu tych krawędzi które należą do cykli.</p>
        <p><strong>Minimalne drzewo rozpinające</strong> jest to drzewo rozpinające danego grafu o najmniejszej z możliwych wag.</p>
        <p>Algorytmy znajdujące dla zadanego grafu minimalne drzewo rozpinające:</p>
        <ul>
          <li><strong>Algorytm Prima</strong> (nazywany też algorytmem Dijkstry-Prima)</li>
          <li><strong>Algorytm Kruskala</strong></li>
        </ul>
        `
      }
    ]
  },
  {
    "id": 6,
    "name": "Programowanie strukturalne",
    "icon": "braces",
    "chance": 90,
    "questions": [
      {
        "id": 0,
        "question": "Typy zmiennych. Podział. Przykłady w wybranym języku programowania.",
        "answer": `
          <p><strong>Typ liczbowy</strong> - zmienne typu liczbowego dzielimy przede wszystkim na liczby <strong>całkowite</strong> (są to liczby bez części
          dziesiętnych) i <strong>zmiennoprzecinkowe</strong> nazywane również <strong>rzeczywistymi</strong> (te liczby mają już części dziesiętne). W
          zależności jaki typ wybierzemy mamy w każdym do wyboru kilka typów różniących się ilością zajmowanych bajtów w
          pamięci i przedziałem wartości. Jedynie typ decimal może występować jako typ całkowity (dokładny) lub
          zmiennoprzecinkowy.</p>
          <ul>
            <li><em>int i = 10;</em></li>
            <li><em>long l = 123456;</em></li>
            <li><em>byte bajt = 100;</em></li>
            <li><em>float f = 3.14f;</em></li>
            <li><em>double d = 3.1412d;</em></li>
          </ul>
          <p><strong>Typ znakowy</strong> - zmienne typu znakowego służą do przechowywania pojedynczego znaku, umieszcza się go w parze
          pojedynczych cudzysłowów. Jeśli chcemy by zapisać więcej znaków trzeba zadeklarować tablicę typu char.</p>
          <ul>
            <li><em>string uwm = "UWM"</em></li>
            <li><em>char litera = 'U';</em></li>
          </ul>
          <p><strong>Typ logiczny</strong> - przechowuje jedną z dwóch wartości - <strong>true</strong> (prawda) albo <strong>false</strong> (fałsz). Wartość logiczna true jest równa 1, natomiast false ma wartość 0.</p>
          <ul>
            <li><em>bool prawda = true;</em></li>
            <li><em>bool falsz = false;</em></li>
          </ul>
          <p><strong>Typ wskaźnikowy</strong> - wskaźnik zawiera adres zmiennej. Deklaracja zmiennych typu wskaźnikowego zawiera
          operator * po nazwą typu, na który ma wskazywać. Kiedy chcemy uzyskać adres zmiennej, poprzedzamy ją
          operatorem &. Kiedy chcemy się odwołać do elementu wskazywanego przez wskaźnik, piszemy przed
          nazwą wskaźnika *.</p>
        `
      },
      {
        "id": 1,
        "question": "Rodzaje pętli. Uwarunkowanie zastosowań. Problem równoważności pętli w wybranym języku programowania.",
        "answer": `
          <p>Pętla, z definicji, to element języka programowania pozwalający na wielokrotne, kontrolowane wykonywanie
          danego fragmentu kodu.</p>
          <p>Zasadniczo, w języku C/C++ zdefiniowane są trzy rodzaje pętli:</p>
          <ul>
            <li>Pętla warunkowa do…while</li>
            <li>Pętla warunkowa while…</li>
            <li>Pętla krokowa for…</li>
          </ul>
          <p>Zaczynając od pętli warunkowych, pętla do… i pętla while… są sobie równoważne pod względem
          konstrukcyjnym. Różnica tkwi w miejscu sprawdzania warunku. Odrębnym rodzajem jest pętla krokowa
          for…. Odrębnym, ponieważ pętla jest wykonywana zadaną ilość razy kontrolowaną przez licznik, tworzony
          specjalnie na potrzeby tej pętli.</p>
          <p>Równoważność między pętlą krokową for… i pętlami warunkowymi while… i do… jest tylko częściowa. Pętlę
          for… zawsze można zaimplementować za pomocą pętli while… lub do…, przenosząc bezpośrednio warunek
          kończący lub zanegowany warunek początkowy i dodając do instrukcji wewnątrz pętli, instrukcję
          inkrementującą lub dekrementującą licznik. Implementacja w drugim kierunku jednak nie zawsze jest
          możliwa.</p>
        `
      },
      {
        "id": 2,
        "question": "Zmienne typu adresowego (wskaźniki). Implementacja w wybranym języku programowania.",
        "answer": `
          <p><strong>Zmienne typu adresowego</strong>, zamiast faktycznej wartości danego typu, przechowują adres fizycznej pamięci,
          przechowujący właściwą zmienną z wartością. Wykorzystywane są najczęściej, jeśli funkcja ma operować na
          głównej zmiennej, a nie standardowo na jej kopii utworzonej na potrzeby funkcji. W przypadku
          deklarowania wskaźnika dla tablicy, pod tą zmienną znajduje się adres pierwszej komórki tej tabeli. Aby
          dostać się do kolejnych komórek tabeli należy inkrementować odpowiednio zmienną wskaźnika.</p>
          <table class="two-cols">
            <tr>
              <td>wskaźnik dla zmiennej typu <em>int</em></td>
              <td>int* wsk;</td>
            </tr>
            <tr>
              <td>wskaźnik dla bloku dowolnego typu pamięci</td>
              <td>void* wsk;</td>
            </tr>
            <tr>
              <td>wskaźnik 3-elemntowej tablicy liczb całkowitych</td>
              <td>int (*tab)[3]</td>
            </tr>
            <tr>
              <td>wskaźnik do 3-elementowej tablicy wskaźników</td>
              <td>int *(*tab)[3]</td>
            </tr>
            <tr>
              <td>tablica 4 wskaźników liczb całkowitych</td>
              <td>int *tab[4];</td>
            </tr>
            <tr>
              <td>stały wskaźnik typu całkowitego</td>
              <td>int *const pc;</td>
            </tr>
            <tr>
              <td>wskaźnik stałej typu całkowitego</td>
              <td>int const *pxc;</td>
            </tr>
          </table>
        `
      },
      {
        "id": 3,
        "question": "Funkcje (z wzmianką o procedurach). Przekazywanie parametrów przez wartość i referencję lub adres.",
        "answer": `
          <p>Funkcja jest to fragment kodu, który może być wywołany wielokrotnie w różnych miejscach programu.
          Każda funkcja powinna zwracać wynik wykonanego kodu. <strong>Procedura nie zwraca wartości</strong>.</p>
          <code class="whitespace-pre">typ nazwa_funkcji(argumenty) {
  // instrukcje
}</code>
          <p><em>Przekazywanie parametrów przez wartość (funkcja f1)</em> – pracujemy na kopiach zmiennych a i b, czyli nie
          zostaną one zmienione w głównym programie.</p>
          <code class="whitespace-pre">void f1(int c, int d) {
  c++;
  d++;
}</code>
          <p><em>Przekazywanie parametrów przez referencję (funkcja f2)</em> – do funkcji przekazujemy adresy zmiennych a i
          b, czyli pracujemy na wartościach przechowywanych w ich adresach.</p>
          <code class="whitespace-pre">void f2(int &x, int &y) {
  x++;
  y++;
}</code>
        `
      },
      {
        "id": 4,
        "question": "Cechy programowania strukturalnego. Kluczowe różnice między programowaniem strukturalnym a obiektowym.",
        "answer": `
          <p>Cechy: hierarchiczna i modułowa struktura programu, możliwość analizy poprawności programu, łatwość
          modyfikacji, redukcja błędów i czasu pisania programu, możliwość wykorzystania wcześniej napisanych
          modułów do innych programów, metody: analityczna, czyli podejście "z góry na dół" (ang. top-down),
          syntetyczna, czyli podejście "z dołu do góry" (ang. bottom-up)</p>
          <p><strong>Do kluczowych różnic między programowaniem strukturalnym a obiektowym zaliczamy</strong>:</p>
          <ul>
            <li>program pisany strukturalnie – zawiera „bloki”, gdzie program pisany obiektowo zawiera obiekty</li>
            <li>dane i procedury w programowaniu strukturalnym nie są ze sobą bezpośrednio powiązane</li>
            <li>programy strukturalne nie posiadają takich cech jak: abstrakcja, hermetyzacja, polimorfizm, dziedziczenie – charakterystyczne dla programowania obiektowego</li>
            <li>programowanie obiektowe może być oparte na wzorach projektowych – gdzie strukturalne ma sztywną metodę pisania</li>
          </ul>
        `
      }
    ]
  },
  {
    "id": 7,
    "name": "Programowanie obiektowe",
    "icon": "grid-3x3-gap",
    "chance": 90,
    "questions": [
      {
        "id": 0,
        "question": "Zalety języków programowania obiektowo orientowanych. Składniki klasy. Konstruktory i destruktory. Podać przykłady.",
        "answer": `
          <p><strong>Zalety programowania obiektowego</strong></p>
          <ol>
            <li>łatwość przekładania poszczególnych wymogów na poszczególne moduły systemu;</li>
            <li>możliwość wielokrotnego zastosowania poszczególnych modułów systemu;</li>
            <li>każdy moduł odpowiedzialny jest za konkretne zadanie, nie występuje dublowanie funkcjonalności co pozwala szybko i skutecznie poprawiać powstałe błędy.</li>
          </ol>
          <p><strong>Klasa</strong> jest to grupa danych i funkcji, które na tych danych operują. Tworząc klasę tworzymy nowy typ
          danych, którego składnikami są inne typy danych.</p>
          <p><strong>Składnik klasy</strong> to element należący do danej klasy. Składnikami klasy mogą być: pola, konstruktory,
          destruktory, metody, właściwości, indeksatory, delegacje, operatory.</p>
          <p><strong>Konstruktor</strong> jest to funkcja w klasie, wywoływana w trakcie tworzenia każdego obiektu danej klasy.
          Należy dodać że każda klasa ma swój konstruktor. Nawet jeżeli nie zadeklarujemy go jawnie zrobi to za
          nas kompilator (stworzy wtedy konstruktor bezparametrowy).</p>
          <p><strong>Destruktor</strong> jest natomiast funkcją, którą wykonuje się w celu zwolnienia pamięci; następuje niszczenie
          obiektu danej klasy.</p>
        `
      },
      {
        "id": 1,
        "question": "Zastosowanie składników statycznych w klasie. Statyczne funkcje składowe. Podać przykłady.",
        "answer": `
          <p>Czasami zachodzi potrzeba dodania elementu, który jest związany z klasą, ale nie z konkretną instancją tej
          klasy. Możemy wtedy stworzyć element <strong>statyczny</strong>. Element <strong>statyczny</strong> jest właśnie elementem, który jest
          powiązany z klasą, a nie z obiektem tej klasy, czyli np. statyczna metoda nie może się odwołać do
          niestatycznej zmiennej lub funkcji.</p>
          <p>Elementy <strong>statyczne</strong> poprzedza się podczas definicji słówkiem <em>static</em>. <strong>Statyczne</strong> mogą być zarówno <em>funkcje</em>,
          jak i <em>pola</em> należące do klasy.</p>
          <code class="whitespace-pre">public class Klasa {
  private static int wystapienia = 0;

  public Klasa() {
    wystapienia++;
  }

  ~Klasa() {
    wystapienia--;
  }

  public static int WystapieniaKlasy() {
    return wystapienia;
  }
}</code>
          <p>W powyższym przykładzie ponadto istnieje <strong>metoda statyczna</strong>. Z takiej metody nie można się odwołać
          do niestatycznych elementów klasy. Zarówno do <strong>klasy statycznej</strong> jak do <strong>statycznego pola</strong> możemy się
          odwołać nawet jeżeli nie został stworzony żaden obiekt klasy Klasa.</p>
          <p>Odwołanie się do <strong>metody statycznej</strong> <em>WystapieniaKlasy</em> z programu wymaga następująco:</p>
          <code class="whitespace-pre">int i = Klasa.WystapieniaKlasy();</code>
          <p>Gdyby zaś pole <em>wystapienia</em> było publiczne, a nie prywatne, to moglibyśmy się do niego odwołać
          poprzez:</p>
          <code class="whitespace-pre">int i = Klasa.wystapienia;</code>
          <p><strong>Statyczne funkcje składowe</strong></p>
          <p>Takie funkcje można wywołać nawet wtedy, gdy nie istnieje jeszcze żaden obiekt klasy. Do jej nazwy
          z zewnątrz klasy odwołujemy się poprzez nazwę klasy za pomocą operatora wyboru składowej (kropka). Ponieważ funkcja statyczna
          <em>nie</em> jest wywoływana na rzecz obiektu, ale jak funkcja globalna, nie można w niej odwoływać się do <strong>this</strong>
          ani do żadnych składowych niestatycznych - te bowiem istnieją tylko wewnątrz konkretnych obiektów i w
          każdym z nich mogą być różne. Można natomiast w funkcjach statycznych klasy odwoływać się do
          składowych statycznych tej klasy: innych funkcji statycznych i zmiennych klasowych (określanych przez
          statyczne pola klasy).</p>
        `
      },
      {
        "id": 2,
        "question": "Dziedziczenie. Dostęp do składników klasy. Kolejność wywołania konstruktorów. Przypisanie i inicjalizacja obiektów w warunkach dziedziczenia.",
        "answer": `
          <p><strong>Dziedziczenie</strong> to w programowaniu obiektowym operacja polegająca na stworzeniu nowej klasy na bazie
          klasy już istniejącej. Stosowana najczęściej przy tworzeniu potomnych klas specjalizowanych, lub przy
          tworzeniu klas użytkowych z klasy abstrakcyjnej.</p>
          <p><strong>Dostęp do składników klasy</strong> głównej jest zależny od tego w jakich sekcjach są one zdefiniowane. Do
          składników <em>private</em> danej klasy można odnieść się jedynie w tej samej klasie. Aby więc klasa
          dziedzicząca mogła naprawdę dziedziczyć z klasy głównej, wszystkie typy danych w klasie głównej muszą
          być typu <em>public</em> lub <em>protected</em>.</p>
          <p>Konkstruktor, jaki jest najpierw wywoływany, to konstruktor <strong>klasy rodzica</strong>, a dopiero potem wywoływane są instrukcje konstruktura
          klasy dziedziczącej.</p>
        `
      },
      {
        "id": 3,
        "question": "Polimorfizm. Deklarowanie funkcji wirtualnych. Mechanizm wywołania. Podać przykłady.",
        "answer": `
          <p><strong>Polimorfizm</strong> - mechanizmy pozwalające programiście używać wartości, zmiennych i funkcji na kilka różnych sposobów.
          Najczęściej polimorfizm przejawia się w postaci <strong>przeciążania metod</strong>, czyli definiowaniu więcej niż jednej metody
          o tej samej nazwie. Takie metody mogą różnić się ilością przyjmowanych argumentów i/lub ich typami.</p>
          <code class="whitespace-pre">public static class Kalkulator {
  public static void DodajLiczby(int a, int b) {
    Console.WriteLine($"a + b = {a + b}");
  }

  public static void DodajLiczby(int a, int b, int c) {
    Console.WriteLine($"a + b + c = {a + b + c}");
  }
}</code>
          <p><strong>Zadeklarowanie funkcji jako wirtualną</strong> oznacza, że funkcja ta może zostać <strong>nadpisana</strong> przez klasy
          dziedziczące. Aby nadpisać taką metodę, używamy słowa kluczowego <em>override</em>. Przy kompilacji, metodzie wirtualnej przypisywana jest
          ostatnie nadpisanie danej metody.</p>
          <p>Prostym przykładem działania funkcji wirtualnych może być obliczenie pola figury geometrycznej.</p>
          <code class="whitespace-pre">public class Figura {
  protected double x, y;

  public Figura(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public virtual double Pole() {
    return x * y;
  }
}

public class Kolo : Figura {
  public Kolo(double r) : base(r, 0) {}

  public override double Pole() {
    return Math.PI * x * x;
  }
}

public class Kula : Figura {
  public Kula(double r) : base(r, 0) {}
  
  public override double Pole() {
    return 4 * Math.PI * x * x;
  }
}</code>
        `
      },
      {
        "id": 4,
        "question": "Definiowanie szablonu klasy. Korzystanie z szablonu.",
        "answer": `
          <p>Szablonów klas używamy, jeśli planujemy używać podobnych/identycznych struktur dla różnych typów
          danych.</p>
          <p><strong>Definiowanie szablonu klasy:</strong></p>
          <code class="whitespace-pre">template &lt;class T&gt;
class Calc
{
  public:
    T sum(T x, T y){
    return x + y;
  }
}</code>
          <p><strong>Korzystanie z szablonu:</strong></p>
          <code class="whitespace-pre">Calc&lt;int&gt; calc_int;
Calc&lt;float&gt; calc_float;

cout &lt;&lt; calc_int.sum(1, 2);
cout &lt;&lt; calc_float.sum(1.0, 2.5);</code>
        `
      }
    ]
  },
  {
    "id": 8,
    "name": "Systemy operacyjne",
    "icon": "display",
    "chance": 5,
    "questions": [
      {
        "id": 0,
        "question": "Cykl pracy procesora przy wykonaniu programu.",
        "answer": `
          Typowy cykl rozkazowy w systemie o architekturze von Neumanna zaczyna się od pobrania rozkazu z
          pamięci i przesłania go do rejestru rozkazów (ang. instruction register). Rozkaz jest następnie
          dekodowany i realizowany (może spowodować pobranie argumentów z pamięci i umieszczenie ich w
          innym rejestrze wewnętrznym). Po wykonaniu rozkazu na argumentach jego wynik można z powrotem
          przechować w pamięci. Zauważmy, że jednostka pamięci „widzi" tylko strumień adresów pamięci. Nie jest jej znany
          sposób, w jaki one powstały (licznik rozkazów, indeksowanie, modyfikacje pośrednie, adresy literalne itp.) ani czemu służą.
        `
      },
      {
        "id": 1,
        "question": "Procesy, zarządzanie procesami.",
        "answer": `
          <p><strong>Proces</strong> jest to po prostu egzemplarz wykonywanego programu. Należy odróżnić jednak proces od wątku.
          Każdy proces posiada własną przestrzeń adresową, natomiast wątki posiadają wspólną sekcję danych.
          Każdemu procesowi przydzielone zostają zasoby, takie jak: procesor, pamięć, dostęp do urządzeń
          wejścia-wyjścia, pliki.</p>
          <p>Każdy proces posiada tzw. "rodzica". W ten sposób tworzy się swego rodzaju drzewo procesów. Proces
          może (ale nie musi) mieć swoje procesy potomne. Za zarządzanie procesami odpowiada jądro systemu
          operacyjnego. Wykonanie musi przebiegać sekwencyjnie.</p>
          <p><strong>Może przyjmować kilka stanów</strong>: działający, czekający na udostępnienie przez system operacyjny
          zasobów, przeznaczony do zniszczenia, właśnie tworzony itd.</p>
          <p><strong>W skład procesu wchodzi: kod programu, licznik rozkazów, stos, dane
          <p><strong>Zarządzanie procesami</strong></p>
          <ul>
            <li>System operacyjny odpowiada za następujące działania dotyczące zarządzania procesami: tworzenie i usuwanie</li>
            <li>planowanie porządku wykonywania</li>
            <li>mechanizmy synchronizacji, komunikacji</li>
            <li>usuwanie zakleszczeń</li>
          </ul>
        `
      },
      {
        "id": 2,
        "question": "Metody przydziału pamięci operacyjnej procesowi. Organizacja pamięci wirtualnej.",
        "answer": `
          <p><strong>First-Fit</strong> - Jest to najprostsza możliwa strategia. Wybierany jest pierwszy (pod względem adresów) wolny
          obszar, który jest wystarczająco duży.</p>
          <p><strong>Best-Fit</strong> - Wybieramy najmniejszy wolny obszar, który jest wystarczająco duży.
          Worst-fit - Przydzielamy pamięć zawsze z największego wolnego obszaru (oczywiście, o ile jest on
          wystarczająco duży).</p>
          <p><strong>Segmentacja</strong> - Pamięć wykorzystywana przez proces, z logicznego punktu widzenia, nie stanowi jednego
          spójnego obszaru. Zwykle składa się z kilku segmentów. Typowe segmenty to: kod programu, zmienne
          globalne, stos i sterta. System operacyjny może więc przydzielać procesom pamięć nie w postaci jednego
          spójnego bloku, ale kilku takich bloków, segmentów. Co więcej, proces może w trakcie działania prosić o
          przydzielenie lub zwolnienie segmentów.</p>
          <p><strong>Stronicowanie</strong> - Podział wirtualnej przestrzeni adresowej procesu na strony. Strona to obszar ciągłej
          pamięci o stałym rozmiarze (zazwyczaj 4kB). Rzeczywista pamięć operacyjna podzielona jest na ramki,
          których rozmiar odpowiada wielkości stron. System operacyjny według uznania może przydzielać
          ramkom strony pamięci lub pozostawiać je puste.</p>
          <p><strong>Pamięć wirtualna</strong> jest techniką programową a także sprzętową gospodarowania pamięcią operacyjną
          RAM pozwalającą na przydzielanie pamięci dla wielu procesów, zwalnianie jej i powtórne przydzielanie, w
          ilości większej niż rzeczywista ilość pamięci fizycznej zainstalowanej w komputerze poprzez przeniesienie
          danych z ostatnio nie używanej pamięci do pamięci masowej (np. twardego dysku), w sytuacji gdy
          procesor odwołuje się do danych z pamięci przeniesionej na dysk przesuwa się te dane do pamięci w
          wolne miejsce, a gdy brak wolnej pamięci zwalnia się ją przez wyżej opisane przerzucenie jej na dysk.</p>
        `
      },
      {
        "id": 3,
        "question": "Funkcje systemowe – podstawowe kategorie, przykłady.",
        "answer": `
          <p>Programy użytkowników mają dostęp do usług systemu operacyjnego poprzez tzw. funkcje systemowe.
          Funkcje te są widoczne w języku programowania podobnie jak funkcje z tego języka, a ich konkretny
          mechanizm wywoływania jest ukryty przed programistą.</p>
          <p>Funkcje udostępniane przez system operacyjny możemy pogrupować w następujący sposób:</p>
          <ul>
            <li><strong>operacje na procesach</strong> - Ta grupa funkcji obejmuje m.in.: tworzenie nowych procesów, uruchamianie
            programów, zakończenie się procesu, przerwanie działania innego procesu, pobranie informacji o
            procesie, zawieszenie i wznowienie procesu, oczekiwanie na określone zdarzenie, przydzielanie i
            zwalnianie pamięci,</li>
            <li><strong>operacje na plikach</strong> - Ta grupa funkcji obejmuje takie operacje, jak: utworzenie pliku, usunięcie pliku,
            otwarcie pliku, odczyt z pliku, zapis do pliku, pobranie lub zmiana atrybutów pliku.</li>
            <li><strong>operacje na urządzeniach</strong> - Zamówienie i zwolnienie urządzenia, odczyt z i zapis do urządzenia,
            pobranie lub zmiana atrybutów urządzenia, (logiczne) przyłączenie lub odłączenie urządzenia.</li>
            <li><strong>informacje systemowe</strong> - Funkcje te obejmują pobieranie i/lub ustawianie najrozmaitszych informacji
            systemowych, w tym: czasu i daty, wersji systemu operacyjnego, atrybutów użytkowników itp.</li>
            <li><strong>komunikacja</strong> - Ta grupa funkcji obejmuje przekazywanie informacji między procesami. Spotykane są
            dwa schematy komunikacji: przekazywanie komunikatów i pamięć współdzielona. Przekazywanie
            komunikatów polega na tym, że jeden proces może wysłać (za pośrednictwem systemu
            operacyjnego) pakiet informacji, który może być odebrany przez drugi proces. Mechanizm pamięci
            współdzielonej polega na tym, że dwa procesy uzyskują dostęp do wspólnego obszaru pamięci.</li>
          </ul>
        `
      },
      {
        "id": 4,
        "question": "Szeregowanie procesów. Wybrane algorytmy szeregowania.",
        "answer": `
          <p>Proces szeregujący (ang. scheduler) to część jądra zajmująca się przydzielaniem procesom procesora,
          zgodnie z pewną polityką, za pomocą pewnych mechanizmów.</p>
          <p><strong>FIFO</strong> - algorytm powszechnie stosowany, jeden z prostszych w realizacji, dający dobre efekty w systemach
          ogólnego przeznaczenia; zadanie wykonuje się aż nie zostanie wywłaszczone przez siebie lub inne
          zadanie o wyższym priorytecie</p>
          <p><strong>Planowanie priorytetowe</strong> - wybierany jest proces o najwyższym priorytecie. W tej metodzie występuje
          problem nieskończonego blokowania (procesu o niskim priorytecie przez procesy o wysokim priotytecie).
          Stosuje się tu postarzanie procesów, polegające na powolnym podnoszeniu priorytetu procesów zbyt
          długo oczekujących.</p>
          <p><strong>Planowanie rotacyjne (round-robin)</strong> - Planista przydziela każdemu z procesów kwant czasu i jeżeli nie
          zdąży się wykonać to ląduje na końcu kolejki FCFS.</p>
          <p><strong>Algorytm wywłaszczający</strong> - Algorytm, który może przerwać wykonanie procesu i przenieść go z
          powrotem do kolejki.</p>
          <p><strong>Algorytm niewywłaszczający</strong> - Algorytm, w którym procesy przełączają się dobrowolnie. Proces aktywny
          jest przenoszony do kolejki procesów oczekujących tylko wtedy, gdy sam przerwie swe działanie; dopóki
          tego nie uczyni (lub nie zakończy działania), żaden inny proces nie otrzyma dostępu do procesora.</p>
        `
      },
      {
        "id": 5,
        "question": "Synchronizacja procesów współbieżnych. Semafory.",
        "answer": `
          <p>Procesy współbieżne to takie procesy, które są wykonywane jednocześnie w systemie operacyjnym.
          Można je podzielić na dwie kategorie: <strong>niezależne</strong> oraz <strong>współpracujące</strong>. Procesy współpracujące mogą
          wpływać na inne procesy lub podlegać ich oddziaływaniom. Mogą również dzielić wspólne dane, do
          których trzeba zagwarantować zsynchronizowany dostęp, aby zapewnić ich spójność, tzn. nie można
          dopuścić do sytuacji, w której wykonywane na nich zmiany zaburzają się wzajemnie.</p>
          <p>Jednym z narzędzi do synchronizacji jest <strong>semafor</strong>. Semafor to zmienna całkowita, która przyjmuje pewną
          początkową wartość (zazwyczaj 1) i na której można wykonać tylko dwie operacje: wait i signal. Są to
          operacje niepodzielne. Jeśli jeden proces zmienia wartość semafora, to żaden inny proces nie może
          jednocześnie tego robić.</p>
          <p>Każdy proces przed wejściem do sekcji krytycznej wywołuje funkcję wait, która sprawdza wartość
          semafora. Jeżeli wartość jest większa od zera, to proces wchodzi do sekcji krytycznej, zmniejsza wartość
          semafora o jeden i modyfikuje dane. Proces, który zakończy wykonywanie sekcji krytycznej wywołuje
          funkcję signal, która dla klasycznego semafora zwiększa jego wartość o jeden. W przypadku rozwiązania
          semafora z kolejką procesów czekających, funkcja signal zwiększa wartość semafora o jeden i wznawia
          jeden z procesów czekających w kolejce.</p>
          <p>Istnieje też druga konstrukcja semafora: <strong>semafor binarny</strong>. Różni się on od wcześniejszego, zwanego
          semaforem zliczającym, tym, że przyjmuje tylko dwie wartości: 0 lub 1.</p>
        `
      }
    ]
  },
  {
    "id": 9,
    "name": "Inżynieria oprogramowania",
    "icon": "gear",
    "chance": 10,
    "questions": [
      {
        "id": 0,
        "question": "Cykle projektowania i życia oprogramowania.",
        "answer": `
          <p><strong>strategiczna</strong>, w tej fazie podejmowane są decyzje strategiczne odnośnie podejmowania przedsięwzięcia
          projektowego: zakresu, kosztów, czasu realizacji itp.<p>
          <p><strong>analizy</strong>, w której budowany jest logiczny model systemu,</p>
          <p><strong>dokumentacji</strong>, w której wytwarzana jest dokumentacja użytkownika. Opracowywanie dokumentacji
          przebiega równolegle z produkcją oprogramowania. Faza ta praktycznie rozpoczyna się już w trakcie
          określania wymagań.</p>
          <p><strong>określania wymagań</strong>, w której określane są cele oraz szczegółowe wymagania wobec systemu,
          projektowania (ang. design), w której powstaje szczegółowy projekt systemu spełniającego ustalone
          wcześniej wymagania,</p>
          <p><strong>implementacji/kodowania</strong>, oraz testowania modułów, w której projekt
          zostaje zaimplementowany w konkretnym środowisku programistycznym oraz wykonywane są testy
          poszczególnych modułów,</p>
          <p><strong>testowania</strong>, w której następuje integracja poszczególnych modułów połączona z testowaniem
          poszczególnych podsystemów oraz całego SI,</p>
          <p><strong>instalacji</strong>, w której następuje przekazanie systemu użytkownikowi,</p>
          <p><strong>konserwacji</strong>, w której oprogramowanie jest wykorzystywane przez użytkownika (ów), a producent dokonuje
          konserwacji SI (a przede wszystkim oprogramowania) – wykonuje modyfikacje polegające na usuwaniu
          błędów, zmianach i rozszerzaniu funkcji systemu;</p>
          <p><strong>likwidacja</strong>, w której wykonuje się czynności związane z zakończeniem użytkowania SI</p>
        `
      },
      {
        "id": 1,
        "question": "Klasyfikacja narzędzi wspierających wytwarzanie oprogramowania.",
        "answer": `
          <ol>
            <li><strong>Zintegrowane środowisko programistyczne</strong> jest to aplikacja lub zespół aplikacji (środowisko)
            służących do tworzenia, modyfikowania, testowania i konserwacji oprogramowania.</li>
            <ul>
              <li>pakiet Microsoft Visual Studio <em>(popularny na systemach rodziny Windows)</em></li>
              <li>Eclipse i NetBeans</li>
              <li>Zend Studio <em>(rozwiązanie dedykowane dla jezyka PHP)</em></li>
            </ul>
            <li><strong>CASE (Computer-Aided Software Engineering)</strong> - oprogramowanie używane do komputerowego
            wspomagania projektowania oprogramowania.</li>
            <ul>
              <li>narzędzia do modelowania w języku UML i podobnych <em>(np. Enterprise Architect)</em></li>
              <li>narzędzia do zarządzania konfiguracją zawierające system kontroli wersji</li>
              <li>narzędzia do re factoringu</li>
            </ul>
            <li><strong>Systemy kontroli wersji</strong> służą do śledzenia zmian głównie w kodzie źródłowym oraz pomocy
            programistom w łączeniu i modyfikacji zmian dokonanych przez wiele osób w różnych momentach.
            Do najpopularniejszych należą Git, Mercurial czy SVN.</li>
            <li>Kompilatory - programy służący do automatycznego tłumaczenia kodu napisanego w
            jednym języku (języku źródłowym) na równoważny kod w innym języku (języku wynikowym)
            Microsoft Visual Studio, Codeblocks, GNU Compiler for Java</li>
            <li><strong>Narzędzia wspomagające kompilację</strong> - Ccache, Distcc, Scratchbox</li>
            <li><strong>Narzędzia wspomagające budowę aplikacji</strong>
            Autotools, Autoconf, Autoheader, Automake</li>
            <li><strong>Narzędzia do analizy programów (Debuggery)</strong> - programy komputerowy służący do dynamicznej analizy innych programów, w celu odnalezienia i
            identyfikacji zawartych w nich błędów, zwanych z angielskiego bugami (robakami).</li>
          </ol>
        `
      },
      {
        "id": 2,
        "question": "Metody oraz strategie testowania oprogramowania.",
        "answer": `
          <p><strong>Testowanie oprogramowania</strong> – proces związany z wytwarzaniem oprogramowania. Jest on jednym z
          procesów kontroli jakości oprogramowania.</p>
          <p>Wyróżniamy testy:</p>
          <p><strong>Statyczne</strong>: proces pozwalający na wyszukanie błędów od fazy zbierania wymagań biznesowych
          poprzez konstruowanie kodu, aż po dostarczenie produktu, jednak bez uruchamiania aplikacji.</p>
          <p>Testy statyczne przeprowadzane są przy użyciu:</p>
          <ul>
            <li>Przegląd jest procesem lub spotkaniem podczas którego produkt/system jest przedstawiany
            członkom zespołu projektowego, użytkownikom oraz innym zainteresowanym stronom w celu
            uzyskania komentarzy lub aprobaty dla danego rozwiązania.</li>
            <li>Analiza statyczna jest to analiza struktury kodu źródłowego lub kodu skompilowanego bez
            jego uruchomienia. Analiza statyczna może odbywać się na etapie budowania aplikacji, ponieważ
            jej wyniki są dostępne już podczas kompilacji kodu źródłowego.</li>
          </ul>
          <p><strong>Dynamiczne</strong>:
          <ul>
            <li><strong>Testy funkcjonalne</strong> (testy czarnej skrzynki). Tester nie ma dostępu do kodu testowanej aplikacji.
            Testy wykonywane są przez osobę, która nie tworzyła aplikacji w opaciu o dokumentację oraz
            założenia funkcjonalne.</li>
            <li><strong>Testy strukturalne</strong>. Przykładem testów strukturalnych są testy jednostkowe (unit tests), które
            polegają na stworzeniu kodu sprawdzającego poprawność działania właściwego kodu aplikacji.
            Do zdefiniowania zasad przejścia przez ścieżki aplikacji, używane są kryteria pokrycia pętli i
            warunków.</li>
          </ul>
        `
      },
      {
        "id": 3,
        "question": "Instalacja i konserwacja oprogramowania.",
        "answer": `
          <p>Przez <strong>instalację (wdrożenie)</strong> systemu informatycznego rozumiemy przekazanie systemu
          użytkownikowi, po czym następuje etap funkcjonowania SI i jego konserwacji (usuwania błędów,
          modyfikacji).</p>
          <p>Proces wdrożenia zależy od tego, czy wytwarzane oprogramowanie jest seryjnym (komercyjnym) −
          sprzedawanym w wielu egzemplarzy, czy też jest stworzono na konkretne zamówienie (jest dedykowanym
          SI). W tym ostatnim wypadku na etapie wdrożenia dokonuje się:</p>
          <ul>
            <li>Szkolenie przyszłych administratorów systemu oraz jego użytkowników.</li>
            <li>Instalacja sprzętu i oprogramowania.</li>
            <li>Wprowadzanie rzeczywistych danych do baz danych.</li>
            <li>Usuwanie znalezionych błędów w programach i dokumentacji.</li>
            <li>Przekazanie systemu użytkownikowi.</li>
          </ul>
          <p><strong>Konserwacja oprogramowania</strong> polega na wykonaniu ewentualnych modyfikacji oprogramowania.
          Tymi modyfikacjami mogą być:</p>
          <ul>
            <li>Dostosowanie oprogramowania do zmian zachodzących w środowisku pracy klienta (zmian pewnych
            funkcji systemu).</li>
            <li>Dostosowanie oprogramowania do zmian sprzętu, urządzeń technicznych.</li>
            <li>Poprawianie jakości oprogramowania.</li>
            <li>Usuwanie odnalezionych błędów.</li>
          </ul>
        `
      },
      {
        "id": 4,
        "question": "Zagadnienia etyczne i prawne związane z procesem wytwarzania i użytkowania oprogramowania.",
        "answer": `
          <p>Przykłady nieetycznych zachowań w obszarze IT obejmują:</p>
          <ul>
            <li>przejmowanie cudzych pomysłów i projektów,</li>
            <li>kradzież informacji, przedkładanie plagiatów,</li>
            <li>złośliwe usuwanie dokumentów,</li>
            <li>handel danymi,</li>
            <li>zamieszczanie informacji obraźliwych,</li>
            <li>celowe przeciążanie serwerów</li>
          </ul>
          <p>Do często spotykanych błędów w kontraktach publicznych na oprogramowanie zalicza się:</p>
          <ul>
            <li>Deklarowane wirtualne koszty, tj. takie, które pozwolą wygrać przetarg,</li>
            <li>Nierealny termin dostawy,</li>
            <li>Nieprawidłowy harmonogram etapów,</li>
            <li>Świadomie niekompletne wymagania,</li>
            <li>Brak doświadczenia dostawcy w danej dziedzinie zastosowań,</li>
            <li>Długi okres wyczekiwania na poprawę zgłaszanych defektów,</li>
            <li>Brak alternatywnych procedur na wypadek niepowodzenia przedsięwzięcia programowego,</li>
            <li>Niewystarczająca troska o walidację danych.</li>
          </ul>
        `
      }
    ]
  },
  {
    "id": 10,
    "name": "Projektowanie systemów informatycznych",
    "icon": "card-checklist",
    "chance": 10,
    "questions": [
      {
        "id": 0,
        "question": "Etapy cyklu życia systemu informatycznego i ich charakterystyka.",
        "answer": `
          <p><strong>Określenie założeń i celów systemu</strong> - Etap ten jest konieczny przed rozpoczęciem pracy analityków i
          projektantów systemu. W deklaracji założeń i celów należy przedstawić problem, który powinien zostać
          rozwiązany oraz obszar działania, którego on dotyczy.</p>
          <p><strong>Zebranie informacji i studium możliwości</strong> - Efektem tego etapu jest ocena technicznych możliwości
          rozwiązania problemów i realizacji założeń projektu. Mogą tutaj zostać zaprezentowane różne warianty
          rozwiązań w ogólnym zarysie wraz z szacunkiem kosztów, efektów i możliwości realizacji. Celem tego etapu
          jest dostarczenie danych niezbędnych do podjęcia decyzji o kontynuowaniu prac nad projektem.</p>
          <p><strong>Analiza systemu</strong> - Faza ta obejmuje stworzenie modelu logicznego przepływu danych i procesów
          niezbędnych do realizacji opisanych funkcji. Efektem tej fazy jest model logiczny systemu uzupełniony o
          specyfikację algorytmów przetwarzania, słowniki danych i modele danych.</p>
          <p><strong>Projektowanie ogólne</strong> – Po analizie systemu, projektanci mogą przystąpić do konstrukcji logicznej
          struktury systemu. Następuje wybór optymalnej platformy sprzętowej, struktury bazy danych, itp. Efektem
          tej fazy są różne warianty projektowanego systemu, wraz z ich oceną dotyczącą kosztu, wpływu na
          organizacje i prognozowanych efektów wdrożenia.</p>
          <p><strong>Projektowanie szczegółowe</strong> - Po akceptacji modelowego rozwiązania niezbędne jest stworzenie wymagań
          dotyczących koniecznych zakupów sprzętu i oprogramowania. Następujące pytania wymagają odpowiedzi:
          jakie programy należy napisać, jaki sprzęt należy kupić, jaka powinna być struktura bazy danych oraz
          systemu plików, jaki powinien być harmonogram wdrożenia</p>
          <p><strong>Wdrożenie</strong> - W czasie wdrożenia, zaprojektowany system jest fizycznie tworzony. Programy są pisane przez
          programistów i instalowane na zakupionym sprzęcie. Przeprowadzane są testy działania systemu i
          współpracy różnych programów. Tworzona jest struktura bazy danych, następuje przeniesienie danych
          historycznych ze starych systemów do nowego systemu.</p>
          <p><strong>Przejście na nowy system</strong> - Jest to etap, w którym przedsiębiorstwo zaczyna wykorzystywać nowy system
          w działalności operacyjnej.</p>
          <p><strong>Eksploatacja oraz ocena działania systemu</strong> - W tym momencie firma posiada już uruchomiony i
          wykorzystywany w codziennej pracy system.</p>
        `
      },
      {
        "id": 1,
        "question": "Modele organizacyjne wytwarzania systemów informatycznych i ich charakterystyka – zalety i wady.",
        "answer": `
        <p>Modele cyklu wytwarzania systemów internetowych porządkują przebieg prac, ułatwiają planowanie zadań oraz monitorowanie realizacji zadań. Dzielimy je na:</p>
        <p><strong>Model kaskadowy (wodospadu)</strong> - W modelu tym, aby zbudować system informatyczny należy przejść przez kolejne etapy, których realizacja ma zapewnić zakończenie projektu. Wyjście z jednego etapu jest równocześnie wejściem w kolejny, bez możliwości powrotu.</p>
        <p><strong>Fazy</strong></p>
        <ul>
          <li>Określanie wymagań – określane są cele oraz szczegółowe wymagania wobec tworzonego systemu,</li>
          <li>Analiza – budowany jest logiczny model systemu</li>
          <li>Projektowanie - powstaje szczegółowy projekt systemu</li>
          <li>Implementacja/kodowanie oraz testowania modułów - projekt zostaje zaimplementowany w konkretnym środowisku programistycznym oraz wykonywane są testy poszczególnych modułów,</li>
          <li>Testowanie - następuje integracja poszczególnych modułów połączona z testowaniem poszczególnych podsystemów oraz całego oprogramowania,</li>
          <li>Wdrożenie - system jest wykorzystywany przez użytkownika(ów), a producent dokonuje konserwacji oprogramowania — wykonuje modyfikacje polegające na usuwaniu błędów, zmianach i rozszerzaniu funkcji systemu</li>
        </ul>
        <p><strong>Zalety</strong></p>
        <ul>
          <li>łatwe planowanie, harmonogramowanie oraz monitorowanie przedsięwzięcia</li>
          <li>zmusza do zdyscyplinowanego podejścia</li>
        </ul>
        <p><strong>Wady</strong></p>
        <ul>
          <li>Wymagania klienta muszą być w dużym stopniu sprecyzowane. W przypadku ich zmiany koszty znacznie wzrastają a wszystkie etapy muszą być przechodzone od nowa.</li>
          <li>Kolejność wykonywania prac musi być ściśle przestrzegana.</li>
          <li>Weryfikacja zgodności produktu z wymaganiami i jego użyteczności następuje dopiero w końcowych krokach.</li>
          <li>Błędy popełnione we wstępnych etapach (zbierania lub analizy wymagań) mogą być wykryte dopiero na etapie testów akceptacyjnych, bądź eksploatacji, a koszty ich naprawy są bardzo wysokie.</li>
          <li>Próba dopasowania produktu do zmieniających się wymagań, powoduje znaczący wzrostu kosztów budowy systemu.</li>
          <li>Długa przerwa w kontaktach z klientem może spowodować zmniejszenie zainteresowania produktem. Klient uczestniczy w projekcie na samym początku przy określaniu wymogów i analiz a następny jego udział jest dopiero na etapie wdrażania.</li>
        </ul>
        <p><strong>Model przyrostowy</strong> jest wariantem modelu ewolucyjnego. W tym modelu rozpoznanie i analiza dotyczą całego systemu. Dopiero po sformułowaniu problemu i określeniu pełnej koncepcji następuje podział systemu na moduły (przyrosty), które są z osobna projektowane, programowane a następnie wdrażane.</p>
        <p><strong>Fazy</strong></p>
          <li>ogólne określenie wymagań,</li>
          <li>budowa prototypu,</li>
          <li>weryfikacja działającego prototypu przez klienta,</li>
          <li>uszczegółowienie wymagań</li>
          <li>realizacja pełnego systemu według modelu kaskadowego</li>
        <p><strong>Zalety</strong></p>
        <ul>
          <li>częste kontakty z klientem (skrócenie przerw w porównaniu z modelem kaskadowym)</li>
          <li>brak konieczności zdefiniowania z góry całości wymagań</li>
          <li>możliwość wczesnego wykorzystania przez klienta dostarczonych części systemu,</li>
          <li>możliwość elastycznego reagowania na opóźnienia realizacji fragmentu – przyspieszenie prac nad inną/innymi częściami (sumarycznie - bez opóźnienia całości przedsięwzięcia projektowego)</li>
        </ul>
        <p><strong>Wady</strong></p>
        <ul>
          <li>dodatkowy koszt związany z niezależną realizacją fragmentów systemu</li>
          <li>potencjalne trudności z wycinaniem podzbioru funkcji w pełni niezależnych</li>
        </ul>
        `
      },
      {
        "id": 2,
        "question": "Istota i znaczenie fazy strategicznej w realizacji przedsięwzięć informatycznych.",
        "answer": `
          <p><strong>Znaczenie fazy strategicznej</strong></p>
          <ul>
            <li>Uniknięcie nieporozumień, nieścisłości dzięki precyzyjnie zdefiniowanym celom projektu - z punktu widzenia klienta.</li>
            <li>Oszacowanie rozmiaru systemu</li>
            <li>Oszacowanie pracochłonności</li>
            <li>Oszacowanie harmonogramu</li>
          </ul>
          <p><strong>Faza strategiczna</strong>: Ocenia się czy rozpoznane potrzeby klientów mogą być spełnione przy obecnych
          technologiach sprzętu i/lub oprogramowania.</p>
        `
      },
      {
        "id": 3,
        "question": "Metody identyfikacji wymagań na systemy informatyczne i ich charakterystyka.",
        "answer": `
          <p><strong>Metodyki</strong></p>
          <ul>
            <li>Postulują przebieg procesu projektowania</li>
            <li>Wyznaczają zadania i kolejność ich wykonywania - co, kiedy i dlaczego powinno być wykonane</li>
            <li>Wskazują zastosowanie odpowiednich technik</li>
          </ul>
          <p><strong>Strukturalne</strong></p>
          <ul>
            <li>Dane i procesy modelowane osobno</li>
            <li>Wykorzystują w zasadzie tylko proste typy danych</li>
            <li>Dobrze dostosowane do modelu relacyjnego danych</li>
            <li>Podstawowe techniki:</li>
            <ul class="pl-8">
              <li>diagramy związków encji (ERD)</li>
              <li>hierarchie funkcji (FHD)</li>
              <li>diagramy przepływu danych (DFD)</li>
            </ul>
          </ul>
          <p><strong>Obiektowe</strong></p>
          <ul>
            <li>Dane i procesy są modelowane łącznie</li>
            <li>Wykorzystują złożone typy danych</li>
            <li>Dostosowane do obiektowego modelu danych</li>
            <li>W przypadku realizacji opartej na relacyjnej b.d.</li>
            <li>wynikowy obiektowy model danych musi być odpowiednio przekształcony</li>
            <li>Podstawowe techniki:</li>
            <ul class="pl-8">
              <li>diagramy klas UML</li>
              <li>przypadki użycia,</li>
              <li>modele dynamiczne UML</li>
            </ul>
          </ul>
        `
      },
      {
        "id": 4,
        "question": "Znaczenie modelowania obiektowego funkcjonowania informatyzowanej organizacji w ustalaniu wymagań na system informatyczny.",
        "answer": `
          <p>Obiektowe modelowanie funkcjonowania organizacji określane jest mianem modelowania biznesowego.
          Służy do opisu wszystkiego, co składa się na daną organizację (dokumentacji, procedur i procesów
          biznesowych). Dzięki modelom biznesowym można odpowiedzieć na 6 kluczowych pytań dla każdej
          organizacji biznesowej: co, jak, dlaczego, kto, gdzie i kiedy.</p>
          <p>Modelowanie biznesowe pozwoli zrozumieć czym zajmuje się dane przedsiębiorstwo, czemu akurat tym i
          czemu akurat w taki sposób. Ponadto można stwierdzić za co odpowiedzialne są konkretne osoby w
          analizowanej firmie, jaka jest ich rola, czy też zakres współpracy z innymi pracownikami.</p>
          <p>Zasadniczym celem budowy modelu biznesowego jest utworzenie takiego obrazu organizacji, który
          będąc opisem rzeczywistości stanie się podstawą szkieletu aplikacji (opisem tego szkieletu).</p>
          <p>Zrozumienie istoty procesów biznesowych jest podstawą specyfikacji wymagań oraz analizy i
          projektowania systemów informatycznych. Wiele metodyk przypisuje temu zagadnieniu wysoki priorytet.</p>
        `
      },
      {
        "id": 5,
        "question": "Pożądana zawartość dokumentu „Wymagania na system informatyczny”.",
        "answer": `
          <ul>
            <li><strong>Wprowadzenie</strong> – cele, zakres i kontekst systemu</li>
            <li><strong>Opis wymagań funkcjonalnych</strong> - opisują funkcje wykonywane przez system. Mogą być opisywane za pomocą: Języka naturalnego, Języka naturalnego strukturalnego, Tablic i Formularzy, Diagramów blokowych, Diagramów kontekstowych i DPU</li>
            <li><strong>Opis wymagań niefunkcjonalnych</strong> - Wymagania niefunkcjonalne – opisują ograniczenia, przy których system ma realizować swoje funkcje;</li>
            <li><strong>Opis ewolucji systemu</strong> – rozwój systemu</li>
            <li><strong>Model logiczny systemu, opracowany przy użyciu np. języka UML</strong> - Model logiczny to zbiór informacji określający zachowanie się systemu. elementy: lista funkcji systemu, określenie obiektów zew. systemu, określenie współzależności między funkcjami systemu</li>
            <li><strong>Słownik terminów niejednoznacznych na udziałowców przedsięwzięcia projektowego</strong> -
            Słownik zawiera terminy, które mogą być niejednoznacznie interpretowane przez uczestników
            przedsięwzięcia informatycznego- co może być przyczyną nieporozumień podczas jego realizacji;</li>
          </ul>
        `
      },
      {
        "id": 6,
        "question": "Podstawowe rezultaty fazy modelowania systemu.",
        "answer": `
          <ol>
            <li>diagram klas</li>
            <li>diagram przypadków użycia SI</li>
            <li>diagramy sekwencji</li>
            <li>diagramy stanów</li>
            <li>diagramy czynności</li>
            <li>raport zawierający definicje i opisy klas, atrybutów, związków, metod, itd.</li>
            <li>słownik danych, zawierający specyfikację modelu</li>
            <li>poprawiony dokument opisujący wymagania</li>
            <li>harmonogram fazy projektowania</li>
          </ol>
        `
      },
      {
        "id": 7,
        "question": "Podstawowe zadania realizowane w procesie budowy obiektowego modelu systemu informatycznego.",
        "answer": `
          <p><strong>Proces tworzenia modelu obiektowego:</strong></p>
          <ul>
            <li>Identyfikacja klas i obiektów</li>
            <li>Identyfikacja związków pomiędzy klasami</li>
            <li>Identyfikacja i definiowanie pól (atrybutów)</li>
            <li>Identyfikacja i definiowanie metod i komunikatów</li>
          </ul>
          <p>Czynności te są wykonywane iteracyjnie. Kolejność ich wykonywania nie jest ustalona i zależy zarówno od
          stylu pracy, jak i od konkretnego problemu.</p>
        `
      },
      {
        "id": 8,
        "question": "Rodzaje modyfikacji wprowadzanych w fazie pielęgnacji systemu informatycznego.",
        "answer": `
          <p>Faza pielęgnacji rozpoczyna się po fazie testowania, kiedy klient rozpoczyna użytkowanie systemu. Jest to
          najczęściej najdłuższa faza cyklu życia oprogramowania, gdyż obejmuje okres eksploatacji
          oprogramowania jak i jego utrzymania.</p>
          <p>Pielęgnacja oprogramowania polega na wprowadzeniu modyfikacji właściwości użytkowych. Istnieją trzy
          rodzaje takich modyfikacji:</p>
          <ul>
            <li><strong>Modyfikacje poprawiające</strong> – polegają na usuwaniu z oprogramowania błędów powstałych bądź wykrytych w poprzednich fazach</li>
            <li><strong>Modyfikacje ulepszające</strong> – polegają na poprawie jakości oprogramowania,</li>
            <li><strong>Modyfikacje dostosowujące</strong> – polegają na dostosowaniu oprogramowania do zmian zachodzących w wymaganiach użytkownika lub w środowisku komputerowym</li>
          </ul>
        `
      }
    ]
  },
  {
    "id": 11,
    "name": "Podstawy logiki i teorii mnogości, algebra liniowa, analiza matematyczna, metody probabilistyczne",
    "icon": "percent",
    "chance": 98,
    "questions": [
      {
        "id": 0,
        "question": "Działania na zbiorach.",
        "answer": `
          <p><strong>Suma zbiorów</strong></p>
          <p>Sumą zbiorów A i B nazywamy zbiór tych
          elementów, które należą do zbioru A lub do
          zbioru B, matematycznie zapisujemy ją
          tak: A ⋃ B = {x: x ∈ A V x ∈ B }</p>
          <p><strong>Różnica zbiorów</strong></p>
          <p>Różnicą zbiorów A i B nazywamy zbiór tych
          elementów, które należą do zbioru A, a które
          nie należą do zbioru B, możemy ją zapisać
          tak: A \\ B = {x: x ∈ A ^ x ∉ B}<p>
          <p>Różnica zbiorów A i B zapisywana jest też A − B.</p>
          <p><strong>Iloczyn zbiorów</strong></p>
          <p>Iloczynem zbioru A i B nazywamy zbiór tych
          elementów, które należą jednocześnie do
          zbioru A i do zbioru B, formalnie zapisujemy ją
          tak: A ⋂ B = {x: x ∈ A ^ x ∈ B }</p>
          <p>Iloczyn zbiorów nazywany jest także częścią
          wspólną zbiorów lub przekrojem zbiorów.</p>
          <p><strong>Dopełnienie zbioru</strong></p>
          <p><strong>Dopełnieniem</strong> zbioru A z przestrzeni <em>U</em>
          nazywamy zbiór tych elementów przestrzeni <em>U</em>,
          które <strong>nie należą</strong> do zbioru A. Dopełnienie
          zbioru A oznaczamy jako A' lub A<sup>c</sup>. Dopełnienie
          możemy zapisać tak: A' = {x: x ∈ U ^ x ∉ A }.</p>
          <p>Z definicji dopełniania wynika także, że jest to
          po prostu różnica przestrzeni U i zbioru A: A' = U \\ A. Zbiór <em>U</em> zwany jest zbiorem
          uniwersum. Czasami zamiast <em>U</em> używa się innego oznaczenia przestrzeni np. <em>X</em>.</p>
          <p>(A ⋃ B)' = A' ⋂ B' - I prawo De Morgana</p>
          <p>(A ⋂ B)' = A' ⋃ B' - II prawo De Morgana</p>
          <p>A ⋃ B = B ⋃ A - przemienność dodawania zbiorów</p>
          <p>A ⋂ B = B ⋂ A - przemienność mnożenia zbiorów</p>
          <p>(A ⋃ B) ⋃ C = A ⋃ (B ⋃ C) - łączność dodawania zbiorów</p>
          <p>(A ⋂ B) ⋂ C = A ⋂ (B ⋂ C) - łączność mnożenia zbiorów</p>
          <p>A ⋃ (B ⋂ C) = (A ⋃ B) ⋂ (A ⋃ C) - rozdzielność dodawania zbiorów względem mnożenia</p>
          <p>A ⋂ (B ⋃ C) = (A ⋂ B) ⋃ (A ⋃ C) - rozdzielność mnożenia zbiorów względem dodawania</p>
        `
      },
      {
        "id": 1,
        "question": "Rachunek zdań.",
        "answer": `
          <p>W klasycznym rachunku zdań przyjmuje się założenie, że każdemu zdaniu można przypisać jedną z dwu
          wartości logicznych - prawdę albo fałsz, które umownie przyjęto oznaczać 1 i 0. Klasyczny rachunek zdań
          jest więc dwuwartościowym rachunkiem zdań.</p>
          <p>Wartość logiczną zdań określa funkcja prawdy, związana z każdym spójnikiem zdaniowym. Wartość ta
          zależy wyłącznie od prawdziwości lub fałszywości zdań składowych. Szczególną rolę w rachunku zdań
          odgrywają takie zdania złożone, dla których wartość logiczna jest równa 1, niezależnie od tego, jakie
          wartości logiczne mają zdania proste, z których się składają. Takie zdania nazywa się prawami rachunku
          zdań lub tautologiami.</p>
          <p><strong>Zmienne zdaniowe</strong>: p, q, r, s, itd.</p>
          <p><strong>Funktory</strong>: koniunkcja, alternatywa, równoważność, implikacja, itd.</p>
          <p><strong>Znaki pomocnicze</strong>: nawiasy: (, ), [, ], {, }.</p>
          <ul>
            <li>(p ^ q) <=> (q ^ p) - prawo przemienności koniunkcji</li>
            <li>(p v q) <=> (q v p) - prawo przemienności alternatywy</li>
            <li>[(p ^ q) ^ r] <=> [p ^ (q ^ r)] - prawo łączności koniunkcji</li>
            <li>[(p v q) v r] <=> [p v (q v r)] - prawo łączności alternatywy</li>
            <li>[p ^ (q v r)] <=> [(p ^ q) v (p ^ r)] - prawo rozdzielności koniunkcji względem alternatywy</li>
            <li>[p v (q ^ r)] <=> [(p v q) ^ (p v r)] - prawo rozdzielności alternatywy względem koniunkcji</li>
            <li>~(p ^ q) <=> (~p v ~q) - pierwsze prawo De Morgana</li>
            <li>~(p v q) <=> (~p ^ ~q) - drugie prawo De Morgana</li>
          </ul>
        `
      },
      {
        "id": 2,
        "question": "Działania na macierzach.",
        "answer": `
          <p><strong>Dodawanie i mnożenie przez skalar</strong></p>
          <p>Mnożenie macierzy przez stałą (skalar, element pierścienia) oraz dodawanie macierzy definiuje się
          następująco:</p>
          </ul>
            <li>a · (a<sub>ij</sub>) = (a · a<sub>ij</sub>)</li>
            <li>(a<sub>ij</sub>) + (b<sub>ij</sub>) = (a<sub>ij</sub> + b<sub>ij</sub>)</li>
          </ul>
          <p>Słownie: mnożenie macierzy przez skalar polega na wymnożeniu przez niego wszystkich jej elementów, a
          dodawanie macierzy na dodaniu odpowiadających sobie elementów. Analogicznie definiuje się różnicę
          macierzy.</p>
          <p>Mnożenie przez skalar spełnia warunki:</p>
          <ul>
            <li>(a + b) · A = a · A + b · A</li>
            <li>a · (A + B) = a · A + a · B</li>
            <li>(ab) · A = a · (b · A)</li>
            <li>1 · A = A</li>
          </ul>
          <p><strong>Przykład mnożenia przez skalar</strong></p>
          <div class="flex items-center">
            <span>3 · </span>
            <div class="matrix">
              <table>
                <tr>
                  <td>1</td>
                  <td>2</td>
                  <td>0</td>
                </tr>
                <tr>
                  <td>-1</td>
                  <td>-4</td>
                  <td>9</td>
                </tr>
              </table>
            </div>
            <span> = </span>
            <div class="matrix">
              <table>
                <tr>
                  <td>3·1</td>
                  <td>3·2</td>
                  <td>3·0</td>
                </tr>
                <tr>
                  <td>3·(-1)</td>
                  <td>3·(-4)</td>
                  <td>3·9</td>
                </tr>
              </table>
            </div>
            <span>=</span>
            <div class="matrix">
              <table>
                <tr>
                  <td>3</td>
                  <td>6</td>
                  <td>0</td>
                </tr>
                <tr>
                  <td>-3</td>
                  <td>-12</td>
                  <td>27</td>
                </tr>
              </table>
            </div>
          </div>
        `
      },
      {
        "id": 3,
        "question": "Układy równań liniowych – twierdzenie Kroneckera-Capelliego, wzory Cramera.",
        "answer": `
          <p><strong>Układy równań liniowych</strong></p>
          <p>Równaniem liniowym o n niewiadomych x<sub>1</sub>, x<sub>2</sub>, ..., x<sub>n</sub> nazywamy równanie postaci</p>
          <p>a<sub>1</sub>x<sub>1</sub> + a<sub>2</sub>x<sub>2</sub> + ... + a<sub>n</sub>x<sub>n</sub> = b</p>
          <p><strong>Układem dwóch równań liniowych o dwóch niewiadomych</strong> nazwiemy wyrażenie postaci</p>
          <div class="equations">
            <div class="flex flex-col">
              <span>c<sub>1</sub>x + b<sub>1</sub>y = c<sub>1</sub></span>
              <span>c<sub>2</sub>x + b<sub>2</sub>y = c<sub>2</sub></span>
            </div>
          </div>
          <p><strong>Układ <em>n</em> równań liniowych o <em>n</em> niewiadomych ma postać</strong></p>
          <div class="equations">
            <div class="flex flex-col">
              <span>a<sub>11</sub>x<sub>1</sub> + a<sub>12</sub>x<sub>2</sub> + ... + a<sub>1n</sub>x<sub>n</sub> = b<sub>1</sub></span>
              <span>a<sub>21</sub>x<sub>1</sub> + a<sub>22</sub>x<sub>2</sub> + ... + a<sub>2n</sub>x<sub>n</sub> = b<sub>2</sub></span>
              <span>...</span>
              <span>a<sub>n1</sub>x<sub>1</sub> + a<sub>n2</sub>x<sub>2</sub> + ... + a<sub>nn</sub>x<sub>n</sub> = b<sub>n</sub></span>
            </div>
          </div>
          <p><strong>Wzory Cramera</strong></p>
          <p>Najpierw wyznaczmy tzw wyznacznik główny <em>W</em></p>
          <div class="flex flex-row items-center">
            <span>W = </span>
            <div class="matrix matrix--flat-border">
              <table>
                <tr>
                  <td>a<sub>11</sub></td>
                  <td>a<sub>12</sub></td>
                  <td>a<sub>13</sub></td>
                  <td>...</td>
                  <td>a<sub>1n</sub></td>
                </tr>
                <tr>
                  <td>a<sub>21</sub></td>
                  <td>a<sub>22</sub></td>
                  <td>a<sub>23</sub></td>
                  <td>...</td>
                  <td>a<sub>2n</sub></td>
                </tr>
                <tr>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-45">...</td>
                  <td class="transform rotate-90">...</td>
                </tr>
                <tr>
                  <td>a<sub>n1</sub></td>
                  <td>a<sub>n2</sub></td>
                  <td>a<sub>n3</sub></td>
                  <td>...</td>
                  <td>a<sub>nn</sub></td>
                </tr>
              </table>
            </div>
          </div>
          <p>Następnie utwórzmy <em>n</em> wyznaczników W<sub>1</sub>, W<sub>2</sub>, ..., W<sub>n</sub></p>
          <div class="flex flex-row items-center">
            <span>W<sub>1</sub> = </span>
            <div class="matrix matrix--flat-border">
              <table>
                <tr>
                  <td>b<sub>1</sub></td>
                  <td>a<sub>12</sub></td>
                  <td>a<sub>13</sub></td>
                  <td>...</td>
                  <td>a<sub>1n</sub></td>
                </tr>
                <tr>
                  <td>b<sub>2</sub></td>
                  <td>a<sub>22</sub></td>
                  <td>a<sub>23</sub></td>
                  <td>...</td>
                  <td>a<sub>2n</sub></td>
                </tr>
                <tr>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-45">...</td>
                  <td class="transform rotate-90">...</td>
                </tr>
                <tr>
                  <td>b<sub>n</sub></td>
                  <td>a<sub>n2</sub></td>
                  <td>a<sub>n3</sub></td>
                  <td>...</td>
                  <td>a<sub>nn</sub></td>
                </tr>
              </table>
            </div>
          </div>
          <div class="flex flex-row items-center">
            <span>W<sub>2</sub> = </span>
            <div class="matrix matrix--flat-border">
              <table>
                <tr>
                  <td>a<sub>11</sub></td>
                  <td>b<sub>1</sub></td>
                  <td>a<sub>13</sub></td>
                  <td>...</td>
                  <td>a<sub>1n</sub></td>
                </tr>
                <tr>
                  <td>a<sub>21</sub></td>
                  <td>b<sub>2</sub></td>
                  <td>a<sub>23</sub></td>
                  <td>...</td>
                  <td>a<sub>2n</sub></td>
                </tr>
                <tr>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-45">...</td>
                  <td class="transform rotate-90">...</td>
                </tr>
                <tr>
                  <td>a<sub>n1</sub></td>
                  <td>b<sub>n</sub></td>
                  <td>a<sub>n3</sub></td>
                  <td>...</td>
                  <td>a<sub>nn</sub></td>
                </tr>
              </table>
            </div>
          </div>
          <div class="flex flex-row items-center">
            <span>W<sub>n</sub> = </span>
            <div class="matrix matrix--flat-border">
              <table>
                <tr>
                  <td>a<sub>11</sub></td>
                  <td>a<sub>12</sub></td>
                  <td>a<sub>13</sub></td>
                  <td>...</td>
                  <td>b<sub>1</sub></td>
                </tr>
                <tr>
                  <td>a<sub>21</sub></td>
                  <td>a<sub>22</sub></td>
                  <td>a<sub>23</sub></td>
                  <td>...</td>
                  <td>b<sub>2</sub></td>
                </tr>
                <tr>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-45">...</td>
                  <td class="transform rotate-90">...</td>
                </tr>
                <tr>
                  <td>a<sub>n1</sub></td>
                  <td>a<sub>n2</sub></td>
                  <td>a<sub>n3</sub></td>
                  <td>...</td>
                  <td>b<sub>n</sub></td>
                </tr>
              </table>
            </div>
          </div>
          <p>Dla w. w. wyznaczników zachodzą 3 przypadki</p>
          <span class="flex items-center">1. W ≠ 0 => x<sub>1</sub> = <div class="fraction"><div>W<sub>1</sub></div><div>W</div></div>, x<sub>2</sub> = <div class="fraction"><div>W<sub>2</sub></div><div>W</div></div>, ..., x<sub>n</sub> = <div class="fraction"><div>W<sub>n</sub></div><div>W</div></div></span>
          <p>2. W = 0 ^ nie każdy W<sub>i</sub> = 0, i = 1,2,3,...,n => układ jest sprzeczny</p>
          <p>3. W = 0 ^ W<sub>1</sub> = 0, W<sub>2</sub> = 0, ..., W<sub>n</sub> = 0 => układ jest nieoznaczony <=> ma nieskończenie wiele rozwiązań</p>
          <p><strong>Twierdzenie Kroneckera-Capelli'ego</strong></p>
          <p>Dla układu <em>m</em> równań liniowych (jednorodnych albo niejednorodnych) o <em>n</em> niewiadomych (m < n V m = n V m > n)</p>
          <div class="equations">
            <div class="flex flex-col">
              <span>a<sub>11</sub>x<sub>1</sub> + a<sub>12</sub>x<sub>2</sub> + ... + a<sub>1n</sub>x<sub>n</sub> = b<sub>1</sub></span>
              <span>a<sub>21</sub>x<sub>1</sub> + a<sub>22</sub>x<sub>2</sub> + ... + a<sub>2n</sub>x<sub>n</sub> = b<sub>2</sub></span>
              <span>...</span>
              <span>a<sub>m1</sub>x<sub>1</sub> + a<sub>m2</sub>x<sub>2</sub> + ... + a<sub>mn</sub>x<sub>n</sub> = b<sub>m</sub></span>
            </div>
          </div>
          <p>utwórzmy macierz W ze współczynników a<sub>11</sub>, a<sub>12</sub>, ..., a<sub>1n</sub>, ..., a<sub>nm</sub></p>
          <div class="flex flex-row items-center">
            <span>W = </span>
            <div class="matrix">
              <table>
                <tr>
                  <td>a<sub>11</sub></td>
                  <td>a<sub>12</sub></td>
                  <td>a<sub>13</sub></td>
                  <td>...</td>
                  <td>a<sub>1n</sub></td>
                </tr>
                <tr>
                  <td>a<sub>21</sub></td>
                  <td>a<sub>22</sub></td>
                  <td>a<sub>23</sub></td>
                  <td>...</td>
                  <td>a<sub>2n</sub></td>
                </tr>
                <tr>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-45">...</td>
                  <td class="transform rotate-90">...</td>
                </tr>
                <tr>
                  <td>a<sub>m1</sub></td>
                  <td>a<sub>m2</sub></td>
                  <td>a<sub>m3</sub></td>
                  <td>...</td>
                  <td>a<sub>mn</sub></td>
                </tr>
              </table>
            </div>
          </div>
          <p>Przez r(W) oznaczamy <strong>rząd</strong> macierzy W - największy stopień (różny od 0) minora z tej macierzy (jeśli wszystkie elementy macierzy wynoszą 0 to r(W) = 0).</p>
          <p>Dopisując do macierzy <em>W</em> kolumnę wyrazów wolnych, otrzymujemy macierz uzupełnioną <em>U</em>.
          <div class="flex flex-row items-center">
            <span>W = </span>
            <div class="matrix">
              <table>
                <tr>
                  <td>a<sub>11</sub></td>
                  <td>a<sub>12</sub></td>
                  <td>a<sub>13</sub></td>
                  <td>...</td>
                  <td>a<sub>1n</sub></td>
                  <td>b<sub>1</sub></td>
                </tr>
                <tr>
                  <td>a<sub>21</sub></td>
                  <td>a<sub>22</sub></td>
                  <td>a<sub>23</sub></td>
                  <td>...</td>
                  <td>a<sub>2n</sub></td>
                  <td>b<sub>2</sub></td>
                </tr>
                <tr>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-90">...</td>
                  <td class="transform rotate-45">...</td>
                  <td class="transform rotate-90">...</td>
                </tr>
                <tr>
                  <td>a<sub>m1</sub></td>
                  <td>a<sub>m2</sub></td>
                  <td>a<sub>m3</sub></td>
                  <td>...</td>
                  <td>a<sub>mn</sub></td>
                  <td>b<sub>m</sub></td>
                </tr>
              </table>
            </div>
          </div>
          <p><strong>Twierdzenie Kroneckera-Capelli'ego</strong> mówi: warunkiem koniecznym i wystarczającym rozwiązalności
          układu równań liniowych jest równość rzędu macierzy <em>W</em> współczynników układu i rzędu macierzy
          uzupełnionej <em>U</em>
          <p>r(W) = r(U) = r</p>
          <p>Gdy:</p>
          <ol>
            <li>r = n => układ ma <strong>dokladnie jedno rozwiązanie</strong></li>
            <li>r < n => układ ma <strong>nieskończenie wiele rozwiązań</strong> zależnych od <em>n - r</em> parametrów</li>
          </ol>
        `
      },
      {
        "id": 4,
        "question": "Pojęcie relacji i funkcji.",
        "answer": `
          <p><strong>Relacja</strong></p>
          <p>Relacją dwuczłonową R nazywa się dowolny podzbiór iloczynu kartezjańskiego A × B dowolnych zbiorów A i B. <strong>R ⊂ A × B</strong></p>
          <p><em>Własności relacji</em>:</p>
          <ul>
            <li>zwrotna (Z), jeśli ∀x ∈ A : xRx</li>
            <li>przeciwzwrotna (PZ), jeśli ∀x ∈ A : ∼ xRx</li>
            <li>symetryczna (S), jeśli ∀x ∈ A, y ∈ A : xRy ⇒ yRx</li>
            <li>antysymetryczna (AS), jeśli ∀x ∈ A, y ∈ A : xRy ∧ yRx ⇒ x = y</li>
            <li>przechodnia (P), jeśli ∀x, y, z ∈ A : xRy ∧ yRz ⇒ xRz</li>
          </ul>
          <p><em>R jest relacją równoważności, gdy</em><p>
          <ol>
            <li>R jest zwrotne,</li>
            <li>R jest symetryczne,</li>
            <li>R jest przechodnie,</li>
          </ol>
          <p><em>R jest relacją częściowego porządku, gdy</em></p>
          <ol>
            <li>R jest zwrotne,</li>
            <li>R jest antysymetryczne,</li>
            <li>R jest przechodnie,</li>
          </ol>
          <p><em>R jest relacją liniowego porządku, gdy</em></p>
          <ol>
            <li>R jest relacją częściowego porządku,</li>
            <li>R jest relacją spójną,</li>
          </ol>
          <p><strong>Funkcja</strong></p>
          <p>Przyporządkowanie każdemu elementowi
          zbioru X dokładnie jednego elementu ze
          zbioru Y. Funkcja jest relacją dwuczłonową.</p>
          <p><em>Funkcje można określić za pomocą</em>:</p>
          <ul>
            <li>grafu</li>
            <li>wykresu</li>
            <li>wzoru</li>
            <li>zbioru</li>
            <li>tabelki</li>
            <li>opisu słownego</li>
          </ul>
          <p><strong>Dziedzina funkcji</strong> - zbiór X</p>
          <p><strong>Zbiór wartości funkcji</strong> - zbiór Y</p>
          <p><strong>Miejsce zerowe funkcji</strong> - argument x dla którego funkcja przyjmuje wartość 0, na wykresie jest to punkt przecięcia z osią X</p>
          <p><strong>Monotoniczność funkcji</strong> - określa czy funkcja jest rosnąca czy malejąca.</p>
          <p>Funkcja jest <strong>rosnąca</strong> gdy wraz ze wzrostem argumentów rosną wartości funkcji</p>
          <p>Funkcja jest <strong>malejąca</strong> gdy wraz ze wzrostem argumentów maleją wartości funkcji</p>
        `
      },
      {
        "id": 5,
        "question": "Własności relacji: relacje porządkujące; relacje równoważności.",
        "answer": `
          <p>Relacją w A<sub>1</sub> x … x A<sub>n</sub> (iloczyn kartezjański) nazywamy dowolny zbiór R ⊂ A<sub>1</sub> x … x A<sub>n</sub>.
          Nazywamy ją relacją n-argumentową. Relacje dwuargumentowe to R ⊂ A x B.</p>
          <p>xRy <=> (x,y) ∈ R</p>
          <p>Np. R = {x ∈ R<sup>2</sup>: x < y}</p>
          <p><strong>Relacja równoważności</strong></p>
          <p>R ⊂ X<sup>2</sup> jest relacją</p>
          <ul>
            <li>zwrotna, jeśli ∀x ∈ A : xRx</li>
            <li>symetryczna, jeśli ∀x ∈ A, y ∈ A : xRy ⇒ yRx</li>
            <li>przechodnia, jeśli ∀x, y, z ∈ A : xRy ∧ yRz ⇒ xRz</li>
          </ul>
          <p>Np.</p>
          <p>xRy <=> x = y na zbiorze R</p>
          <p>xRy <=> |x| = |y| na zbiorze R</p>
          <p><strong>Relacje porządkujące</strong></p>
          <p><strong>Częściowo porządkująca</strong></p>
          <p>R ⊂ X<sup>2</sup> jest relacją</p>
          <ul>
            <li>zwrotna, jeśli ∀x ∈ A : xRx</li>
            <li>antysymetryczna, jeśli ∀x ∈ A, y ∈ A : xRy ∧ yRx ⇒ x = y</li>
            <li>przechodnia, jeśli ∀x, y, z ∈ A : xRy ∧ yRz ⇒ xRz</li>
          </ul>
          <p><strong>Porządkująca i dodatkowo spójna</strong></p>
          <p>Relacja porządkująca, która jest spójna</p>
          <p>∀x, y ∈ A : x ≠ y ⇒ xRy V yRx</p>
          <p>Np.</p>
          <p>xRy <=> x ≤ y</p>
        `
      },
      {
        "id": 6,
        "question": "Własności funkcji: miejsca zerowe, ciągłość, pochodna.",
        "answer": `
          <p><strong>Ciągłość</strong></p>
          <p>Niech funkcja f będzie określona w pewnym otoczeniu U punktu x<sub>0</sub></p>
          Funkcję f nazywamy ciągłą w punkcie x<sub>0</sub>, jeżeli istnieje jej granica w tym punkcie i
          <div class="flex flex-row items-center">
            <div class="lim">x->x0</div>
            <span>f(x) = f(x<sub>0</sub>)
          </div>
          <p>Funkcja f jest ciągła w przedziale otwartym (a, b), jeżeli jest ciągła w każdym punkcie x<sub>0</sub> ∈ (a,b)</p>
          <p>Funkcja f jest ciągła w przedziale domkniętym &lt;a, b&gt;, jeżeli spełnia następujące warunki:</p>
          <p>1. Jest ciągła w (a,b)</p>
          <div class="flex flex-row items-center">
            <span>2.</span>
            <div class="lim">x->a+</div>
            <span>f(x) = f(a) (funkcja prawostronnie ciągła w punkcie a),
          </div>
          <div class="flex flex-row items-center">
            <span>3.</span>
            <div class="lim">x->b-</div>
            <span>f(x) = f(b) (funkcja lewostronnie ciągła w punkcie b),
          </div>
          <p><strong>Miejsce zerowe</strong> (pierwiastek) funkcji to argument, dla którego dana funkcja przyjmuje wartość 0.</p>
          <p>Niech U ⊂ R będzie  przedziałem otwartym i funkcja f: U -> R.</p>
          <p>Jeśli dla pewnego x<sub>0</sub> ∈ U istnieje skończona granica ilorazu różnicowego</p>
          <div class="flex flex-row items-center">
            <div class="lim">x->x0</div>
            <div class="fraction">
              <div>f(x) - f(x<sub>0</sub>)</div>
              <div>x - x<sub>0</sub></div>
            </div>
            <span>=</span>
            <div class="lim">x->x0</div>
            <div class="fraction">
              <div>f(x<sub>0</sub> + h) - f(x<sub>0</sub>)</div>
              <div>h</div>
            </div>
          </div>
          <p>to mówimy, że <em>f</em> jest <strong>różniczkowalna</strong> w punkcie x<sub>0</sub></p>
          <p>Wartość powyższej granicy nazywamy <strong>pochodną funkcji</strong> <em>f</em> w punkcie x<sub>0</sub> i oznaczamy symbolem <em>f'(x<sub>0</sub>)</em>.</p>
        `
      },
      {
        "id": 7,
        "question": "Zmienna losowa i jej charakterystyki liczbowe.",
        "answer": `
          <p>Pojęcie zmiennej losowej: Intuicyjne można powiedzieć, że zmienna losowa (związana z pewnym
          doświadczeniem), to taka zmienna, która w wyniku doświadczenia przyjmuje wartość liczbową zależną od
          przypadku (nie dając ą się ustalić przez przeprowadzeniem doświadczenia).</p>
          <p>Wartość oczekiwana. Obliczenie wartości oczekiwanej zmiennej losowej bezpośrednio za pomocą
          funkcji prawdopodobieństwa lub gęstości prawdopodobieństwa zmiennej losowej X przedstawia
          następujący wzór:</p>
          <p><strong>1</strong>. Zmienna losowa dyskretna – Niech X będzie zmienną losową typu dyskretnego. Wartością
          oczekiwaną nazywa się sumę iloczynów wartości tej zmiennej losowej oraz prawdopodobieństw z jakimi
          są one przyjmowane.</p>
          <div class="flex flex-row items-center">
            <span>E(x) =</span>
            <div class="top-bottom" data-top="n" data-bottom="i=1">∑</div>
            <span>x<sub>i</sub>p<sub>i</sub></span>
          </div>
          <p><strong>2</strong>. Zmienna losowa ciągła - Jeżeli X jest zmienną losową typu ciągłego zdefiniowaną na przestrzeni
          probabilistycznej (Ω, P, F), to wartość oczekiwaną zmiennej losowej definiuje się jako całkę.</p>
          <div class="flex flex-row items-center">
            <span>E(X) =</span>
            <div class="top-bottom" data-top="+∞" data-bottom="-∞">∫</div>
            <span>XdP</span>
          </div>
          <p><strong>Wariancja zmiennej losowej</strong> jest średnią arytmetyczną kwadratów odchyleń (różnic) poszczególnych
          wartości cechy od wartości oczekiwanej.</p>
          <ol>
            <li>D<sup>2</sup>X = E(X<sup>2</sup>) - (EX)<sup>2</sup>,</li>
            <li>D<sup>2</sup>X = E[X - E(X)]<sup>2</sup>,</li>
          </ol>
          <p><strong>Odchylenie standardowe zmiennej losowej</strong>. Odchylenie standardowe zmiennej losowej X jest to
          pierwiastek z jej wariancji i opisany jest wzorem:</p>
          <p>σ = √(D<sup>2</sup>X)</p>
        `
      }
    ]
  },
  {
    "id": 12,
    "name": "Programowanie deklaratywne – paradygmaty programowania",
    "icon": "question",
    "chance": 1,
    "questions": [
      {
        "id": 0,
        "question": "Cel i rodzaje programowania deklaratywnego.",
        "answer": `
          <p><strong>Programowanie deklaratywne</strong> - polega na określeniu oczekiwanych własności rozwiązania które ma być
          uzyskane bez podawania szczegółowego algorytmu jego uzyskania.</p>
          <p><strong>Programowanie funkcyjne</strong>:</p>
          <ul>
            <li>nie ma zmiennych (stanu programu) - tylko argumenty funkcji</li>
            <li>nie ma trwałych efektów ubocznych wykonania funkcji</li>
            <li>nie ma instrukcji iteracyjnych</li>
            <li>instrukcjom iteracyjnym odpowiada rekurencja,</li>
            <li>do rozwiązania bardziej złożonych problemów definiuje sie liczne funkcje pomocnicze</li>
          </ul>
          <p><strong>Przykładowe języki</strong>: LISP, Haskell, można ten styl stosować w większości języków strukturalnych</p>
          <p><strong>Programowanie w logice</strong>:</p>
          <ul>
            <li>Program to sformułowanie własności rozwiązania w języku formalnym pozwalającym na definiowanie predykatów.</li>
            <li>Wykonanie programu polega na znalezieniu takich danych, dla których można "udowodnić" spełnienie wymaganej własności.</li>
            <li>Zadaniem kompilatora i interpretera programu logicznego jest znalezienie ciągu operacji prowadzących do rozwiązania problemu (udowodnienia własności)</li>
          </ul>
          <p><strong>Przykładowe języki</strong>: Prolog, SQL</p>
        `
      },
      {
        "id": 1,
        "question": "Definicje unifikatora (podstawienia uzgadniającego), najogólniejszego unifikatora, algorytm unifikacji i twierdzenie o unifikacji.",
        "answer": `
          <p><strong>Podstawienie uzgadniające</strong> dwóch termów jest to takie podstawienie, które czyni te termy
          identycznymi.</p>
          <p><strong>Najbardziej ogólne podstawienie uzgadniające</strong> dwóch termów to takie podstawienie uzgadniające, w
          którym wspólna instancja jest w najbardziej ogólnej postaci.</p>
          <p><strong>Algorytm unifikacji</strong>: Ideę algorytmu unifikacji można tłumaczyć jako kolejne przeszukiwania struktury
          wyrażeń do unifikacji w celu znalezienia niespójnych elementów i zastąpienie jednego z nich innym.</p>
        `
      },
      {
        "id": 2,
        "question": "Programy Horna, SLD-rezolucja, odpowiedzi poprawne, odpowiedzi obliczone, poprawność i zupełność SLD-rezolucji.",
        "answer": `
          <p><strong>Klauzula i program Horna</strong></p>
          <p>Klauzula Horna jest to dowolna klauzula, zawierająca co najwyżej jeden literał pozytywny.
          Programem Horna nazywamy skończony zbiór klauzul Horna.</p>
          <p><strong>SLD-rezolucja</strong>, jest podstawową regułą wnioskowania wykorzystywaną w programowaniu deklaratywnym.
          SLD-rezolucja to algorytm służący do udowodnienia formuły logiki pierwszego rzędu z zestawu klauzul Horna. Jest
          oparta na liniowej rezolucji.</p>
          <p><strong>Def. Odpowiedź poprawna</strong> jest pojęciem deklaratywnym, związanym ze znaczeniem programu.</p>
          <p><strong>Def. Odpowiedź obliczona</strong> jest pojęciem proceduralnym związanym z wykonaniem programu.</p>
          <p><strong>O poprawności rezolucji liniowej</strong> Jeśli na podstawie metody rezolucji ze zbioru klauzul zostanie wyprowadzona
          klauzula pusta, to zbiór klauzul jest niespełnialny.</p>
          <p><strong>O zupełności rezolucji liniowej</strong> Odpowiedzi poprawne pokrywają się ze wszystkimi możliwymi konkretyzacjami
          odpowiedzi obliczonych.</p>
        `
      },
      {
        "id": 3,
        "question": "Zasada rezolucji liniowej w programowaniu w logice.",
        "answer": `
          <p><strong>1. Rezolucja liniowa</strong>:</p>
          <p>Rezolucja liniowa jest to strategia wnioskowania za pomocą reguły rezolucji, w której każdą, kolejną parę
          przesłanek tworzą:</p>
          <ul>
            <li>rezolwenta,</li>
            <li>dowolny aksjomat, negacja dowodzonej hipotezy lub dowolna rezolwenta skonstruowana wcześniej.</li>
          </ul>
          <p><strong>2. Przykład</strong>:</p>
          <p>KOBIETA(Ewa)</p>
          <p>MEZCZYZNA(Adam)</p>
          <p>~MEZCZYZNA(x) V ~KOBIETA(y) V ~LUBI_WINO(y) V KOCHA(x,y)</p>
          <p>~KOBIETA(x) V LUBI_WINO(x)</p>
          <p>Naszym zadaniem jest, na podstawie powyższych formuł udowodnić prawdziwość formuły KOCHA(Adam, Ewa).</p>
          <p>Pierwszym krokiem jest zanegowanie tej formuły, a następnie dodanie jej w takiej formie do zbioru
          formuł (przy założeniu, że zbiór ten jest niesprzeczny), z których będziemy prowadzić wywód. Po tym
          kroku zbiór formuł wygląda następująco:</p>
          <p>(1) KOBIETA(Ewa)</p>
          <p>(2) MEZCZYZNA(Adam)</p>
          <p>(3) ~MEZCZYZNA(x) V ~KOBIETA(y) V ~LUBI_WINO(y) V KOCHA(x,y)</p>
          <p>(4) ~KOBIETA(x) V LUBI_WINO(x)</p>
          <p>(5) ~KOCHA(Adam, Ewa)</p>
          <p>Teraz można już prowadzić wywód. Naszym celem jest uzyskanie klauzuli pustej.</p>
          <p>(6) ~KOBIETA(y) V ~LUBI_WINO(y) V KOCHA(Adam,y) - otrzymane z formuł 2 i 3, przy zastosowaniu reguły nr 2.</p>
          <p>(7) LUBI_WINO(Ewa) - otrzymane z formuł 2 i 3, przy zastosowaniu reguły nr 2.</p>
          <p>(8) ~KOBIETA(Ewa) V KOCHA(Adam,Ewa) - otrzymane z formuł 6 i 7, przy zastosowaniu reguły nr 2.</p>
          <p>(9) KOCHA(Adam,Ewa) - otrzymane z formuł 1 i 8, przy zastosowaniu reguły Modus ponendo ponens.</p>
          <p>(10) [] - otrzymane z formuł 5 i 9, przy zastosowaniu reguły nr 4.</p>
          <p>Jak widać udało nam się uzyskać klauzulę pustą. Świadczy to o tym, iż formuła KOCHA(Adam, Ewa) jest niespełniona, zatem formuła KOCHA(Adam, Ewa) jest spełniona, co należało dowieść.</p>
        `
      },
      {
        "id": 4,
        "question": "Budowa programu w Prologu: klauzule (fakty, reguły), definicje predykatów. Sposób realizacji programu.",
        "answer": `
          <p><strong>PROGRAM</strong> zbiór procedur; kolejność procedur w programie nie jest istotna.</p>
          <p><strong>PROCEDURA</strong> ciąg klauzul definiujących jeden predykat kolejność klauzul w procedurze jest istotna</p>
          <p><strong>KLAUZULA</strong> fakt lub reguła.</p>
          <p><strong>FAKT</strong> Bezwarunkowo prawdziwe stwierdzenie o istnieniu pewnych powiazań (relacji) miedzy obiektami.</p>
          <p>Budowa: symbol_relacji (obiekt1, ..., obiektn)</p>
          <p>Przykłady <strong>student(marcin)</strong>. Marcin jest studentem.</p>
          <p><strong>lubi(ewa, marek)</strong>. Ewa lubi Marka</p>
          <p><strong>REGUŁA</strong>: Warunkowe stwierdzenie istnienia pewnych powiazań (relacji) miedzy obiektami.</p>
          <p><strong>symbol_relacji(obiekt1, ..., obiektn) :- symbol_relacji_1 (obiekt1, ..., obiektn1), symbol_relacji_2 (obiekt1, ..., obiektn2)…</strong></p>
          <p>Przykład: <strong>powierzchnia (X, Y) :- dlugosc (X, D), szerokosc (X, S), Y is D*S.</strong></p>
          <p>Powierzchnia Y prostokata X jest równa iloczynowi długosci D i szerokosci S tego prostokata.</p>
          <p>Rozpoczęcie działania programu polega na wywołaniu dowolnej procedury, które jest nazywane w Prologu zadawaniem pytań lub podawaniem celu.</p>
          <p><strong>Celem działania programu</strong> jest uzyskanie odpowiedzi na Pytanie o prawdziwość podanych faktów lub</p>
          <p>polecenie znalezienia nazw obiektów będących w podanej relacji z innymi.</p>
          <p>Postać ?- symbol_relacji_1 (obiekt1, ..., obiektn1),</p>
          <p>symbol_relacji_2 (obiekt1, ..., obiektn2).</p>
          <p><strong>Przykład</strong></p>
          <p>?- <strong>lubi(marta, jan).</strong> - Czy Marta lubi Jana?</p>
          <p><strong>lubi(marta, X).</strong> – Kogo lubi Marta?</p>
          <p><strong>lubi(X, marta)</strong> – Kto lubi Martę?</p>
        `
      }
    ]
  },
  {
    "id": 13,
    "name": "Technika cyfrowa",
    "icon": "ui-radios-grid",
    "chance": 1,
    "questions": [
      {
        "id": 0,
        "question": "Co to są systemy (zestawy) funkcjonalnie pełne? Podać przykładowe.",
        "answer": `
        <p><strong>Systemem funkcjonalnie pełnym</strong> nazywa się taki zbiór operatorów (funkcji), którymi można przedstawić
        dowolne wyrażenie logiczne (dowolną funkcję).</p>
        <p>Funkcje sumy, iloczynu i negacji tworzą tzw. <strong>podstawowy system funkcjonalnie pełny</strong>. Nie jest to jednak
        system minimalny. Systemy funkcjonalnie pełne tworzą również:</p>
        <ul>
          <li>iloczyn i negacja (suma może zostać wyeliminowana dzięki prawu De Morgana)</li>
          <li>suma i negacja (analogicznie jak wyżej)</li>
        <li>funkcja Sheffer'a (NAND) (jak wyżej oraz ponieważ <span class="neg">a</span> = <span class="neg">a · a</span>)</li>
          <li>funkcja Pierce'a (NOR) (<span class="neg">a</span> = <span class="neg">a + a</span>)</li>
        </ul>
        `
      },
      {
        "id": 1,
        "question": "Podaj rodzaje układów sekwencyjnych oraz różnice w procedurach ich projektowania.",
        "answer": `
          <p><strong>Układ sekwencyjny</strong></p>
          <p>Charakteryzuje się tym, że stan wyjść y zależy od stanu wejść x
          oraz od poprzedniego stanu, zwanego stanem wewnętrznym,
          pamiętanego w zespole rejestrów (pamięci).</p>
          <p>Jeżeli stan wewnętrzny nie ulega zmianie pod wpływem podania
          różnych sygnałów X, to taki stan nazywa się stabilnym.</p>
          <p><strong>Rozróżnia się dwa rodzaje układów sekwencyjnych</strong>:</p>
          <ol>
            <li>asynchroniczne</li>
            <li>synchroniczne</li>
          </ol>
          <p><strong>Układy asynchroniczne</strong></p>
          <p>W układach asynchronicznych zmiana sygnałów wejściowych X natychmiast powoduje zmianę wyjść Y. W
          związku z tym układy te są szybkie, ale jednocześnie podatne na zjawisko hazardu i wyścigu. Zjawisko
          wyścigu występuje, gdy co najmniej dwa sygnały wejściowe zmieniają swój stan w jednej chwili czasu (np.
          11b -> 00b).</p>
          <p><strong>Układy synchroniczne</strong></p>
          <p>W układach synchronicznych zmiana stanu wewnętrznego następuje wyłącznie w określonych chwilach,
          które wyznacza sygnał zegarowy (ang. clock). Każdy układ synchroniczny posiada wejście zegarowe
          oznaczane zwyczajowo symbolami C, CLK lub CLOCK. Charakterystyczne dla układów synchronicznych,
          jest to, iż nawet gdy stan wejść się nie zmienia, to stan wewnętrzny - w kolejnych taktach zegara - może
          ulec zmianie.</p>
          <p><strong>Różnice polegają na konieczności eliminacji zjawisk szkodliwych w układach asynchronicznych
          (wyścigów i hazardów).</strong></p>
        `
      },
      {
        "id": 2,
        "question": "Jakie znasz elementy pamięciowe stosowane układach sekwencyjnych?",
        "answer": `
          <p>Układy synchroniczne produkuje się z przerzutników typu D, T, i JK.</p>
          <p>Układy asynchroniczne produkuje się z przerzutników RS lub bez przerzutników (bramki ze sprzężeniem zwrotnym)</p>
          <img src="http://utk.edu.pl/wp-content/uploads/2017/09/przerzutnik-D.png" style="filter: invert(1);">
          <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/T-Type_Flip-flop.svg/1024px-T-Type_Flip-flop.svg.png" style="filter: invert(1); width: 150px;">
          <img src="https://eduinf.waw.pl/inf/alg/002_struct/images/0034_01.gif" style="filter: invert(1);">
          <img src="http://utk.edu.pl/wp-content/uploads/2017/09/przerzutnik-RS.png" style="filter: invert(1); width: 400px;">
        `
      },
      {
        "id": 3,
        "question": "Co oznacza termin mikroprogramowanie? Do czego służy?",
        "answer": `
          <p><strong>Układ mikroprogramowany</strong> to jedno z możliwych rozwiązań stosowanych w technice cyfrowej dla
          projektów o dużej złożoności. <strong>Projekt dekomponowany jest na dwie części: układ operacyjny
          (wykonawczy) i układ sterujący</strong>. Układ sterujący można zaprojektować jako układ mikroprogramowany.
          Bity sterujące, wraz z innymi odpowiednimi bitami służącymi do napisania mikroprogramu, umieszcza się
          w pamięci ROM.</p>
        `
      },
      {
        "id": 4,
        "question": "Podaj znane zapisy liczbowe i zakres ich stosowania.",
        "answer": `
          <p>Zapisy można podzielić na <strong>stałopozycyjne (liczby całkowite)</strong> i <strong>zmiennopozycyjne (liczby rzeczywiste)</strong>.
          <strong>Stałopozycyjne</strong> to: ZNAK-MODUŁ, U1 i U2. Najpowszechniejszy jest U2 ze względu na najprostsze
          algorytmy operacji arytmetycznych.</p>
          <p><strong>Zmiennopozycyjny</strong> zapis polega na reprezentowaniu liczby za pomocą trzech grup bitów:
          1 bit znaku, 23-bitowej mantysy i np. 8-bitowego wykładnika.</p>
          <table>
            <tr>
              <td>8 bitów</td>
              <td>char</td>
              <td>-128 - +127 (ze znakiem)</td>
            </tr>
            <tr>
              <td>16 bitów</td>
              <td>short int</td>
              <td>-32 768 - +32 767 (ze znakiem)</td>
            </tr>
            <tr>
              <td>32 bity</td>
              <td>int</td>
              <td>-2 147 483 648 - +2 147 483 647 (ze znakiem)</td>
            </tr>
            <tr>
              <td>32 bity</td>
              <td>float</td>
              <td></td>
            </tr>
            <tr>
              <td>64 bity</td>
              <td>double</td>
              <td></td>
            </tr>
          </table>
        `
      }
    ]
  },
  {
    "id": 14,
    "name": "Systemy wbudowane",
    "icon": "stoplights",
    "chance": 1,
    "questions": [
      {
        "id": 0,
        "question": "Co to są mikrokontrolery?",
        "answer": `
          <p><strong>Mikrokontroler</strong> - system mikroprocesorowy zrealizowany w postaci pojedynczego układu scalonego,
          zawierającego jednostkę centralną (CPU), pamięć RAM oraz na ogół pamięć programu i rozbudowane
          układy wejścia-wyjścia. Określenie <em>mikrokontroler</em> pochodzi od głównego obszaru zastosowań, jakim jest
          sterowanie urządzeniami elektronicznymi.</p>
          <p>Mikrokontroler stanowi użyteczny i całkowicie autonomiczny system mikroprocesorowy, nie wymagający
          użycia dodatkowych elementów, których wymagałby do pracy tradycyjny mikroprocesor. Mikrokontrolery
          wykorzystuje się powszechnie w sprzęcie AGD, układach kontrolno-pomiarowych, w przemysłowych
          układach automatyki, w telekomunikacji itp.</p>
        `
      },
      {
        "id": 1,
        "question": "Jakie parametry są charakterystyczne dla pamięci dynamicznych?",
        "answer": `
          <p><strong>Dla pamięci dynamicznej charakterystycznymi parametrami są</strong>:</p>
          <ul>
            <li>wymagany czas odświeżania (8-64 ms)</li>
            <li>wymaganą liczbę cykli odświeżania (pierwiastek z liczby bitów).</li>
          </ul>
          <p><strong>Podstawowymi parametrami opisującymi każdy typ pamięci są</strong>:</p>
          <ul>
            <li><strong>Pojemność</strong> każdej pamięci, to ilość informacji którą dana pamięć jest w stanie przechować.</li>
            <li><strong>Szybkość</strong> jest parametrem określającym jak często procesor, lub inne urządzenie mogą z niej korzystać.</li>
            <li><strong>Czasem dostępu</strong> - access time. Jest to czas upływający od momentu w którym wysyłane jest żądanie dostępu do pamięci do czasu w którym informacja zwrotna ukaże się na jej wyjściu.</li>
            <li><strong>Czasem cyklu</strong> - cycle time. Jest to czas najkrótszy, który upływa między dwoma kolejnymi żądaniami dostępu do pamięci.</li>
            <li><strong>Szybkością transmisji</strong> - Szybkość transmisji mierzymy liczbą bitów, bądź bajtów, którą jesteśmy w stanie przesłać pomiędzy urządzeniem, a pamięcią.</li>
            <li><strong>Również pobór mocy</strong> stanowi bardzo ważny parametr, który staje się problemem przy budowaniu urządzeń zasilanych bateryjnie, jak komputery kieszonkowe, laptopy.</li>
          </ul>
        `
      },
      {
        "id": 2,
        "question": "Podaj tryby adresowania dla rozkazów mikrokontrolera i odpowiadający im czas trwania cyklu instrukcyjnego wraz z omówieniem kolejnych cykli maszynowych.",
        "answer": `
          <p><strong>Trybem adresowania</strong> nazywa się sposób określenia miejsca przechowywania argumentów rozkazu.</p>
          <p><em>Rozróżniamy następujące tryby adresowania</em>:</p>
          <ul>
            <li><strong>adresowanie natychmiastowe</strong>, w którym argument rozkazu zawarty jest w kodzie rozkazu. <strong>2 cykle</strong></li>
            <li><strong>adresowanie bezpośrednie</strong>, w którym kod rozkazu zawiera adres komórki pamięci przechowującej argument rozkazu. <strong>4 cykle</strong></li>
            <li><strong>adresowanie rejestrowe</strong>, w którym w kodzie rozkazu określony jest rejestr zawierający argument rozkazu. <strong>1 cykl</strong></li>
            <li><strong>adresowanie pośrednie</strong> (rejestrowe pośrednie), w którym kod rozkazu zawiera określenie rejestru, bądź rejestrów zawierających adres komórki pamięci z argumentem rozkazu. <strong>2 cykle</strong></li>
          </ul>
          <p><em>Rodzaje cykli</em>:</p>
          <ul>
            <li>Pobranie kodu operacyjnego (fetch)</li>
            <li>Odczyt z pamięci (memory read)</li>
            <li>Zapis do pamięci (memory write)</li>
            <li>Odczyt z urządzenia wejściowego (IOread)</li>
            <li>Zapis do urządzenia wyjściowego (IOwrite)</li>
            <li>Cykl przyjęcia przerwania (interrupt)</li>
            <li>Cykl zatrzymania (HALT)</li>
          </ul>
        `
      },
      {
        "id": 3,
        "question": "Jakie znasz rodzaje transmisji szeregowej i na czym one polegają?",
        "answer": `
          <p><strong>Transmisja szeregowa</strong> – rodzaj cyfrowej transmisji danych, podczas której poszczególne bity informacji są przesyłane kolejno po sobie wraz z dodatkowymi danymi pozwalającymi na kontrolę tej transmisji. Medium transmisyjnym może być połączenie elektryczne (przewód lub para skręconych przewodów), radiowe (fale elektromagnetyczne) lub optyczne (fale rozchodzące się swobodnie lub wzdłuż określonej drogi, na przykład w światłowodzie).</p>
          <p><strong>Asynchroniczna</strong>: Transmisja ta pozwala na transmisję 1 bajtu z parametrami. Zegary w nadajniku i
          odbiorniku ustawione są na tą samą częstotliwość. Transmisja rozpoczyna się od przesłania bitu startu,
          następnie przesyłany jest znak i transmisję kończy bit stopu.</p>
          <p><strong>Synchroniczna</strong>: W transmisji synchronicznej specjalna preambuła dokonuje zsynchronizowania nadawcy i
          odbiorcy. Po synchronizacji następuje przesłanie danych.</p>
        `
      },
      {
        "id": 4,
        "question": "Porównaj pod względem szybkości znane rozwiązania operacji mnożenia w systemach wbudowanych.",
        "answer": `
          <p><strong>Mnożenie w układzie kombinacyjnym</strong> (układ iteracyjny gdzie wynik pojawia się po czasie opóźnienia
          wnoszonym przez bramki) jest najszybsze.</p>
          <p><strong>Mnożenie w układzie sekwencyjnym</strong> (tyle kroków ile jest bitów, a każdy krok to dodawanie warunkowe
          i przesunięcie) jest wolniejsze.</p>
          <p><strong>Mnożenie programowe</strong> (bez żadnego sprzętu) jest najwolniejsze.</p>
        `
      }
    ]
  },
  {
    "id": 15,
    "name": "Sztuczna inteligencja",
    "icon": "diagram-2",
    "chance": 50,
    "questions": [
      {
        "id": 0,
        "question": "Model obliczeniowy perceptronu – możliwości i ograniczenia.",
        "answer": `
          <p><strong>Perceptron</strong> - posiada wejścia (<em>n</em> zmiennych wejściowych <em>x<sub>1</sub>,...,x<sub>n</sub></em>) przyjmujące wartości rzeczywiste lub
          binarne, będące odpowiednikiem dendrytów. Wejścia te albo są połączone z innymi perceptronami, albo
          stanowią wejścia (np. czujniki) całego układu. Wyjście stanowi pojedynczą wartość 0 lub 1.
          Parametry wewnętrzne perceptronu to <em>n</em> wag połączeń <em>w<sub>1</sub>, ..., w<sub>n</sub></em> (liczb rzeczywistych), symulujących wagi
          synaps łączących naturalne neurony. Ostatnim parametrem jest wartość odchylenia <em>b</em> (ang. bias)
          odpowiadająca za nieliniowe przekształcenie wejść w wyjście.</p>
          <p><strong>Zasada działania</strong></p>
          <p>Do każdego <em>i</em>-tego wejścia perceptronu przypisana jest waga <em>w<sub>i</sub></em>. Dla danych stanów wejściowych <em>x<sub>1</sub>, ... , x<sub>n</sub></em> liczymy sumę ważoną:</p>
          <div class="flex flex-row items-center">
            <span>s =</span>
            <div class="top-bottom" data-top="n" data-bottom="i=1">∑</div>
            <span>x<sub>i</sub>w<sub>i</sub> + b</span>
          </div>
          <p>Jeżeli s jest większa lub równa 0, to ustawiamy wyjście <em>y=1</em>, zaś w
          przeciwnym przypadku ustawiamy <em>y=0</em>. Perceptron opisany jest więc
          jednoznacznie przez zbiór wag <em>w<sub>1</sub>, ..., w<sub>n</sub></em> oraz wartość odchylenia <em>b</em>.</p>
          <p>Pojedynczy perceptron nie potrafi odróżniać zbiorów
          nieseparowalnych liniowo, tzn. takich, że między punktami z
          odpowiedzią pozytywną i negatywną nie da się poprowadzić
          prostej rozgraniczającej. Jednym z najprostszych przykładów takich
          zbiorów jest funkcja logiczna XOR (alternatywa wykluczająca).</p>
        `
      },
      {
        "id": 1,
        "question": "Metody uczenia sieci neuronowych.",
        "answer": `
          <p>Pod pojęciem uczenia sieci rozumiemy wymuszenie na niej określonej reakcji na zadane sygnały
          wejściowe. Uczenie jest konieczne tam, gdzie brak jest informacji doświadczalnych o powiązaniu wejścia z
          wyjściem lub jest ona niekompletna, co uniemożliwia szczegółowe zaprojektowanie sieci. Uczenie może
          być realizowane krok po kroku lub poprzez pojedynczy zapis.</p>
          <p><strong>Uczenie z nadzorem</strong></p>
          <p>Stosuje się tylko wówczas, gdy istnieje możliwość zweryfikowania poprawności odpowiedzi udzielanych
          przez sieć; oznacza to, że dla każdego wektora wejściowego musi być znana dokładna postać wektora
          wyjściowego (pożądana odpowiedź). Celem uczenia pod nadzorem jest minimalizacja odpowiednio
          zdefiniowanej funkcji celu.</p>
          <p><strong>Uczenie bez nadzoru</strong></p>
          <p>Stosuje się wówczas gdy nie znamy oczekiwanych odpowiedzi na zadany wzorzec. Podczas uczenia bez
          nauczyciela pożądana odpowiedz nie jest znana. Ze względu na brak informacji o poprawności, czy
          niepoprawności odpowiedzi sieć musi się uczyć poprzez analizę reakcji na pobudzenia. W trakcie analizy
          parametry sieci podlegają zmianom, co nazywamy samoorganizacją.</p>
        `
      },
      {
        "id": 2,
        "question": "Mechanizm działania algorytmu genetycznego.",
        "answer": `
          <p><strong>Algorytm genetyczny</strong> - rodzaj algorytmu przeszukującego przestrzeń alternatywnych rozwiązań
          problemu w celu wyszukania rozwiązań najlepszych.
          Sposób działania algorytmów genetycznych nieprzypadkowo przypomina zjawisko ewolucji biologicznej.
          Obecnie zalicza się go do grupy algorytmów ewolucyjnych.</p>
          <p><strong>Najczęściej działanie algorytmu przebiega następująco</strong>:</p>
          <ol>
            <li>Losowana jest pewna populacja początkowa.</li>
            <li>Populacja poddawana jest ocenie (<strong>selekcja</strong>). Najlepiej przystosowane osobniki biorą udział w procesie reprodukcji.</li>
            <li>Genotypy wybranych osobników poddawane są operatorom ewolucyjnym:</li>
            <ul class="pl-8">
              <li>są ze sobą kojarzone poprzez złączanie genotypów rodziców (<strong>krzyżowanie</strong>),</li>
              <li>przeprowadzana jest mutacja, czyli wprowadzenie drobnych losowych zmian.</li>
            </ul>
            <li>Rodzi się drugie (kolejne) pokolenie i algorytm powraca do kroku drugiego, jeżeli nie znaleziono dostatecznie dobrego rozwiązania. W przeciwnym wypadku uzyskujemy wynik.</li>
          </ol>
        `
      },
      {
        "id": 3,
        "question": "Definicja entropii informacji i wybrane zastosowanie tego pojęcia.",
        "answer": `
          <p><strong>Entropia</strong> – w ramach teorii informacji jest definiowana jako średnia ilość informacji, przypadająca na
          pojedynczą wiadomość ze źródła informacji. Innymi słowy jest to średnia ważona ilości informacji
          niesionej przez pojedynczą wiadomość, gdzie wagami są prawdopodobieństwa nadania poszczególnych
          wiadomości.</p>
          <p>Wzór na entropię:</p>
          <div class="flex flex-row items-center">
            <span>H(x) =</span>
            <div class="top-bottom" data-top="n" data-bottom="i=1">∑</div>
            <span>p(i)log<sub>2</sub></span>
            <div class="fraction">
              <div>1</div>
              <div>p(i)</div>
            </div>
            <span>= -</span>
            <div class="top-bottom" data-top="n" data-bottom="i=1">∑</div>
            <span>p(i)log<sub>2</sub>p(i)</span>
          </div>
          <p>gdzie:</p>
          <p>p(i) - prawdopodobieństwo zajścia zdarzenia <em>i</em>,</p>
          <p>n - liczba wszystkich zdarzeń danej przestrzeni,</p>
          <ul>
            <li>przy ocenie bezpieczeństwa bezwarunkowego systemów szyfrowania</li>
            <li>w uczeniu maszynowym przez zastosowanie ukrytych modeli Markowa (HMM)</li>
            <li>porównywanie modeli językowych w lingwistyce komputerowej</li>
            <li>rekonstrukcja obrazu w tomografii komputerowej dla zastosowań medycznych</li>
          </ul>
        `
      },
      {
        "id": 4,
        "question": "Metody generacji reguł systemu decyzyjnego, minimalne, pokrywające, wyczerpujące.",
        "answer": `
          <p><strong>Reguły systemu decyzyjnego</strong> są jednym z narzędzi reprezentacji wiedzy. Opisują one związki między
          atrybutami warunkowymi, a atrybutem decyzyjnym. Systemy decyzyjne oparte na regułach zawierają
          zwykle wiele reguł, z których każda może być oparta na innym zestawie atrybutów warunkowych.</p>
          <p><strong>Metody minimalne</strong> - metody tworzenia systemu decyzyjnego poprzez wygenerowanie minimalnej ilości
          reguł ze zbioru treningowego. Przykład: LEM2</p>
          <p><strong>Metody pokrywające</strong> - idea: nauczyć sie reguły, po czym usunąć obiekt, który ta reguła pokrywa -
          powtarzać aż do wyczerpania zbioru obiektów. Przykład: Pokrywanie sekwencyjne.</p>
          <p><strong>Metody wyczerpujące</strong> - Polega na przeszukiwaniu wszystkich deskryptorów warunkowych obiektów
          treningowych począwszy od kombinacji długości 1 w celu znalezienia kombinacji , która jest niesprzeczna
          w systemie decyzyjnym i która nie zawiera w sobie reguł niższego rzędu. Przykład: EXHAUSTIVE: z
          najmniejszą ilością deskryptorów.</p>
        `
      }
    ]
  },
  {
    "id": 16,
    "name": "Wprowadzenie do grafiki maszynowej",
    "icon": "image",
    "chance": 1,
    "questions": [
      {
        "id": 0,
        "question": "Podaj znaczenie oraz omów wzajemne związki między terminami: przestrzeń urządzenia, przestrzeń operacyjna urządzenia, powierzchnia obrazowania, macierz adresowalna, jednostka rastru, krok kreślaka.",
        "answer": `
          <p><strong>Przestrzeń urządzenia (obrazującego)</strong> - to obszar zdefiniowany przez układ współrzędnych,
          ograniczony pojemnością rejestrów pozycji x i y w urządzeniu graficznym</p>
          <p><strong>Przestrzeń operacyjna</strong> - to obszar przestrzeni urządzenia którego zawartość została odwzorowana na
          powierzchni obrazowania</p>
          <p><strong>Powierzchnia obrazowania</strong> - nośnik, na którym mogą pojawiać się obrazy i rysunki, np. ekran lampy
          kineskopowej lub papier w ploterze.</p>
          <p><strong>Macierz adresowalna</strong> - to macierz utworzona z punktów adresowalnych określająca wielkość obrazu
          który można przesłać do generatora obrazu</p>
          <p><strong>Jednostka rastru</strong> - jednostką rastru jest piksel</p>
          <p><strong>Krok kreślaka</strong> - odpowiednik jednostki rastru, ale dla kreślaka</p>
        `
      },
      {
        "id": 1,
        "question": "Zdefiniuj okno i widok oraz oknowanie i obcinanie; podaj co to są współrzędne znormalizowane i do czego one służą.",
        "answer": `
          <p><strong>Okno</strong> – ograniczony obszar w obrębie przestrzeni obrazowania, który może być rozszerzony do całej przestrzeni obrazowania.</p>
          <p><strong>Widok</strong> – ograniczony obszar w obrębie przestrzeni operacyjnej urządzenia, który prezentuje zawartość okna. Widok może być rozszerzony do całej przestrzeni operacyjnej. Obszar (prostokątny) na urządzeniu graficznym</p>
          <p><strong>Okienkowanie</strong> – wyizolowanie dowolnej części wyświetlonego obrazu .</p>
          <p><strong>Obcinanie</strong> – wyznaczanie elementów wewnątrz okna.</p>
          <p><strong>Współrzędne</strong> znormalizowane – Często przejście od współrzędnych rzeczywistych do współrzędnych danego urządzenia graficznego jest wykonywane dwustopniowo. Najpierw przechodzi do współrzędnych znormalizowanych, to znaczy do kwadratu [0,1]x[0,1], a stad do współrzędnych urządzenia.</p>
        `
      },
      {
        "id": 2,
        "question": "Podaj ideę algorytmu Cohena-Sutherlanda obcinania odcinka do prostokątnego okna i jego trzy pierwsze kroki; podaj w jakich współrzędnych działa ten algorytm i dlaczego?",
        "answer": `
          <p>Algorytm Cohena-Sutherlanda jest analitycznym algorytmem obcinania dwuwymiarowych odcinków
          przez prostokąt obcinający, którego boki są równoległe do osi układu współrzędnych.</p>
          <p>Algorytm Cohena-Sutherlanda polega na przypisaniu każdemu końcowi odcinka czterobitowego kodu
          określającego położenie tego punktu względem prostokątnego okna. Dokładniej, bity są numerowane od
          prawego: <strong>Kod(P) = b<sub>4</sub>b<sub>3</sub>b<sub>2</sub>b<sub>1</sub></p></strong>
          <p>b<sub>1</sub> – gdy P leży na lewo od okna</p>
          <p>b<sub>2</sub> - gdy P leży na prawo od okna</p>
          <p>b<sub>3</sub> - gdy P leży poniżej okna</p>
          <p>b<sub>4</sub> - gdy P powyżej okna</p>
          <p>a w przeciwnym razie odpowiednie bity kodu mają wartość 0.</p>
          <p><strong>Algorytm przebiega następująco</strong>:</p>
          <ol>
            <li>Wybierany jest koniec odcinka leżący poza prostokątem, a więc mający niezerowy kod; jeśli kody obu końców są niezerowe to można wybrać dowolny z nich.</li>
            <li>Wyznaczany jest punkt przecięcia odcinka z jedną z prostych. Wybór prostych determinuje kod wybranego końca.</li>
            <li>Następnie odcinek jest przycinany do tego punktu - koniec wybrany w punkcie pierwszym jest zastępowany przez punkt przecięcia.</li>
          </ol>
        `
      },
      {
        "id": 3,
        "question": "Sformułuj zadanie i ogólne warunki trójkątowania wielokąta, warunki trójkątowania naturalnego oraz podaj ideę trójkątowania wielokąta monotonicznego.",
        "answer": `
          <p>Triangulacja wielokątów jest przeprowadzana w celu uproszczenia kształtów powierzchni, przez co
          ułatwia się rozwiązywanie zadań typu: wypełnianie obszarów, określenie zasłaniania, oświetlenia i
          przecinania się obiektów 3W.</p>
          <p><strong>Zadanie triangulacji</strong> formułuje się jako podział wielokąta zwykłego na sumę nie nakładających się na
          siebie trójkątów, których wierzchołkami mogą być tylko wierzchołki danego
          wielokąta.</p>
          <p><strong>Ogólne warunki</strong>:</p>
          <ol>
            <li>Liczba trójkątów powinna być jak najmniejsza (tj. równa n-2 dla n-kąta)</li>
            <li>trójkąty nie mogą się nakładać</li>
            <li>ich wierzchołkami mogą być tylko wierzchołki wielokąta</li>
          </ol>
          <p>W praktyce polega to na przeglądaniu odpowiednio uporządkowanych wierzchołków
          i tam, gdzie będzie to możliwe poprowadzenie przez kolejny wierzchołek przekątnej.
          Przekątna odetnie trójkąt pozostawiając prostszy wielokąt do dalszej triangulacji.</p>
          <p><strong>Triangulacja wielokąta monotonicznego</strong> - Algorytm (zachłanny, metoda „zamiatania”)</p>
          <ol>
            <li>Porządkujemy wierzchołki wzdłuż kierunku wyznaczonego przez prostą k (względem której wielokąt jest monotoniczny). Wymaga to czasu liniowego, gdyż jest to równoważne ze scalaniem dwóch uporządkowanych ciągów.</li>
            <li>Poruszając się wzdłuż prostej łączymy krawędzią punkt, do którego doszliśmy z wszystkimi możliwymi punktami, które zostały już odwiedzone, ale jeszcze nie zostały odcięte od reszty wielokąta przez krawędź triangulacji.</li>
          </ol>
        `
      },
      {
        "id": 4,
        "question": "Zdefiniuj przekształcenie 3-punktowe. Do czego ono służy?",
        "answer": `
          <p><strong>Przekształcenie trzypunktowe</strong></p>
          <p>Jest ono wykorzystywane przy budowie scen trójwymiarowych. Dane są trzy nie współliniowe punkty: P1,
          P2, P3 i Q1, Q2, Q3.</p>
          <p>Szukamy takiej izometrii (przekształcenia), która będzie spełniała następujące warunki:</p>
          <ul>
            <li>Odwzorowuje punkt P1 w Q1,</li>
            <li>Kierunek P=P2-P1 w kierunek Q=Q2-Q1,</li>
            <li>Transformuje płaszczyznę wyznaczoną przez : P1, P2, P3 w płaszczyznę wyznaczoną przez Q1, Q2, Q3</li>
          </ul>
        `
      },
      {
        "id": 5,
        "question": "Podaj wzajemne położenie układów współrzędnych: danych i obserwatora.",
        "answer": `
          <p>Układ współrzędnych obserwatora nie jest określony
          jednoznacznie. Przyjmuje się, że jego początek O’
          pokrywa się z początkiem O układu danych.
          Aby przekształcić układ danych do układu
          obserwatora należy:</p>
          <ol>
            <li>Określić kierunki osi w układzie obserwatora</li>
            <li>Przedstawić dane we współrzędnych układu obserwatora</li>
          </ol>
        `
      },
      {
        "id": 6,
        "question": "Wymień we właściwej kolejności operacje, jakie należy wykonać, aby obrócić punkt Po kąt ϕ dookoła prostej zadanej przez dwa punkty P1 i P2.",
        "answer": `
          <p>1) Przesunięcie, aby punkt P1 znalazł się w początku układu współrzędnych.</p>
          <p>2) Obrót wokół osi OX.</p>
          <p>3) Obrót wokół osi OY. Obroty (etap 2. i 3. ) zapewniają, że zadana oś obrotu (prosta) pokryje się (z uwzględnieniem zwrotów) z osią OX układu współrzędnych.</p>
          <p>4) Realizacja zadanego obrotu o kąt wokół osi OX.</p>
          <p>5) Obrót będący operacją odwrotną do operacji 3.</p>
          <p>6) Obrót będący operacją odwrotną do operacji 2.</p>
          <p>7) Przesunięcie odwrotne do przesunięcia 1.</p>
        `
      }
    ]
  },
  {
    "id": 17,
    "name": "Problemy społeczne i zawodowe informatyki",
    "icon": "vector-pen",
    "chance": 97,
    "questions": [
      {
        "id": 0,
        "question": "Opisać 3 podstawowe obszary uzależnień komputerowych.",
        "answer": `
          <p><strong>Środkami uzależniającymi są</strong>:</p>
          <ul>
            <li>gry komputerowe,</li>
            <li>Internet,</li>
            <li>programowanie.</li>
          </ul>
          <p>Pozbawieni dostępu do komputera przeżywają stany identyczne z zespołem abstynenckim, są pobudzeni,
          cierpią na zaburzenia snu, popadają w stany depresyjne, fantazjują na temat Internetu. Uzależnienia
          komputerowe można podzielić na pierwotne i wtórne. W pierwszym przypadku mamy do czynienia z
          potrzebą przeżycia emocji, uzyskania efektu pobudzenia, sprawdzenia swoich umiejętności. W drugim
          przypadku komputer jest traktowany jako forma ucieczki od rzeczywistości. Ponadto należy pamiętać, że ten
          rodzaj uzależnienia ma negatywne konsekwencje dla zdrowia.
          Uzależnienie od komputera u dziecka można stwierdzić, gdy:</p>
          <ul>
            <li>udaje, że się uczy, kiedy tymczasem godzinami siedzi przed ekranem monitora,</li>
            <li>w czasie gry lub przeglądania stron WWW wpada w stan przypominający trans,</li>
            <li>próby ograniczenia czasu spędzanego przy komputerze napotykają na silny opór, nawet agresję,</li>
            <li>niechętnie spotyka się z kolegami, brak mu przyjaciół, woli komputer niż ruch na świeżym powietrzu,</li>
            <li>nie potrafi odpoczywać, nie wzrusza się.</li>
          </ul>
        `
      },
      {
        "id": 1,
        "question": "Wymienić przynajmniej 3 polskie ustawy dotyczące środowiska informatycznego.",
        "answer": `
          <ul>
            <li>ustawa z dnia 23 listopada 1990 r. o łączności</li>
            <li>ustawa z dnia 29 sierpnia 1997 r. o ochronie danych osobowych</li>
            <li>ustawa z dnia 27 lipca 2001 r. o ochronie baz danych</li>
            <li>ustawa z dnia 6 września 2001 r. o dostępie do informacji publicznej</li>
            <li>ustawa z dnia 18 września 2001 r. o podpisie elektronicznym</li>
            <li>ustawa z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną</li>
          </ul>
        `
      },
      {
        "id": 2,
        "question": "Jaka jest zasadnicza różnica między ochroną własności intelektualnej i ochroną patentową?",
        "answer": `
          <p>Podstawową różnicą prawa patentowego jako systemu ochrony partykularnej jest wyłączenie możliwości
          uzyskania patentu przez różne podmioty na wynalazki, które są do siebie podobne.</p>
          <p>Czyli mając coś opatentowane nie możesz stworzyć czegoś podobnego posiadającego podobne algorytmy,
          funkcje itp a w prawie autorskim chronisz tylko program a nie algorytm itd.
          Jak cos jest chronione patentem to jest chronione jako patent i własność intelektualna, a jak cos jest
          chronione jako własność intelektualna, to niekoniecznie jest chronione patentem.</p>
          <p>Prawo autorskie chroni tylko formę (kod źródłowy i wynikowy). Nie chroni jednak odkryć, idei, procedur,
          metod, zasad działania, koncepcji matematycznych.</p>
        `
      },
      {
        "id": 3,
        "question": "Opisać na czym polega szpiegostwo komputerowe.",
        "answer": `
          <p>Szpiegostwo definiuje się jako działanie przestępne dokonywane na szkodę określonego państwa,
          polegające na wykonywaniu odpowiednich czynności na rzecz obcego wywiadu, a w szczególności na
          zbieraniu, przekazywaniu informacji organom obcego wywiadu lub na gromadzeniu i przechowywaniu
          informacji w celu przekazania obcemu wywiadowi.</p>
          <p>Artykuł 130 – paragrafy 1 do 4 określają szpiegostwo komputerowe jako:</p>
          <ul>  
            <li>Działanie w obcym wywiadzie przeciwko Rzeczypospolitej Polskiej</li>
            <li>Działanie w obcym wywiadzie lub biorąc w nim udział, udziela informacji, które mogą wyrządzić szkodę Rzeczypospolitej Polskiej,</li>
            <li>Kto, w celu udzielenia obcemu wywiadowi wiadomości określonych w § 2, gromadzi je lub przechowuje, włącza się do sieci komputerowej w celu ich uzyskania albo zgłasza gotowość działania na rzecz obcego wywiadu przeciwko Rzeczypospolitej Polskiej,</li>
            <li>Kierowanie lub tworzenie organizacji obcego wywiadu</li>
          </ul>
        `
      }
    ]
  }
];