import { IQuestion } from "./IQuestion";

export interface ICategory {
  id?: number;
  name: string;
  icon: string;
  chance: number | string;
  questions?: IQuestion[];
}