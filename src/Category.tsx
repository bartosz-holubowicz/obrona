import React, { FC } from "react";
import { ICategory } from "./data/ICategory";
// @ts-ignore
import wrapCustomElement from "@shoelace-style/react-wrapper";
import cn from "classnames";
import { IQuestion } from "./data/IQuestion";

const ShoelaceIcon = wrapCustomElement("sl-icon");
const ShoelaceDetails = wrapCustomElement("sl-details");

const Category: FC<ICategory> = ({ name, icon, chance, questions }) => {
  const chanceColor = chance >= 75 ?
    "text-red-500" : chance >= 50 ?
    "text-amber-600" : chance >= 30 ?
    "text-amber-300" : chance >= 10 ?
    "text-lime-400" : chance >= 3 ?
    "text-lime-300" : "text-blue-400";

  const handleQuestionClick = (evt: React.MouseEvent<any, MouseEvent>) => {
    // @ts-ignore
    if (evt.target.open)
      window.scroll({
        top: evt.pageY,
        left: 0,
        behavior: "smooth"
      });
  };

  return (
    <>
      <ShoelaceDetails onClick={handleQuestionClick}>
        <div slot="summary" className="flex flex-row items-center gap-x-3 z-0">
          <div className="text-2xl inline-flex"><ShoelaceIcon name={icon}></ShoelaceIcon></div>
          <h1>{name}</h1>
          <h3 className={cn(chanceColor)}>{chance}%</h3>
        </div>
        {questions?.map((el: IQuestion, idx: number) =>
          <ShoelaceDetails
            key={`.q_${icon}_${idx}`}
            summary={`${el.question}`}
            onClick={handleQuestionClick}
          >
            <div dangerouslySetInnerHTML={{__html: el.answer }}></div>
          </ShoelaceDetails>
        )}
        </ShoelaceDetails>
    </>
  );
};

export default Category;