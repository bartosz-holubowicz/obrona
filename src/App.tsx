import React from 'react';
import './App.css';
import Category from './Category';
import { questionsData } from "./data/data";
import { ICategory } from './data/ICategory';

function App() {
  return (
    <div className="App p-1">
      {questionsData.sort((a, b) => +b.chance - +a.chance).map((el: ICategory, idx: number) => <Category
        key={`.c_${idx}`}
        name={el.name}
        icon={el.icon}
        chance={el.chance}
        questions={el.questions}
      />)}
    </div>
  );
}

export default App;
