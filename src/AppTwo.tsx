import React, { useEffect, useRef, useState } from 'react';
import { questionsData } from "./data/data";
import { ICategory } from './data/ICategory';
import { useHotkeys } from "react-hotkeys-hook";
// @ts-ignore
import wrapCustomElement from "@shoelace-style/react-wrapper";
import './App.css';
import { IQuestion } from './data/IQuestion';

const ShoelaceIcon = wrapCustomElement("sl-icon");
const ShoelaceDetails = wrapCustomElement("sl-details");
const ShoelaceButton = wrapCustomElement("sl-button");

function AppTwo() {
  const [questionView, setQuestionView] = useState<Boolean>(false);
  const [selectedQuestions, setSelectedQuestions] = useState<IQuestion[]>([]);
  const [searchValue, setSearchValue] = useState<string>("");
  const searchInput = useRef<HTMLInputElement>(null);

  const questionsForm = useRef<HTMLFormElement>(null);
  
  const getQuestion = (cIdx: number, qIdx: number): IQuestion | undefined => {
    return questionsData.find((q: ICategory) => q.id === cIdx)?.questions?.[qIdx];
  }

  const addQuestion = (q: IQuestion) => {
    setSelectedQuestions((prev: IQuestion[]) => [...prev, q]);
  };

  const removeQuestion = (q: IQuestion) => {
    setSelectedQuestions((prev: IQuestion[]) => prev.filter((_q: IQuestion) => _q.question !== q?.question));
  };

  const handleCheckboxChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
    const { name, checked } = evt.target;
    if (checked)
      addQuestion(idToQuestion(name));
    else
      removeQuestion(idToQuestion(name));
  };

  const handleViewToggle = (evt: Event) => {
    evt.preventDefault();

    window.scrollTo({ top: 0, left: 0 });
    setQuestionView((prev) => {
      if (prev === true)
        setSearchValue("");

      return !prev;
    });
  };

  useHotkeys("ctrl+space", handleViewToggle, {
    enableOnTags: ["INPUT"],
    filterPreventDefault: true,
  });
  useHotkeys("ctrl+z", (evt: KeyboardEvent) => {
    evt.preventDefault();
    evt.stopPropagation();
    setSearchValue("");
    searchInput.current?.focus();
  }, {
    enableOnTags: ["INPUT"],
    filterPreventDefault: true,
  });
  useHotkeys("ctrl+x", (evt: KeyboardEvent) => {
    evt.preventDefault();
    evt.stopPropagation();
    
    const firstQuestion = questionsData
      .sort((a, b) => +b.chance - +a.chance)
      .filter((c: ICategory) => (searchCategoryForQuestions(c) || []).length > 0)
      .map((c: ICategory) => {
        return {
          ...c,
          questions: (searchValue.length > 0 ? c.questions?.filter(isQuestionSearched) : c.questions)
        }
      })[0].questions?.[0];
    
    if (!selectedQuestions.find((q: IQuestion) => q.question === firstQuestion?.question))
      // @ts-ignore
      addQuestion(firstQuestion);
    else
      // @ts-ignore
      removeQuestion(firstQuestion);

    searchInput.current?.focus();
  }, {
    enableOnTags: ["INPUT"],
    filterPreventDefault: true,
  });

  useHotkeys("1,numpad1", (evt: KeyboardEvent) => {
    window.scrollTo({
      top: document.querySelectorAll("h1")?.[0]?.offsetTop,
      left: 0,
    });
  }, {
    filterPreventDefault: true,
  });

  useHotkeys("2", (evt: KeyboardEvent) => {
    window.scrollTo({
      top: document.querySelectorAll("h1")?.[1]?.offsetTop,
      left: 0,
    });
  }, {
    filterPreventDefault: true,
  });

  useHotkeys("3", (evt: KeyboardEvent) => {
    window.scrollTo({
      top: document.querySelectorAll("h1")?.[2]?.offsetTop,
      left: 0,
    });
  }, {
    filterPreventDefault: true,
  });

  useHotkeys("4", (evt: KeyboardEvent) => {
    window.scrollTo({
      top: document.querySelectorAll("h1")?.[3]?.offsetTop,
      left: 0,
    });
  }, {
    filterPreventDefault: true,
  });

  useHotkeys("5", (evt: KeyboardEvent) => {
    window.scrollTo({
      top: document.querySelectorAll("h1")?.[4]?.offsetTop,
      left: 0,
    });
  }, {
    filterPreventDefault: true,
  });

  useHotkeys("6", (evt: KeyboardEvent) => {
    window.scrollTo({
      top: document.querySelectorAll("h1")?.[5]?.offsetTop,
      left: 0,
    });
  }, {
    filterPreventDefault: true,
  });

  useHotkeys("7", (evt: KeyboardEvent) => {
    window.scrollTo({
      top: document.querySelectorAll("h1")?.[6]?.offsetTop,
      left: 0,
    });
  }, {
    filterPreventDefault: true,
  });

  useHotkeys("8", (evt: KeyboardEvent) => {
    window.scrollTo({
      top: document.querySelectorAll("h1")?.[7]?.offsetTop,
      left: 0,
    });
  }, {
    filterPreventDefault: true,
  });

  useHotkeys("9", (evt: KeyboardEvent) => {
    window.scrollTo({
      top: document.querySelectorAll("h1")?.[8]?.offsetTop,
      left: 0,
    });
  }, {
    filterPreventDefault: true,
  });

  const idToQuestion = (id: string): IQuestion => {
    const [cIdx, qIdx]: number[] = id.split("-").map(Number);

    // @ts-ignore
    return getQuestion(cIdx, qIdx);
  };

  const isQuestionSearched = (q: IQuestion): Boolean => {
    return q.question?.toLowerCase().indexOf(searchValue.toLowerCase()) > -1;
  };

  const searchCategoryForQuestions = (c: ICategory) => {
    return c.questions?.filter(isQuestionSearched);
  };

  useEffect(() => {
    console.log(selectedQuestions);
  }, [selectedQuestions]);

  return (
    <div className="App p-1 pt-20">
      <ShoelaceButton onClick={handleViewToggle}>
        {questionView ? "Wróć do kategorii" : "Pokaż wybrane pytania"}
      </ShoelaceButton>
      { !questionView ? 
      <form id="questions" ref={questionsForm}>
        <input
          ref={searchInput}
          className="flex bg-black placeholder-gray-500 border-b border-solid py-1 w-full outline-none border-gray-500"
          value={searchValue} onChange={(evt: React.ChangeEvent<HTMLInputElement>) => setSearchValue(evt.target.value)}
          placeholder="Wyszukaj pytanie"
        />
        {questionsData
          .sort((a, b) => +b.chance - +a.chance)
          .filter((c: ICategory) => (searchCategoryForQuestions(c) || []).length > 0)
          .map((c: ICategory, cIdx: number) => {
            return (
              <ShoelaceDetails key={`.cat_${c.id}`} open={searchValue.length > 0}>
                <div slot="summary" className="flex flex-row items-center gap-x-3 z-0">
                  <div className="text-2xl inline-flex"><ShoelaceIcon name={c.icon}></ShoelaceIcon></div>
                  <h1>{c.name}</h1>
                </div>
                {
                (searchValue.length > 0 ? c.questions?.filter(isQuestionSearched) : c.questions)?.map((q: IQuestion, qIdx: number) => {
                  const id = `${c.id}-${q.id}`;
                  
                  return (
                    <div key={`.q_${q.id}`} className="flex items-center">
                      <input
                        className="mr-2"
                        type="checkbox"
                        id={`question_${id}`}
                        name={`${id}`}
                        onChange={handleCheckboxChange}
                        checked={!!selectedQuestions.find((_q: IQuestion) => _q.question === q.question)}
                      />
                      <label className="py-2 flex-1" htmlFor={`question_${id}`}>{q.question}</label><br/>
                    </div>
                  )
                })}
              </ShoelaceDetails>
            );
        })}
      </form> :
      <div>
        {selectedQuestions.map((q: IQuestion, idx: number) =>
          <div key={`.answer_${q.id}-${idx}`}>
            <h1 className="text-4xl font-bold pb-2">{q.question}</h1>
            <div dangerouslySetInnerHTML={{ __html: q.answer }} className="mb-8" />
          </div>
        )}
      </div> }
      <div className="fixed bg-black border border-solid border-white px-2 left-2 bottom-2">
        <p><strong>CTRL+Z</strong> - search focus</p>
        <p><strong>CTRL+X</strong> - wybierz pierwsze pytanie z wyświetlonych</p>
        <p><strong>CTRL+Spacja</strong> - przełącz widok</p>
        <p><strong>1,2,3…</strong> - przeskocz do pytania</p>
      </div>
    </div>
  );
}

export default AppTwo;
